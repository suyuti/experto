const sql = require('sql-template-strings');
const db = require('./db');


//-------------------------------------------------------------------------------------------------------

async function getAll() {
    const {rows} = await db.query(sql`
        SELECT * FROM "Roles" ORDER BY id;`);
    return rows;
}


//-------------------------------------------------------------------------------------------------------

async function getById(id) {
    const result = await db.query(sql`
        SELECT * FROM "Roles" WHERE id=${id} LIMIT 1;
    `);
    return result.rows
}

//-------------------------------------------------------------------------------------------------------

async function create(roleAndPermission) 
{ //roleAndPermission{
    // RoleFormu 
    // rolePermission  } 
    try {
        var q = sql`INSERT INTO "Roles" ("RoleName") 
        VALUES (${roleAndPermission.RoleFormu.RoleName});`
        await db.query(q);

        q = sql`SELECT * FROM "Roles"  WHERE "RoleName"=${roleAndPermission.RoleFormu.RoleName}`
        const result = await db.query(q);
        var selectRole = result.rows[0];


        q = `INSERT INTO "RolesPermissions" ("RoleId","PermissionId") 
            VALUES`;

        for (let index = 0; index < roleAndPermission.rolePermission.length; index++) {
            const element = roleAndPermission.rolePermission[index];
            q += `(${selectRole.id},${element.id}),`
        }

        var temp = q.split("");
        temp[temp.length - 1] = ";";
        q = temp.join("");

        console.log(q)
        await db.query(q);

        return true;
    }
    catch (error) {
        throw error;
    }
}

//-------------------------------------------------------------------------------------------------------
// https://stackoverflow.com/questions/42876071/how-to-save-js-date-now-in-postgresql

async function update(RoleId, roleAndPermission) {
    try {
        // var now = new Date().getTime().toString();
        // var q = sql`Update "Roles" Set 
        //                 "RolesAdi"       = ${Roles.RolesAdi}
        //                 "MusteriId	"   = ${Roles.MusteriId	}
        //              Where id = ${id}`
        // const {rows} = await db.query(q);



        var q =  `Update "Roles" Set   "RoleName"= '${roleAndPermission.RoleFormu.RoleName}'  Where id = ${RoleId};`

            q +=  `DELETE FROM "RolesPermissions" WHERE "RoleId"=${RoleId};`

            q += `INSERT INTO "RolesPermissions" ("RoleId","PermissionId") 
            VALUES`;

            for (let index = 0; index < roleAndPermission.rolePermission.length; index++) {
                const element = roleAndPermission.rolePermission[index];
                q += `(${RoleId},${element.id}),`
            }

            var temp = q.split("");
            temp[temp.length - 1] = ";";
            q = temp.join("");

            console.log(q) 
            const result= await db.query( q );
            return true;

    }
    catch(error) {
        throw error;
    }
    return Roles
}

//-------------------------------------------------------------------------------------------------------

async function remove(id) {
    try {
        var q = `Delete FROM "Roles" Where id = ${id};`
        q +=  `DELETE FROM "RolesPermissions" WHERE "RoleId"=${id};`
        const result = await db.query(q);
        //console.log("deneme "+JSON.stringify( result))
        return true;
    }
    catch(error) {
        throw error;
    }
}

//-------------------------------------------------------------------------------------------------------

async function getAllByMusteriId(id) {
    return "hazır degil"
    const result = await db.query(sql`
        SELECT * FROM "Roles" WHERE id=${id} LIMIT 1;
    `);
    return result.rows
}
//-------------------------------------------------------------------------------------------------------

async function getPermissionByRoleId(RoleId) {
    const result = await db.query(sql`
        SELECT * FROM "Permissions" WHERE id IN (SELECT "PermissionId" FROM "RolesPermissions" WHERE "RoleId"=${RoleId});
    `);
    return result.rows
}

//-------------------------------------------------------------------------------------------------------

module.exports ={
    getAll,
    getById,
    create,
    update,
    remove,
    getAllByMusteriId,
    getPermissionByRoleId
}