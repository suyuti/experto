const sql = require('sql-template-strings');
const db = require('./db');


//-------------------------------------------------------------------------------------------------------

async function getAll() {
    const {rows} = await db.query(sql`
        SELECT * FROM "Permissions" ORDER BY id;`);
    return rows;
}


//-------------------------------------------------------------------------------------------------------

async function getById(id) {
    const result = await db.query(sql`
        SELECT * FROM "Permissions" WHERE id=${id} LIMIT 1;
    `);
    return result.rows
}

//-------------------------------------------------------------------------------------------------------

async function create(Permissions) 
{
    try {
        var q = sql`INSERT INTO "Permissions" ("Path","Aciklama" ,"PermissionKey","Method") 
        VALUES (${Permissions.Path},${Permissions.Aciklama},${Permissions.PermissionKey},${Permissions.Method})`
        console.log(q)
        const result= await db.query(q);
        //console.log("deneme "+JSON.stringify( deneme))
        return true;
    }
    catch(error) {
        throw error;
    }
}

//-------------------------------------------------------------------------------------------------------
// https://stackoverflow.com/questions/42876071/how-to-save-js-date-now-in-postgresql

async function update(id, Permissions) {
    try {
        var now = new Date().getTime().toString();
        var q = sql`Update "Permissions" Set 
                        "Path" = ${Permissions.Path},
                        "Aciklama" = ${Permissions.Aciklama},
                        "PermissionKey" = ${Permissions.PermissionKey},
                        "Method" = ${Permissions.Method}
                     Where id = ${id}`
        const {rows} = await db.query(q);
    }
    catch(error) {
        throw error;
    }
    return Permissions 

}

//-------------------------------------------------------------------------------------------------------

async function remove(id) {
    try {
        var q = sql`Delete FROM "Permissions" Where id = ${id}`
        const result = await db.query(q);
        //console.log("deneme "+JSON.stringify( result))
        return true;
    }
    catch(error) {
        throw error;
    }
}

module.exports ={
    getAll,
    getById,
    create,
    update,
    remove
}