const sql = require('sql-template-strings');
const db = require('./db');


//-------------------------------------------------------------------------------------------------------

async function getAll() {
    const {rows} = await db.query(sql`
        SELECT * FROM "PersonelProfil" ORDER BY id;`);
    return rows;
}

//-------------------------------------------------------------------------------------------------------

async function getAllByMusteriId(Musteri_ID) {
    // const {rows} = await db.query(sql`
    // select  * from "PersonelProfil" as pp 
    // where pp."id" IN (select mpp."PersonelProfil_ID"  from "Musteri.PersonelProfil" as mpp where mpp."Musteri_ID"= ${Musteri_ID});`)
    
    const {rows} = await db.query(sql`
    SELECT * FROM "PersonelProfil" WHERE "musteriID"=${Musteri_ID};
`);  
    return rows;

    
}
 

//-------------------------------------------------------------------------------------------------------

async function getById(id) {
    const result = await db.query(sql`
        SELECT * FROM "PersonelProfil" WHERE id=${id} LIMIT 1;
    `);
    return result.rows
}

//-------------------------------------------------------------------------------------------------------

async function create(PersonelProfil) 
{
    try {
        var q = sql`INSERT INTO "PersonelProfil" ("ProfilAdi","musteriID") VALUES (${PersonelProfil.ProfilAdi},${PersonelProfil.musteriID})`
        console.log(q)
        const result= await db.query(q);
        //console.log("deneme "+JSON.stringify( deneme))
        return true;
    }
    catch(error) {
        throw error;
    }
}

//-------------------------------------------------------------------------------------------------------
// https://stackoverflow.com/questions/42876071/how-to-save-js-date-now-in-postgresql

async function update(id, PersonelProfil) {
    try {
        var now = new Date().getTime().toString();
        var q = sql`Update "PersonelProfil" Set 
                        "ProfilAdi" = ${PersonelProfil.ProfilAdi},
                        "musteriID" = ${PersonelProfil.musteriID}
                     Where id = ${id}`
        const {rows} = await db.query(q);
    }
    catch(error) {
        throw error;
    }
    return PersonelProfil
}

//-------------------------------------------------------------------------------------------------------

async function remove(id) {
    try {
        var q = sql`Delete FROM "PersonelProfil" Where id = ${id}`
        const result = await db.query(q);
        //console.log("deneme "+JSON.stringify( result))
        return true;
    }
    catch(error) {
        throw error;
    }
}

module.exports ={
    getAll,
    getById,
    create,
    update,
    remove,
    getAllByMusteriId
}