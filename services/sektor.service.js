const sql = require('sql-template-strings');
const db = require('./db');


//-------------------------------------------------------------------------------------------------------

async function getAll() {
    const {rows} = await db.query(sql`
        SELECT * FROM "Sektor" ORDER BY id;`);
    return rows;
}


//-------------------------------------------------------------------------------------------------------

async function getById(id) {
    const result = await db.query(sql`
        SELECT * FROM "Sektor" WHERE id=${id} LIMIT 1;
    `);
    return result.rows
}

//-------------------------------------------------------------------------------------------------------

async function create(Sektor) 
{
    try {
        var q = sql`INSERT INTO "Sektor" ("SektorAdi") 
        VALUES (${Sektor.SektorAdi})`
        const result= await db.query(q);
        return true;
    }
    catch(error) {
        throw error;
    }
}

//-------------------------------------------------------------------------------------------------------
// https://stackoverflow.com/questions/42876071/how-to-save-js-date-now-in-postgresql

async function update(id, Sektor) {
    try {
        var now = new Date().getTime().toString();
        var q = sql`Update "Sektor" Set 
                        "SektorAdi"  = ${Sektor.SektorAdi}
                     Where "id" = ${id}`
        const {rows} = await db.query(q);
    }
    catch(error) {
        throw error;
    }
    return Sektor
}

//-------------------------------------------------------------------------------------------------------

async function remove(id) {
    try {
        var q = sql`Delete FROM "Sektor" Where id = ${id}`
        const result = await db.query(q);
        return true;
    }
    catch(error) {
        throw error;
    }
}

module.exports ={
    getAll,
    getById,
    create,
    update,
    remove
}