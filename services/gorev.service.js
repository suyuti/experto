const sql = require('sql-template-strings');
const db = require('./db');


//-------------------------------------------------------------------------------------------------------

async function getAll() {
    const {rows} = await db.query(sql`
        SELECT * FROM "Gorev" ORDER BY id;`);
    return rows;
}


//-------------------------------------------------------------------------------------------------------

async function getById(id) {
    const result = await db.query(sql`
        SELECT * FROM "Gorev" WHERE id=${id} LIMIT 1;
    `);
    return result.rows
}

//-------------------------------------------------------------------------------------------------------

async function create(Gorev) 
{
    try {
        var q = sql`INSERT INTO "Gorev" ("GorevAdi") VALUES (${Gorev.GorevAdi})`
        console.log(q)
        const result= await db.query(q);
        //console.log("deneme "+JSON.stringify( deneme))
        return true;
    }
    catch(error) {
        throw error;
    }
}

//-------------------------------------------------------------------------------------------------------
// https://stackoverflow.com/questions/42876071/how-to-save-js-date-now-in-postgresql

async function update(id, Gorev) {
    try {
        var now = new Date().getTime().toString();
        var q = sql`Update "Gorev" Set 
                        "GorevAdi" = ${Gorev.GorevAdi}
                     Where id = ${id}`
        const {rows} = await db.query(q);
    }
    catch(error) {
        throw error;
    }
    return Gorev
}

//-------------------------------------------------------------------------------------------------------

async function remove(id) {
    try {
        var q = sql`Delete FROM "Gorev" Where id = ${id}`
        const result = await db.query(q);
        //console.log("deneme "+JSON.stringify( result))
        return true;
    }
    catch(error) {
        throw error;
    }
}

module.exports ={
    getAll,
    getById,
    create,
    update,
    remove
}