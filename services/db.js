const { Pool } = require('pg');
require('dotenv').config()

module.exports = new Pool({
  max: 10,
  //connectionString: process.env.DATABASE_URL
  //connectionString:  'postgresql://localhost:5432/db?user=user&password=pass'
  connectionString: process.env.DB_CONNECTION // 'postgres://user:pass@localhost:5432/dbTest'
  //connectionString:  'postgres://user:pass@192.168.1.47:5432/db'
  //connectionString:  'postgres://znonuxcwhmxlhw:07fd679ab8a863bb45b07bce34195a5442c878d6b39ae8b8967becfc853a3021@ec2-23-21-186-85.compute-1.amazonaws.com:5432/df5f9ki9kp37b6'

});
