const sql = require('sql-template-strings');
const db = require('./db');
const command = require('./command');
const projeService = require('../services/proje.service');
const isPaketleriService = require('../services/isPaketleri.service');
const   adamAyService    = require('../services/adamay.service'); 

//-------------------------------------------------------------------------------------------------------

async function getAll() {

    const { rows } = await db.query(sql`
        SELECT * FROM "Personel" WHERE "Status" = 1 ORDER BY id;`);
    return rows;
}

async function getAllForList() {
    const { rows } = await db.query(sql`
    select P.id, P."Adi", P."Soyadi", M."FirmaAdi", E."EgitimDurumAdi", PP.id as "ProfileId", PP."ProfilAdi",P."Durumu" from "Personel" as P
    left join "Musteri" as M
    on M.id = P."MusteriId"
    left join "EgitimDurumu" as E
    on P."EgitimDurumu" = E.id
    left join "PersonelProfil" as PP
      
    on PP.id = ANY( string_to_array(p."ProfilId", ',')::int[]  )  
    WHERE P."Status" = 1
    ORDER BY P.id;
    `);
 
//  on P."ProfilId"    ILIKE '%' || PP.id || '%'
    return rows;
}

//-------------------------------------------------------------------------------------------------------

async function getAllByMusteriId(musteriId) {
    let { rows } = await db.query(sql`
    select P.id, P."Adi", P."Soyadi",P."SadelestirilmisAdSoyad", M."FirmaAdi", E."EgitimDurumAdi",P."Durumu", PP.id as "ProfileId", PP."ProfilAdi" from "Personel" as P
    left join "Musteri" as M
    on M.id = P."MusteriId"
    left join "EgitimDurumu" as E
    on P."EgitimDurumu" = E.id
    left join "PersonelProfil" as PP
    on PP.id = ANY( string_to_array(p."ProfilId", ',')::int[]  )  
    WHERE P."Status" = 1   and P."MusteriId" = ${musteriId}
    ORDER BY P.id;
    `);
    return rows;
}
async function getAllByMusteriIdPDKS(musteriId) {
    let { rows } = await db.query(sql`
    select P.id, P."Adi", P."Soyadi",P."SadelestirilmisAdSoyad", M."FirmaAdi", E."EgitimDurumAdi",P."Durumu", PP.id as "ProfileId", PP."ProfilAdi" from "Personel" as P
    left join "Musteri" as M
    on M.id = P."MusteriId"
    left join "EgitimDurumu" as E
    on P."EgitimDurumu" = E.id
    left join "PersonelProfil" as PP
    on PP.id = ANY( string_to_array(p."ProfilId", ',')::int[]  )  
    WHERE P."Status" = 1 and P."Durumu" = 1    and P."MusteriId" = ${musteriId}
    ORDER BY P.id;
    `);

  //  , PP.id as "ProfileId", PP."ProfilAdi"

//    left join "PersonelProfil" as PP
//     on P."ProfilId"    ILIKE '%' || PP.id || '%'  
//    var  ProfilId = await db.query(sql`
//     select  * from "PersonelProfil" WHERE  "musteriID" = ${musteriId} ORDER BY id;`);
//     rows.map((item,index)=>{
//         let personelProfilleri=JSON.parse(item.ProfilId)    
//         rows

//         for (let profil_index = 0; profil_index < personelProfilleri.length; profil_index++) {
//             const profil = personelProfilleri[profil_index];
            
            
//         }

        
//     })
    
//console.log(ProfilId.rows)
    
    return rows;
 
}

//-------------------------------------------------------------------------------------------------------

async function getById(id) {
    const result = await db.query(sql`
        SELECT * FROM "Personel" WHERE id=${id} LIMIT 1;
    `); 
    var rows = result.rows.map((item)=>{
        item.ProfilId=`[${item.ProfilId}]`
        item.ProfilId= JSON.parse(item.ProfilId)
        return item
    })
    return rows
}

//-------------------------------------------------------------------------------------------------------

async function create(Personel) {
    Personel.ProfilId=JSON.stringify(Personel.ProfilId)
    Personel.ProfilId=`${Personel.ProfilId}` .replace(/(\[|\])/ig, "") 
    try {

        var q=  sql`SELECT * FROM "Personel" WHERE
        "TcNo" = ${ Personel.TcNo } and
        "SadelestirilmisAdSoyad" = ${command.Pdks_sadelestir(Personel.Adi+ Personel.Soyadi)} and 
        "MusteriId"=${Personel.MusteriId} ORDER BY id`
         var  {rows}  = await db.query(q);
         if (rows.length>0) {
            console.log(  "boyle bir personel vardır" )
            return "boyle bir personel vardır"  
         }

        var defaultmodelQuery = sql`INSERT INTO "Personel" 
                ("Adi", 
                "Soyadi", 
                "EgitimDurumu", 
                "Gorevi", 
                "Durumu", 
                "EkipId", 
                "DepartmanId", 
                "MusteriId", 
                "Status", 
                "ProfilId",
                "UpdatedAt", 
                "UpdatedBy", 
                "CreatedAt", 
                "CreatedBy", 
                "IseBaslamaTarihi", 
                "IstenAyrilmaTarihi",
                "Bolum",
                "Okul",
                "TcNo" ,
                "SadelestirilmisAdSoyad"
                )
        VALUES (
            ${Personel.Adi}, 
            ${Personel.Soyadi}, 
            ${Personel.EgitimDurumu ? Personel.EgitimDurumu : null},
            ${Personel.Gorevi ? Personel.Gorevi : null},
            ${Personel.Durumu ? Personel.Durumu : 1},
            ${Personel.EkipId ? Personel.EkipId : null},
            ${Personel.DepartmanId ? Personel.DepartmanId : null},
            ${Personel.MusteriId}, 
            ${Personel.Status ? Personel.Status : 1},
            ${Personel.ProfilId ? Personel.ProfilId : null},
            ${Personel.UpdatedAt ? Personel.UpdatedAt : null}, 
            ${Personel.UpdatedBy ? Personel.UpdatedBy : null},
            ${Personel.CreatedAt ? Personel.CreatedAt : Date.now()},
            ${Personel.CreatedBy ? Personel.CreatedBy : null}, 
            ${Personel.IseBaslamaTarihi ? Personel.IseBaslamaTarihi : null},
            ${Personel.IstenAyrilmaTarihi ? Personel.IstenAyrilmaTarihi : null},
            ${Personel.Bolum ? Personel.Bolum : null}, 
            ${Personel.Okul ? Personel.Okul : null},
            ${Personel.TcNo ? Personel.TcNo : null},
            ${command.Pdks_sadelestir(Personel.Adi+ Personel.Soyadi)}
              );`;
          await db.query(defaultmodelQuery); 
        
        var q=  sql`SELECT * FROM "Personel" WHERE
        "TcNo" = ${ Personel.TcNo } and
        "SadelestirilmisAdSoyad" = ${command.Pdks_sadelestir(Personel.Adi+ Personel.Soyadi)} and 
        "MusteriId"=${Personel.MusteriId} ORDER BY id`
         var  {rows}  = await db.query(q);
 

        rows = rows.map((item)=>{
            item.ProfilId=`[${item.ProfilId}]`
            item.ProfilId= JSON.parse(item.ProfilId)
            return item
        })

        console.log('başarılı' + rows);


        //silinecek  
        var retunData = await AdamayPersonelEkleme(rows[0]);

        if (typeof retunData == "object" && retunData.length > 0) {
            retunData.map(x => {
                adamAyService.Personel_Proje_AdamAy_create(x);
            });
        }
        return new Promise(function (resolve, reject) {
            if (typeof retunData == "object") {
                resolve(true);
            } else {
                resolve(false)
            }
        })

    }
    catch (error) {
        console.log(error)
        throw error;
    }
}


//-------------------------------------------------------------------------------------------------------

async function update(id, Personel) {
    Personel.ProfilId=JSON.stringify(Personel.ProfilId) 
    Personel.ProfilId=`${Personel.ProfilId}` .replace(/(\[|\])/ig, "") 
    if (Personel.Adi == '' || Personel.Adi == null) {//gerekli alanların kontrolü
        return 'Gerekli Alanlar Eksik'
    }

    try {
        console.log(Personel);

        var QueryString = 'UPDATE "Personel" SET ';
        if (Personel.TcNo) { QueryString += '"TcNo" = \'' + Personel.TcNo + '\','; }
        if (Personel.Adi) { QueryString += '"Adi" = \'' + Personel.Adi + '\','; }
        if (Personel.Soyadi) { QueryString += '"Soyadi" = \'' + Personel.Soyadi + '\','; }
        if (Personel.Adi || Personel.Soyadi) { QueryString += '"SadelestirilmisAdSoyad" = \'' + command.Pdks_sadelestir(Personel.Adi+ Personel.Soyadi) + '\','; }
        if (Personel.Okul) { QueryString += '"Okul" = \'' + Personel.Okul + '\','; }
        if (Personel.EgitimDurumu) { QueryString += '"EgitimDurumu" = \'' + Personel.EgitimDurumu + '\','; }
        if (Personel.Gorevi) { QueryString += '"Gorevi" = \'' + Personel.Gorevi + '\','; }
        if (Personel.IseBaslamaTarihi) { QueryString += '"IseBaslamaTarihi" = \'' + Personel.IseBaslamaTarihi + '\','; }
        if (Personel.IstenAyrilmaTarihi) { QueryString += '"IstenAyrilmaTarihi" = \'' + Personel.IstenAyrilmaTarihi + '\','; }
        if (Personel.Durumu) { QueryString += '"Durumu" = \'' + Personel.Durumu + '\','; }
        if (Personel.EkipId) { QueryString += '"EkipId" = \'' + Personel.EkipId + '\','; }
        if (Personel.DepartmanId) { QueryString += '"DepartmanId" = \'' + Personel.DepartmanId + '\','; }
        if (Personel.MusteriId) { QueryString += '"MusteriId" = \'' + Personel.MusteriId + '\','; }
        if (Personel.CreatedAt) { QueryString += '"CreatedAt" = \'' + Personel.CreatedAt + '\','; }
        if (Personel.CreatedBy) { QueryString += '"CreatedBy" = \'' + Personel.CreatedBy + '\','; }
        //if(Personel.UpdatedAt)          {QueryString += '"UpdatedAt" = \''+  Personel.UpdatedAt+'\',';}
        if (Personel.UpdatedBy) { QueryString += '"UpdatedBy" = \'' + Personel.UpdatedBy + '\','; }
        if (Personel.Status) { QueryString += '"Status" = \'' + Personel.Status + '\','; }
        if (Personel.ProfilId) { QueryString += '"ProfilId" = \'' + Personel.ProfilId + '\','; }

        QueryString += '"UpdatedAt" = \'' + Date.now() + '\' WHERE  "id" = ' + id;


        console.log(QueryString);

        //var q = sql`Update "Personel" Set "FirmaAdi" = ${Personel.FirmaAdi} Where id = ${Personel.id}`
        //var dede = sql`+QueryString+`
        const { rows } = await db.query(QueryString);


        return true;

    }
    catch (error) {
        throw error;
    }
    return Personel
}

//-------------------------------------------------------------------------------------------------------

async function remove(id) {
    try {
        var q = sql`Update "Personel" Set "Status" = '0' Where id = ${id}` 
        await adamAyService.removeByPersonel(id);
        const { rows } = await db.query(q);
        return true
    }
    catch (error) {
        throw error;
    }
}

async function getByMusteriFromProfilId(musteriId, ProfilId,EkipId) {
    var TEMP_ProfilId =ProfilId.split(",")
    try { 
        var profil ="("
    for (let index = 0; index < TEMP_ProfilId.length; index++) { 
        profil +=` (${TEMP_ProfilId[index]} = ANY( string_to_array("ProfilId", ',')::int[]  )  )`
        if (index!==TEMP_ProfilId.length-1) {
            profil +=" OR "  
        }
    }
    profil +=" ) " 
        var q =  `SELECT * FROM "Personel" WHERE "MusteriId" = ${musteriId} ${EkipId? "AND  \"EkipId\"='"+EkipId +"'":""}  AND ${profil}    AND "Status"= '1' ORDER BY id;`;
       
        const { rows } = await db.query(q);

        return rows;
    }
    catch (error) {
        throw error;
    }


}

async function AdamayPersonelEkleme(personel) {
    //personel["id"]=8
    //console.log(personel);
    personel.IseBaslamaTarihi.setDate(8);
    var aylikSonuclar = []
    var Personeller = []
    var Projeler = []
    var IsPaketleri = []
    var AdamAyList = []

    
    // musterinin projeleri alinir
    Projeler = await projeService.getAllByMusteriId(personel.MusteriId);

    if (personel.EkipId) { /// personelin ekibine uygun projeleri filtreler
        Projeler=Projeler.filter(item=>{return  item.EkipId == personel.EkipId})  
    }

    // projelerin is paketleri alinir
    for (var pKey in Projeler) {
        var ip = await isPaketleriService.getByProjeId(Projeler[pKey].id)
        IsPaketleri = IsPaketleri.concat(ip)
    }

    IsPaketleri = IsPaketleri.map((item) => {
        // item.BaslangicTarihi = new Date(item.BaslangicTarihi);
        // item.BitisTarihi = new Date(item.BitisTarihi);
        item.BaslangicTarihi.setDate(8);
        item.BitisTarihi.setDate(8); 
        return item
    });

     

    IsPaketleri = IsPaketleri.filter((item) => { 
        //personelin işe başlama tarihinden sonraki veya o tarin içinde oldugu iş paketlerinin filtreleme işlemi
        var a   =  personel.IseBaslamaTarihi <=  item.BaslangicTarihi 
        var b   =  item.BaslangicTarihi <   personel.IseBaslamaTarihi  &&  personel.IseBaslamaTarihi <= item.BitisTarihi
       // aynı profilde oldugunun kontrolü
       // var c   =  item.ProfilId == personel.ProfilId   

        let c = item.ProfilId.filter(x => personel.ProfilId.includes(x)); 
        c = c.length !== 0
        return   ((a||b)&& c ) 
    });

    for (let indexIspaketi = 0; indexIspaketi < IsPaketleri.length; indexIspaketi++) {
        const element_Ispaketi = JSON.parse(JSON.stringify(IsPaketleri[indexIspaketi])) ;
        element_Ispaketi.BaslangicTarihi=new Date(element_Ispaketi.BaslangicTarihi)
        element_Ispaketi.BitisTarihi=new Date(element_Ispaketi.BitisTarihi)
        var BaslangicTarihi = element_Ispaketi.BaslangicTarihi,
            BitisTarihi = element_Ispaketi.BitisTarihi
           // console.clear();

            let sabitOran = AdamAyList.filter(function (adamay) {
                return adamay.Yil === BaslangicTarihi.getFullYear() &&
                    adamay.Ay === BaslangicTarihi.getMonth() + 1 &&
                    adamay.readOnly === 1})
            .map(function (newPersonel)       {   return newPersonel.Oran;})
            .reduce(function (toplam, oran)   {   return toplam + oran; }, 0);

            let DegiskenOranCount = IsPaketleri.filter(function (item) { 
                var bas=item.BaslangicTarihi.getTime()
                var bit=item.BitisTarihi.getTime()
                var comB =BaslangicTarihi.getTime() ;
                var comS =BitisTarihi   
                var a = bas <= comB && comB <= bit; 
                var b=  comB <= bas &&  bas <=  comS        
                return   (a ||b)   
            })

            var oran = Math.floor((10 - sabitOran) / (DegiskenOranCount.length-indexIspaketi ) ) 

 
       var ilk=0
        for (var d = BaslangicTarihi; d <= BitisTarihi; d.setMonth(d.getMonth() + 1)) {   
            if(d <   personel.IseBaslamaTarihi){continue} 
            let newAdamAy = {
                Yil: d.getFullYear(),
                Ay: d.getMonth() + 1,
                Oran: oran,
                MusteriId: personel.MusteriId,
                PersonelId: personel.id,
                ProjeId: element_Ispaketi.ProjeId,
                IsPaketiId: element_Ispaketi.id,
                "manualUpdateDate": null,
                "createDate": new Date(),
                "readOnly": 1,
                "manualUpdate": null,
                "IsPaketiColor": element_Ispaketi.IsPaketiColor
            }   
            // if(ilk==0){
            //     newAdamAy.readOnly=null; 
            //     ilk++
            // } 
            AdamAyList.push(newAdamAy) ;
        }
    } 

    ///adamaydakigibi çalışmasını istiyorsak bu if şartını kaldırın. bu kop projelerin iş paketi çakışmalarındankaynaklı hataları örtbasetmek için eklenmiştir
    //hata durumu meydana geldiginde ek frontent düzenlemesi eklenmesi gerkmektedir.şimdilik bu dğzenleme olmadıgı için bu şart eklenmiştir.
    if(true){   
        AdamAyList= AdamAyList.map((item)=>{
            item.readOnly=null
            return item
        })
    }
// return  AdamAyList 

 
    if(IsPaketleri.length==0){
        return  [] 
    } 
    else if (AdamAyList && AdamAyList.length>0) {
        return AdamAyList 
    } else {
        await remove(personel.id);
        return 'Bir sıkıntı var...' 
    } 

}

module.exports = {
    getAll,
    getById,
    create,
    update,
    remove,
    getAllByMusteriId,
    getAllByMusteriIdPDKS,
    getAllForList,
    getByMusteriFromProfilId
}