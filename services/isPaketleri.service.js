const sql = require('sql-template-strings');
const db = require('./db');

 
//-------------------------------------------------------------------------------------------------------

async function getAll() {
    const { rows } = await db.query(sql`
        SELECT * FROM "IsPaketleri" ORDER BY "id";`);

        for (let index = 0; index < rows.length; index++) {
          rows[index].CalisacakProfilListesi =JSON.parse( rows[index].CalisacakProfilListesi );
        }
        console.log(rows)
    return rows;
}

 
//-------------------------------------------------------------------------------------------------------

async function getById(id) {
    const result = await db.query(sql`
        SELECT * FROM "IsPaketleri" WHERE "id"=${id} LIMIT 1;
    `);

    for (let index = 0; index < result.rows.length; index++) {
        result.rows[index].CalisacakProfilListesi =JSON.parse( result.rows[index].CalisacakProfilListesi );
      }
    return result.rows
}

//-------------------------------------------------------------------------------------------------------

async function create(IsPaketleri) {
    try {
        console.log("Ispaketi.Create")
        IsPaketleri.ProfilId=JSON.stringify(IsPaketleri.ProfilId)
        IsPaketleri.ProfilId=`${IsPaketleri.ProfilId}` .replace(/(\[|\])/ig, "") 

        const { rows } = await db.query(sql`
        SELECT * FROM "IsPaketleri" WHERE  
        "ProjeId"=${IsPaketleri.ProjeId}  ORDER BY "IsPaketiColor";`); 
        let IsPaketlerirows=rows;

        IsPaketleri.BaslangicTarihi = new Date(IsPaketleri.BaslangicTarihi); 

        IsPaketleri.IsPaketiColor = 1 ;
        for (let index = 0; index < IsPaketlerirows.length; index++) { 
           if( IsPaketlerirows[index].BaslangicTarihi < IsPaketleri.BaslangicTarihi) {
            IsPaketleri.IsPaketiColor=index+2; 
           } else{
            IsPaketlerirows[index].IsPaketiColor++
            update(IsPaketlerirows[index].id, IsPaketlerirows[index])
           }
        }   

        var q = sql`INSERT INTO "IsPaketleri" 
            (
            "Adi", 
            "Aciklama", 
            "ProjeId", 
            "BaslangicTarihi",
            "BitisTarihi",
            "ProfilId",
            "IsPaketiColor" )
        VALUES (
            ${IsPaketleri.Adi ? IsPaketleri.Adi : ''}, 
            ${IsPaketleri.Aciklama ? IsPaketleri.Aciklama : ''}, 
            ${IsPaketleri.ProjeId ? IsPaketleri.ProjeId : null},
            ${IsPaketleri.BaslangicTarihi ? IsPaketleri.BaslangicTarihi : null},
            ${IsPaketleri.BitisTarihi ? IsPaketleri.BitisTarihi : null},
            ${IsPaketleri.ProfilId ? IsPaketleri.ProfilId : null},
            ${IsPaketleri.IsPaketiColor ? IsPaketleri.IsPaketiColor : null}
                )`;

        console.log(q)
          await db.query(q);

        const result = await db.query(sql`
            SELECT * FROM "IsPaketleri" WHERE 
            "Adi"=${IsPaketleri.Adi} AND 
            "Aciklama"=${IsPaketleri.Aciklama} AND
            "ProjeId"=${IsPaketleri.ProjeId} AND
            "BaslangicTarihi"=${IsPaketleri.BaslangicTarihi} AND
            "BitisTarihi"=${IsPaketleri.BitisTarihi} AND
            "ProfilId"=${IsPaketleri.ProfilId} 
            limit 1 ;
        `); 


        console.log('ok')
        //console.log("deneme "+JSON.stringify( deneme))
        return result.rows[0];
    }
    catch (error) {
        console.log(error)
        throw error;
    }
}

//-------------------------------------------------------------------------------------------------------
// https://stackoverflow.com/questions/42876071/how-to-save-js-date-now-in-postgresql

async function update(id, IsPaketleri) {

    try {
        var now = new Date().getTime().toString();
        var q = sql`Update "IsPaketleri" Set 
                        "Adi" = ${IsPaketleri.Adi},
                        "Aciklama" = ${IsPaketleri.Aciklama}, 
                        "BaslangicTarihi" = ${IsPaketleri.BaslangicTarihi},
                        "BitisTarihi" = ${IsPaketleri.BitisTarihi}, 
                        "IsPaketiColor" = ${IsPaketleri.IsPaketiColor}, 
                        "CalisacakProfilListesi" = ${JSON.stringify(IsPaketleri.CalisacakProfilListesi)}
                     Where "id" = ${id}`
        const { rows } = await db.query(q);
    }
    catch (error) {
        throw error;
    }
    return IsPaketleri
}

//-------------------------------------------------------------------------------------------------------

async function remove(id) {
    try { 
        const getIsPaketi = await db.query(sql`SELECT * FROM "IsPaketleri" WHERE "id"=${id} LIMIT 1;`);
        //getIsPaketi.rows[0].ProjeId
        var q = sql`Delete FROM "Personel_Proje_AdamAy" Where  "ProjeId"=${getIsPaketi.rows[0].ProjeId} AND  "IsPaketiId" = ${id}`
        const resultDeletePersonel_Proje_AdamAy = await db.query(q);

        let IsPaketi =getIsPaketi.rows[0]

        const { rows } = await db.query(sql`
        SELECT * FROM "IsPaketleri" WHERE  
        "ProjeId"=${IsPaketi.ProjeId}  ORDER BY "IsPaketiColor";`); 
        let IsPaketlerirows=rows;
        
        IsPaketi.BaslangicTarihi = new Date(IsPaketi.BaslangicTarihi); 

        for (let index = 0; index < IsPaketlerirows.length; index++) { 
           if( IsPaketlerirows[index].BaslangicTarihi < IsPaketi.BaslangicTarihi) {
           
           } else{
            IsPaketlerirows[index].IsPaketiColor--
            update(IsPaketlerirows[index].id, IsPaketlerirows[index])
           }
        }

        var q = sql`Delete FROM "IsPaketleri" Where "id" = ${id}`
        const resultDeleteIsPaketleri = await db.query(q);
        //console.log("deneme "+JSON.stringify( result))
        return true;
    }
    catch (error) {
        console.log(error)
        throw error;
    }
}


//-------------------------------------------------------------------------------------------------------

async function getByProjeId(ProjeId) {
    const result = await db.query(sql`
        SELECT * FROM "IsPaketleri" WHERE "ProjeId"=${ProjeId};
    `);

    var rows = result.rows.map((item)=>{

        item.ProfilId=`[${item.ProfilId}]`
        item.ProfilId= JSON.parse(item.ProfilId)
        return item
    })
    return rows
}
//-------------------------------------------------------------------------------------------------------

async function getByMounthAllIsPaketi(satartDate,endDate) {
    const result = await db.query(sql` 
        SELECT * FROM "IsPaketleri" WHERE   "BaslangicTarihi" >= ${satartDate} and "BaslangicTarihi" < ${endDate}  ;
    `); 
    return result.rows
}
 
async function getIsPaketiId(ProjeId) {
    const { rows } = await db.query(sql` 
        SELECT * FROM "IsPaketleri" WHERE   "ProjeId"=${ProjeId} ;
    `); 
    return rows.length;
}
 
async function checkByDateIsPaketiAndCreate(isPaketi) {
    let ispaketiId=null;
    let IsPaketiColor=null;

    ////**/*/*/*/*/*/*/*/*/*/*/*/*////// */
    const { rows } = await db.query(sql` 
        SELECT * FROM "IsPaketleri" WHERE 
        (
            (${isPaketi.BaslangicTarihi}<="BaslangicTarihi" AND "BaslangicTarihi" <= ${isPaketi.BitisTarihi})
            OR
            ("BaslangicTarihi"<=${isPaketi.BaslangicTarihi} AND ${isPaketi.BitisTarihi} <= "BitisTarihi")
            or
            (${isPaketi.BaslangicTarihi} <= "BitisTarihi" AND "BitisTarihi" <= ${isPaketi.BitisTarihi})
            or
            ( ${isPaketi.BaslangicTarihi}<="BaslangicTarihi" AND "BitisTarihi" <= ${isPaketi.BitisTarihi})
        )
        
        
        AND    "ProjeId"=${isPaketi.ProjeId} ORDER BY "IsPaketiColor" ;
    `); 

    if(rows.length==0){


        let getIspaketi=  await create(isPaketi)
        ispaketiId=getIspaketi.id
        IsPaketiColor=getIspaketi.IsPaketiColor
    }

    return  [ ispaketiId ,IsPaketiColor ]
}




module.exports = {
    getAll,
    getById,
    create,
    update,
    remove,
    getByProjeId,
    getByMounthAllIsPaketi,
    getIsPaketiId,
    checkByDateIsPaketiAndCreate
}