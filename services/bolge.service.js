const sql = require('sql-template-strings');
const db = require('./db');


//-------------------------------------------------------------------------------------------------------

async function getAll() {
    console.log('getAll');
    const {rows} = await db.query(sql`
        SELECT * FROM "Bolge" ORDER BY "id";`);
    return rows;
}


//-------------------------------------------------------------------------------------------------------

async function getById(id) {
    console.log('getById :',id);
    const result = await db.query(sql`
        SELECT * FROM "Bolge" WHERE "id"=${id} LIMIT 1;
    `);
    return result.rows
}

//-------------------------------------------------------------------------------------------------------

async function create(Bolge) 
{
    console.log('create :')
    try {
        var q = sql`INSERT INTO "Bolge" ("BolgeAdi") 
        VALUES (${Bolge.BolgeAdi})`
        const result= await db.query(q);
        return true;
    }
    catch(error) {
        throw error;
    }
}

//-------------------------------------------------------------------------------------------------------
// https://stackoverflow.com/questions/42876071/how-to-save-js-date-now-in-postgresql

async function update(id, Bolge) {
    console.log('getById :',id);
    try {
        var now = new Date().getTime().toString();
        var q = sql`Update "Bolge" Set 
                        "BolgeAdi"  = ${Bolge.BolgeAdi}
                     Where "id" = ${id}`
        const {rows} = await db.query(q);
    }
    catch(error) {
        throw error;
    }
    return Bolge
}

//-------------------------------------------------------------------------------------------------------

async function remove(id) {
    console.log('remove :',id);
    try {
        var q = sql`Delete FROM "Bolge" Where "id" = ${id}`
        const result = await db.query(q);
        return true;
    }
    catch(error) {
        throw error;
    }
}

module.exports ={
    getAll,
    getById,
    create,
    update,
    remove
}