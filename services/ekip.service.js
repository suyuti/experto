const sql = require('sql-template-strings');
const db = require('./db');


//-------------------------------------------------------------------------------------------------------

async function getAll() {
    const {rows} = await db.query(sql`
        SELECT * FROM "Ekip" ORDER BY id;`);
    return rows;
}


//-------------------------------------------------------------------------------------------------------

async function getById(id) {
    const result = await db.query(sql`
        SELECT * FROM "Ekip" WHERE id=${id} LIMIT 1;
    `);
    return result.rows
}

//-------------------------------------------------------------------------------------------------------

async function create(Ekip) 
{
    try {
        var q = sql`INSERT INTO "Ekip" ("EkipAdi","MusteriId") 
        VALUES (${Ekip.EkipAdi},${Ekip.MusteriId})`
        console.log(q)
        const result= await db.query(q);
        //console.log("deneme "+JSON.stringify( deneme))
        return true;
    }
    catch(error) {
        throw error;
    }
}

//-------------------------------------------------------------------------------------------------------
// https://stackoverflow.com/questions/42876071/how-to-save-js-date-now-in-postgresql

async function update(id, Ekip) {
    try {
        var now = new Date().getTime().toString();
        var q = sql`Update "Ekip" Set 
                        "EkipAdi"       = ${Ekip.EkipAdi},
                        "MusteriId"   = ${Ekip.MusteriId}
                     Where id = ${id}`
        const {rows} = await db.query(q);
    }
    catch(error) {
        throw error;
    }
    return Ekip
}

//-------------------------------------------------------------------------------------------------------

async function remove(id) {
    try {
        var q = sql`Delete FROM "Ekip" Where id = ${id}`
        const result = await db.query(q);
        //console.log("deneme "+JSON.stringify( result))
        return true;
    }
    catch(error) {
        throw error;
    }
}

//-------------------------------------------------------------------------------------------------------

async function getEkipByMusteriId(id) {
    try {
        var q = sql`SELECT * FROM "Ekip" Where "MusteriId" = ${id}`
        const {rows} = await db.query(q); 
        return rows;
    }
    catch(error) {
        throw error;
    }  
}

module.exports ={
    getAll,
    getById,
    create,
    update,
    remove,
    getEkipByMusteriId
}