const config = require('../config.json');
const jwt = require('jsonwebtoken');
var crypto = require('crypto');
const userService = require('./user.service');
const db = require('./db');
const sql = require('sql-template-strings');
const personelService = require('./personel.service');

var buffer = require('buffer');
var path = require('path');
var fs = require('fs');

var XLSX = require('xlsx');

Date.prototype.monthDays = function () {
    var d = new Date(this.getFullYear(), this.getMonth() + 1, 0);
    return d.getDate();
}

Date.prototype.ConvertMonth = function () {
    //sadece yıl ve ay bilgisini aya cevitrip gönderir
    var d = ((this.getFullYear() - 1970) * 12) + this.getMonth() + 1
    return d
}

Date.prototype.sonHaftaCumartesiVarMi = function () {
    var firstWeekday = new Date(this.getFullYear(), this.getMonth() + 1, 0).getDay();
    if (firstWeekday == 0 || firstWeekday == 6) { return true }
    else { return false }
}

Date.prototype.getWeekOfMonth = function () {
    var firstWeekday = new Date(this.getFullYear(), this.getMonth(), 1).getDay();
    var offsetDate = this.getDate() + firstWeekday - 1;
    return Math.ceil(offsetDate / 7);
}

Date.prototype.cumartesiVarMi = function () {
    var day = this.monthDays()
    var d = new Date(this.getFullYear(), this.getMonth(), day);
    var x = d.getWeekOfMonth();
    return x;

}


String.prototype.haftaSonuMu = function () {
    var str = this.trim();
    var arr = str.split(/[:./]/);
    var d = new Date(arr[2], arr[1] - 1, arr[0]);
    if (d.getDay() == 0 || d.getDay() == 6)//haftasonu
    {
        return true
    } else {
        return false
    }

}

String.prototype.saatiDakikayaCevir = function () {
    var str = this.trim();
    var arr = str.split(/[:./]/);
    return (parseInt(arr[0]) * 60) + parseInt(arr[1])
}

Number.prototype.dakikayiSaateCevir = function () {
    var dakika =this.valueOf()
    var saat = Math.floor(dakika /60);  
    dakika = dakika % 60
 
    var s = "0" + saat; 
 
    var d  = "0" + dakika;

    return  s.substr(-2) + ":" + d.substr(-2);
 
}


Date.prototype.TimezOlmadanGetir = function () {
    var d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDay()));
    return d
}


String.prototype.convertTrDateToGlobalDate = function () {
    var str = this.trim();
    var arr = str.split(/[:./]/);
    arr[2] = "20" + arr[2]
    var d = new Date(Date.UTC(arr[2].substr(-4), arr[1] - 1, arr[0]));
    return d
}

Date.prototype.haftadaKacGunVarHaftasonuHaric = function () {
    var haftadaKacGunVar = {};
    for (i = 1; i <= this.monthDays(); i++) {
        var d = new Date(this.getFullYear(), this.getMonth(), i);
        var x = d.getWeekOfMonth()
        var n = d.getDay()
        if (n !== 0 && n !== 6) {
            if (haftadaKacGunVar[x] == undefined) {
                haftadaKacGunVar[x] = 0;
            }
            haftadaKacGunVar[x] = haftadaKacGunVar[x] + 1
        }
    }
    return haftadaKacGunVar;
}

Date.prototype.aydaKacCumartesiVar = function () {
    var d = new Date(this.getFullYear(), this.getMonth() + 1, 0)
    var n = d.getDate() // bu aydaki toplam gün sayısı 
    var kacicciGunu = d.getDay()
    var count = 0 
    if (kacicciGunu < 6) {
        n = n - (kacicciGunu + 1)
    } else if (kacicciGunu > 6) {
        n = n - 1
    }
    for (i = n; i > 0; i = i - 7) {
        count++
    }
    return count;
}


function encode_base64(filename) {
    fs.readFile(path.join(__dirname + "/../", '/public/', filename), function (error, data) {
        if (error) {
            throw error;
        } else {
            var buf = Buffer.from(data);
            var base64 = buf.toString('base64');
            console.log('File convert to base64 string! ==> ' + base64);
            //console.log('Base64 of ddr.jpg :' + base64);
            return base64;
        }
    });
}


function decode_base64(base64str, filename) {
    var buf = Buffer.from(base64str, 'base64');
    console.log(__dirname);

    fs.writeFile(path.join(__dirname + "/../", 'public/', filename), buf, function (error) {
        if (error) {
            throw error;
        } else {
            // console.log('File created from base64 string! ==> '+ __dirname+"/../",'public/',filename);
            return true;
        }
    });

}

function get_From_base64_To_xmlsData(base64str) {
    data = base64str.split(";base64,")[1];
    var read_opts = { type: 'base64' }
    var wb = XLSX.read(data, read_opts)
    return wb
}



var Pdks_sadelestir = function Pdks_sadelestir(data1) {
    //function Pdks_sadelestir(data1) {
    if (data1 == "" || data1 == undefined) { return "" }
    data1 = data1.replace(":", "");
    data1 = data1.replace("Ö", "o");
    data1 = data1.replace("Ç", "c");
    data1 = data1.replace("Ş", "s");
    data1 = data1.replace("İ", "i");
    data1 = data1.replace("Ğ", "g");
    data1 = data1.replace("Ü", "u");
    data1 = data1.replace("ğ", "g");
    data1 = data1.replace("ü", "u");
    data1 = data1.replace("ı", "i");
    data1 = data1.replace("ş", "s");
    data1 = data1.replace("ç", "c");
    data1 = data1.replace("ö", "o");
    data1 = data1.replace(/\s/g, "");
    data1 = data1.toLowerCase();
    data1 = data1.replace("ğ", "g");
    data1 = data1.replace("ü", "u");
    data1 = data1.replace("ı", "i");
    data1 = data1.replace("ş", "s");
    data1 = data1.replace("ç", "c");
    data1 = data1.replace("ö", "o");
    return data1.trim();
}
exports.Pdks_sadelestir = Pdks_sadelestir;


function gunSaatCıkart(tarih1, tarih2) {
    tarih1 = tarih1.split(":");
    tarih2 = tarih2.split(":");
    //Date(year, month, day, hours, minutes, seconds, milliseconds);
    tarih1[0] = tarih1[0] ? tarih1[0] : 0
    tarih1[1] = tarih1[1] ? tarih1[1] : 0
    tarih1[2] = tarih1[2] ? tarih1[2] : 0

    tarih2[0] = tarih2[0] ? tarih2[0] : 0
    tarih2[1] = tarih2[1] ? tarih2[1] : 0
    tarih2[2] = tarih2[2] ? tarih2[2] : 0
    tarih1 = new Date(0, 0, 12, tarih1[0], tarih1[1], tarih1[2]); // 2014 Yılının 7. Ayının 12. Günü Saat 9:00 AM
    tarih2 = new Date(0, 0, 12, tarih2[0], tarih2[1], tarih2[2]); // 2014 Yılının 7. Ayının 13. Günü Saat öğleden sonra 7:00 PM (19.00)

    var t = tarih2 - tarih1;

    var cm = 60 * 1000, //dakika
        ch = 60 * 60 * 1000,  //saat
        //d = Math.floor(t / cd), 
        h = '0' + Math.floor(t / ch),
        m = '0' + Math.round((t - (h * ch)) / cm);

    //return [  h , m ].join(':');
    return [h.substr(-2), m.substr(-2)].join(':');
}


// function gunSaatCıkart(tarih1, tarih2) {
//     tarih1 = tarih1.split(":");
//     tarih2 = tarih2.split(":");
//     tarih1 = new Date(0, 0, 12, tarih1[0], tarih1[1], tarih1[2]); // 2014 Yılının 7. Ayının 12. Günü Saat 9:00 AM
//     tarih2 = new Date(0, 0, 12, tarih2[0], tarih2[1], tarih2[2]); // 2014 Yılının 7. Ayının 13. Günü Saat öğleden sonra 7:00 PM (19.00)

//     var t = tarih2 - tarih1;

//     var cd = 24 * 60 * 60 * 1000,
//         ch = 60 * 60 * 1000,
//         d = Math.floor(t / cd),
//         h = '0' + Math.floor((t - d * cd) / ch),
//         m = '0' + Math.round((t - d * cd - h * ch) / 60000);
//     return [d, h.substr(-2), m.substr(-2)].join(':');
// }

function gunSaatTopla(tarih1, tarih2) {
    // tarih1 = tarih1.split(":");
    // tarih2 = tarih2.split(":"); 
    // var gun =parseInt(tarih1[0]) + parseInt(tarih2[0])
    // var saat =parseInt(tarih1[1]) + parseInt(tarih2[1])
    // var dakika =parseInt(tarih1[2]) + parseInt(tarih2[2])

    // saat=saat + Math.floor( dakika/60 )
    // dakika = dakika%60 

    // gun =gun + Math.floor(saat/24 ) 
    // saat = saat % 24
    // gun=gun.toString()
    // saat='0' +saat.toString()
    // dakika='0' +dakika.toString() 
    // return [gun, saat.substr(-2), dakika.substr(-2)].join(':'); 
    tarih1 = tarih1.split(":");
    tarih2 = tarih2.split(":");


    var dakika = parseInt(tarih1[1]) + parseInt(tarih2[1])
    var saat = parseInt(tarih1[0]) + parseInt(tarih2[0])
    saat = saat + Math.floor(dakika / 60)
    dakika = dakika % 60

    saat = '0' + saat.toString()
    dakika = '0' + dakika.toString()
    return [saat.substr(-2), dakika.substr(-2)].join(':');
}
function gunSaatcevirDakika(tarih1, ) {
    tarih1 = tarih1.split(":");
    var sonuc = (parseInt(tarih1[0]) * 24 * 60) + (parseInt(tarih1[1] * 60)) + (parseInt(tarih1[2]))
    return sonuc
}

// function gunSaatKarsılastır(tarih1, tarih2) {
//     tarih1 = tarih1.split(":");
//     tarih2 = tarih2.split(":");
//     tarih1 = new Date(0, 0, tarih1[0], tarih1[1], tarih1[2], 12); // 2014 Yılının 7. Ayının 12. Günü Saat 9:00 AM
//     tarih2 = new Date(0, 0, tarih2[0], tarih2[1], tarih2[2], 12); // 2014 Yılının 7. Ayının 13. Günü Saat öğleden sonra 7:00 PM (19.00)
//     if (tarih1 > tarih2) {
//         return 1
//     } else if (tarih1 == tarih2) {
//         return 0
//     } else {
//         return -1
//     }
// }


function gunSaatKarsılastır(tarih1, tarih2) {
    tarih1 = tarih1.split(":");
    tarih2 = tarih2.split(":");
    tarih1 = new Date(0, 0, 1, tarih1[0], tarih1[1], 12); // 2014 Yılının 7. Ayının 12. Günü Saat 9:00 AM
    tarih2 = new Date(0, 0, 1, tarih2[0], tarih2[1], 12); // 2014 Yılının 7. Ayının 13. Günü Saat öğleden sonra 7:00 PM (19.00)
    if (tarih1 > tarih2) {
        return 1
    } else if (tarih1 == tarih2) {
        return 0
    } else {
        return -1
    }
}


function haftalikKontrolSaati(sayi) {
    return sayi * 9 * 60
    // var ss
    //     switch (sayi) {
    //         case 1:
    //             ss =   9*60;
    //             break;
    //         case 2:
    //             ss =   18 *60;
    //             break;
    //         case 3:
    //             ss = (1 * 60*60 )+ (3 *60) ;
    //             break;
    //         case 4:
    //             ss =  (1 * 60*60 )+ (12 *60);
    //             break;
    //         case 5:
    //             ss =  (1 * 60*60 )+ (21 *60);
    //             break; 
    //     }
    // return ss;
}
function haftalikKontrolSaatininYarisi(sayi) {
    var ss
    switch (sayi) {
        case 1:
            ss = "0:09:00";
            break;
        case 2:
            ss = "0:09:00";
            break;
        case 3:
            ss = "0:18:00";
            break;
        case 4:
            ss = "0:18:00";
            break;
        case 5:
            ss = "0:27:00";
            break;
    }
    return ss;
}

function AlanKontrol(model, alanlar) {
    for (let index = 0; index < alanlar.length; index++) {
        const element = alanlar[index];
        if (!model[alanlar[index]]) {
            return false;
        }
    }
}

let publicUrl = [
    "/users/authenticate"
]

function TokenKontrol(req, res, next) {
    //public url kontrolü
    for (let index = 0; index < publicUrl.length; index++) {
        if (publicUrl[index] == req.url) {
            next();
            return;
        }
    }

    let token = req.headers['x-access-token'] || req.headers['authorization'];
    if (!token) {
        return res.json({
            success: false,
            message: 'Token YOK Lütfen Giriş Yapınız'
        });
    }

    /*// Express headers are auto converted to lowercase
    if (token.startsWith('Bearer ')) {
        // Remove Bearer from string
        token = token.slice(7, token.length);
    }*/

    if (token) {
        jwt.verify(token, config.secret, (err, decoded) => {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Token Geçerli Degil Lütfen Tekrar Giriş Yapınız.'
                });
            } else {

                userService.getById(decoded.userId)
                    .then(user => {
                        if (user) {
                            req.TokenUser = user[0];
                            req.decoded = decoded;
                            next();
                        }
                        else {
                            res.json({ success: false, message: 'Token Geçerli Kullanıcı Bulununamadı Lütfen bilgilerinizi Kontrol Ediniz.' })
                        }
                    })
                    .catch(err => next(err));
            }
        });
    }
    else {
        return res.json({
            success: false,
            message: 'Token YOK Lütfen Giriş Yapınız'
        });
    }
}


function IzinKontrol(req, res, next) {
    console.log('izin kontrol')
    if (req.decoded) {

        // userService.getUserPermissions(req.TokenUser.id)
        /*  
          userService.getUserPermissions(req.TokenUser.id).then(data => {
              if (data.length) {
  
                  res.json(returnTemplate(data, ""))
              } else {
                  res.json(returnTemplate([], "Kayıt bulunamamıştır"))
              }
          }).catch(err => next(err));
  
  */



    }

    /*
    userService.getUserPermissions(req.TokenUser.id)
      .then(user => 
          
          user ?req.TokenUser = user : res.json({ success: false, message: 'Token Geçerli Kullanıcı Bulununamadı Lütfen bilgilerinizi Kontrol Ediniz.' })
  
        ).catch(err => next(err));
  
    console.log(req.TokenUser);
    req.decoded = decoded;
  
  
  if (token) {      
      
  } else {
    return res.json({
      success: false,
      message: 'BU servis için yetkilendirmeniz bulunmamaktadır.Lütfen yöneticinize başvurun.'
    });
  }
  */
    next()
}

function pathToRegexConverter(value) {
    // var str = "/proje/:id/ispaketi/:id/";  böyle gelmesi gerekiyor
    var str = value;
    var patt = /[:][a-z]*[/]/g;
    str = str.replace(patt, "[0-9]*/");
    patt = /[:][a-z]*/g;
    str = str.replace(patt, "[0-9]*");
    patt = /[/]/g;
    str = str.replace(patt, "\\/");
    return new RegExp(str, "g");

}

function pathToRegexCompare(path, regexPath) {
    /// path = "/proje/244/ispaketi/212";  örnek path
    /// regexPath = "/proje/:pid/ispaketi/:id";  örnek regexPath
    var result = path.match(pathToRegexConverter(regexPath))
    return result == path;
}




function databaseConnection() {
    // console.log(db);

    db.connect(function (err, client, done) {
        if (err) {
            console.log("Veritabanına baglanılamadı \n" + err);
        }
    })
}


function ResHeader(req, res, next) {
    console.log("ResHeader");
    // res.set({ 'content-type': 'application/json; charset=utf-8' });
    /*const head = {
     'Content-Range': `bytes ${start}-${end}/${fileSize}`,
     'Accept-Ranges': 'bytes',
     'Content-Length': chunksize,
     'Content-Type': 'video/mp4',
   }
   res.writeHead(200, head);
   */
    next()
}

exports.HashPassword = function (password) {
    const salt = crypto.randomBytes(16).toString('hex');
    const hash = crypto.pbkdf2Sync(password, salt, 2048, 32, 'sha512').toString('hex');
    return [salt, hash].join('$');
}

exports.VerifyHash = function (password, original) {
    const originalHash = original.split('$')[1];
    const salt = original.split('$')[0];
    const hash = crypto.pbkdf2Sync(password, salt, 2048, 32, 'sha512').toString('hex');
    return hash === originalHash
}

function PersonellereSadelestirilmisAdSoyadEkle(req, res, next) {
    personelService.getAll()
        .then(data => {
            if (data.length) {
                for (let index = 0; index < data.length; index++) {
                    const Personel = data[index];
                    personelService.update(Personel.id, Personel)
                        .then(data => {
                            console.log(Personel.id, data)
                        })
                }
            } else {
                console.log("sadeleştirilmiş ad soyad eklenememiştir")
                next()
            }
        })
        .catch(err => next(err));
}



module.exports = {
    AlanKontrol,
    TokenKontrol,
    IzinKontrol,
    databaseConnection,
    pathToRegexCompare,
    pathToRegexConverter,
    ResHeader,
    encode_base64,
    decode_base64,
    get_From_base64_To_xmlsData,
    gunSaatCıkart,
    gunSaatTopla,
    gunSaatKarsılastır,
    haftalikKontrolSaati,
    haftalikKontrolSaatininYarisi,
    gunSaatcevirDakika,
    PersonellereSadelestirilmisAdSoyadEkle,
    Pdks_sadelestir



}



//kullanımı
//encode_base64('ddr.jpg');
//decode_base64('herhangi_base64_stringi','rane.jpg');