const sql = require('sql-template-strings');
const db = require('./db');
const projeService = require('./proje.service');
const personelService = require('./personel.service');
const isPaketleriService = require('./isPaketleri.service');
const musteriService = require('./musteri.service');
var _ = require('lodash');

//--------------------------------------------------------------------------------------------
function BuAydaIsPaketiVarMi(ip, yil, ay) {
    if (ip === undefined) return false;

    var bas = new Date(ip.BaslangicTarihi)
    var bit = new Date(ip.BitisTarihi)

    basYil = bas.getFullYear()
    bitYil = bit.getFullYear()
    basAy = bas.getMonth() + 1
    bitAy = bit.getMonth() + 1
    
    f = (basYil * 100) + basAy
    l = (bitYil * 100) + bitAy
    v = (yil * 100) + ay

    if (v>=f && v<l) {
        //console.log(`"${ay}/${yil}" ${ip.id} [${ip.BaslangicTarihi}:${ip.BitisTarihi}]`)
        return true
    } 
    return false
}

function buAydakiIsPaketleri(isPaketleri, yil, ay) {
    var secilenIspaketleri = isPaketleri.filter(ip => {return BuAydaIsPaketiVarMi(ip, yil, ay)})
    return secilenIspaketleri;
}

//--------------------------------------------------------------------------------------------

function buIsPaketlerindeCalisabilecekler(isPaketleri, personeller) {
    var isPaketlerindekiProfiller = [...new Set(isPaketleri.map(ip=>ip.ProfilId))]
    var secilenPersoneller = personeller.filter(per => {
        for(var profilKey in isPaketlerindekiProfiller) {
            if (per.ProfilId === isPaketlerindekiProfiller[profilKey]) {
                return true;
            }
        }
        return false;
    })
    return secilenPersoneller;
}

//--------------------------------------------------------------------------------------------

function calisanlariIsPaketlerineDagit(calisabilecekler, isPaketleri, yil, ay) {
    var result = {
        ay: ay,
        yil: yil,
        isPaketleri: []
    }

    for(var key in calisabilecekler) {
        var personel = calisabilecekler[key]
        var buPersoneleUygunIsPaketleri = isPaketleri.filter(ip => {return ip.ProfilId === personel.ProfilId})
        if (buPersoneleUygunIsPaketleri) {
            var aylikOran = 1 / buPersoneleUygunIsPaketleri.length
            aylikOran = aylikOran.toFixed(2)
            buPersoneleUygunIsPaketleri.map(p => {
                var found = false
                result.isPaketleri.forEach(ip => {
                    if (p.id === ip.id) {
                        var x = {}
                        x.isPaketiId = p.id
                        x.oran = aylikOran
                        x.personelId = personel.id
                        x.personelAdi = personel.Adi
                        x.musteriId = personel.MusteriId
                        x.projeId = ip.PorjeId
                        ip.push(x)
                    }
                })
                if (found === false) {
                    result.isPaketleri.push({isPaketiId: p.id, 
                                            oran:aylikOran, 
                                            personelId: personel.id, 
                                            personelAdi: personel.Adi,
                                            musteriId: personel.MusteriId, 
                                            projeId:p.ProjeId})
                }
            })
        }
    }
    return result;
}


  
async function Personel_Proje_AdamAy_create(record) 
{
    var q = sql`SELECT * FROM "Personel_Proje_AdamAy" WHERE 
    "Yil" = ${record.Yil} AND 
    "Ay" = ${record.Ay} AND
    "MusteriId" = ${record.MusteriId} AND
    "PersonelId" = ${record.PersonelId} AND 
    "ProjeId" = ${record.ProjeId} AND
    "IsPaketiId" = ${record.IsPaketiId};`
    const {rows} = await db.query(q);

     if (rows.length>0) {

        var q = sql`UPDATE "Personel_Proje_AdamAy" SET 
        "Yil" = ${record.Yil},
        "Ay" = ${record.Ay},
        "Oran" = ${record.Oran},
        "MusteriId" = ${record.MusteriId},
        "PersonelId" = ${record.PersonelId},
        "ProjeId" = ${record.ProjeId},
        "IsPaketiId" = ${record.IsPaketiId}, 
        "manualUpdateDate" = ${record.manualUpdateDate},
        "readOnly" = ${record.readOnly},
        "manualUpdate" = ${record.manualUpdate} 
        WHERE "id" = ${rows[0].id};`
        console.log(q)
        const {rowsUpdata} = await db.query(q);
    }else{
          var q = sql`
          INSERT INTO "Personel_Proje_AdamAy" ("Yil", "Ay", "Oran", "MusteriId", "PersonelId", "ProjeId", "IsPaketiId","manualUpdateDate", "createDate", "readOnly", "manualUpdate")
            VALUES (${record.Yil}, ${record.Ay}, ${record.Oran}, ${record.MusteriId}, ${record.PersonelId}, ${record.ProjeId}, ${record.IsPaketiId}, ${record.manualUpdateDate}, ${record.createDate}, ${record.readOnly},  ${record.manualUpdate}); `
            console.log(q)
        const result= await db.query(q);

    }
    
}

 


 function kaydet(aylikSonuclar) {
   for (let index = 0; index < aylikSonuclar.length; index++) {
       const aylikSonuc = aylikSonuclar[index];
       
       for (let j = 0; j < aylikSonuc.isPaketleri.length; j++) {
           const isPaketi = aylikSonuc.isPaketleri[j];
            var record = {}
            record.yil = aylikSonuc.yil
            record.ay = aylikSonuc.ay
            record.isPaketiCode = isPaketi.isPaketiId
            record.personelCode = isPaketi.personelId
            record.oran = isPaketi.oran * 100
            record.musteriCode = isPaketi.musteriId
            record.projeCode = isPaketi.projeId
           
            Personel_Proje_AdamAy_create(record);; // Syntax error
       }
   }
}

async function hesapla(basYil, basAy, kacAylik, musteriCode) {
    var i = 0
    var yil = basYil
    var ay = basAy
    var takvim = []
    while(i < kacAylik) {
        ay++;
        if (ay > 12) {
            yil++;
            ay = 1;
        }
        takvim.push({yil:yil, ay:ay})
        i++;
    }

    var aylikSonuclar = []
    var Personeller = []
    var Projeler = []
    var IsPaketleri = [] 
    
    // musterinin projeleri alinir
    Projeler = await projeService.getAllByMusteriId(musteriCode);

    // projelerin is paketleri alinir
    for(var pKey in Projeler) {
        var ip = await isPaketleriService.getByProjeId(Projeler[pKey].id)
        IsPaketleri = IsPaketleri.concat(ip)
    }

    // musterinin personelleri alinir
    Personeller = await personelService.getAllByMusteriId(musteriCode)


    for(var key in takvim) {
        var ay = takvim[key]
        var isPaketleri = buAydakiIsPaketleri(IsPaketleri, ay.yil, ay.ay)
        var calisabilecekler = buIsPaketlerindeCalisabilecekler(isPaketleri, Personeller)
        var aylikSonuc = calisanlariIsPaketlerineDagit(calisabilecekler, isPaketleri, ay.yil, ay.ay)
        aylikSonuclar.push(aylikSonuc)
       
    }

    kaydet(aylikSonuclar);
/*
    var byPersonel = aylikSonuclar.reduce((a, b) => {
        b.isPaketleri.forEach(ip => {
            var x = a[ip.personelId] || (a[ip.personelId] = {})
            x.personelId = ip.personelId
            x.personelAdi = ip.personelAdi
            x.oranlar = x.oranlar || []
            var y = x.oranlar.filter(f => (f.ay === b.ay && f.yil === b.yil) )
            if (y.length > 0) {
                var totalVal = parseFloat(y[0].oran)
                totalVal += parseFloat(ip.oran)
                y[0].oran = totalVal.toFixed(2)
            }
            else {
                x.oranlar.push({ay: b.ay, yil: b.yil, oran: ip.oran})
            }
    
            x.projeler = x.projeler || []
            var projePart = x.projeler.filter(p => (p.id === ip.projeId))
            if (projePart.length > 0) {
                projePart[0].oranlar.push({ay:b.ay, yil:b.yil, oran:ip.oran})
            }
            else {
                x.projeler.push({id: ip.projeId, projeAdi: 'Proje-'+ip.projeId, oranlar: [{ay:b.ay, yil:b.yil, oran:ip.oran}]})
            }
    
        });
        return a
    }, {})
*/
    return {aylikSonuclar: aylikSonuclar}
}







async function getByMusteriCode(musteriCode) 
{
    const {rows} = await db.query(sql`
        SELECT * FROM "Personel_Proje_AdamAy" WHERE "MusteriCode" = ${musteriCode} ORDER BY id;`);

    if (rows) {
        var proje   = await projeService.getByCode(rows[0].ProjeCode)

        var result = [{
            personeller:[]
        }]
    }

        if (rows) {
        }
        else {
            return rows;
        }
        return result;
}

async function getByPersonelCode(personelCode) 
{
    const {rows} = await db.query(sql`
        SELECT * FROM "Personel_Proje_AdamAy" WHERE "PersonelCode" = ${personelCode} ORDER BY id;`);
        return rows;
}

async function getByProjeCode(projeCode) 
{
    const {rows} = await db.query(sql`
        SELECT * FROM "Personel_Proje_AdamAy" WHERE "ProjeCode" = ${projeCode} ORDER BY id;`);
    return rows;
} 

async function adamAyHesapla(musteriId,startDate,endDate )
{
     startDate=new Date(startDate);
     endDate=new Date(endDate);
 
     var q=sql`
    SELECT * FROM "Personel_Proje_AdamAy" WHERE  
    "MusteriId" =${musteriId} AND 
    (  
        ("Yil"=${startDate.getFullYear()}  AND "Ay">=${startDate.getMonth()+1})
        OR
         ("Yil">${startDate.getFullYear()} AND "Yil"<${endDate.getFullYear()})
        OR
        ("Yil"=${endDate.getFullYear()}  AND "Ay"<=${endDate.getMonth()+1})
    )
    ORDER BY id;`
    const {rows} = await db.query(q);

    return rows; 
}
 
function personeleGoreToplamAdamAylar(rows) {
    return _.chain(rows)
    .groupBy("PersonelCode")
    .map((value, key) => ({ PersonelCode: key, Val: value }))
    .value()
}

async function foo(data) {
    var result = {}
    result.personeller = []

    var dataGroupByPersonel = _.chain(data)
                                .groupBy("personelId")
                                //.orderBy(["Yil", "Ay"])
                                .map((value, key) => ({ PersonelCode: key, Val: value }))
                                .value()
    console.log(JSON.stringify(data))
    //dataGroupByPersonel.forEach((d) => {
    for(var dKey in dataGroupByPersonel) {
        var d = dataGroupByPersonel[dKey]
        var personelRecord = {}
        personelRecord.personelCode = d.PersonelCode
        var _personel = await personelService.getById(d.PersonelCode)
        personelRecord.personelAdi = "ADI SOYADI"
        personelRecord.id = d.PersonelCode
        if (_personel.length > 0) {
            personelRecord.personelAdi = _personel[0].Adi + " " + _personel[0].Soyadi
        }
        personelRecord.oranlar = []
        personelRecord.projeler = []
        result.personeller.push(personelRecord)

        for(var rKey in d.Val) {
        //d.Val.forEach(r => {
            var r = d.Val[rKey]
            var yilAyDeger = personelRecord.oranlar.filter(p => {return (p.Yil === r.Yil && p.Ay === r.Ay)})
            if (yilAyDeger.length > 0) {
                yilAyDeger[0].Oran += r.Oran
            }
            else {
                var yilAyDeger = {}
                yilAyDeger.Yil = r.Yil
                yilAyDeger.Ay = r.Ay
                yilAyDeger.Oran = r.Oran
                personelRecord.oranlar.push(yilAyDeger)
            }

            // projeler
            var projeRecords = personelRecord.projeler.filter(p => {return (p.projeCode === r.ProjeCode)})
            if (projeRecords.length > 0) {
                var found = false
                projeRecords[0].oranlar.forEach(oran => {
                    if(oran.Yil === r.Yil && oran.Ay === r.Ay) {
                        oran.Oran += r.Oran
                        found = true
                    }
                })
                if (found === false) {
                    projeRecords[0].oranlar.push({Yil:r.Yil, Ay:r.Ay, Oran:r.Oran})
                }
            }
            else {
                var newProjeRecord = {}
                newProjeRecord.projeCode = r.ProjeCode
                var _proje = await projeService.getById(r.ProjeCode)
                newProjeRecord.projeAdi = "PROJE ADI"
                if (_proje.length > 0) {
                    newProjeRecord.projeAdi = _proje.ProjeAdi
                }
                newProjeRecord.oranlar = [{Yil:r.Yil, Ay:r.Ay, Oran:r.Oran}]
                personelRecord.projeler.push(newProjeRecord)
            }

        }
    }
    return result;
}



async function getByHesaplamaId(id) {
    const {rows} = await db.query(sql`
        SELECT * FROM "Personel_Proje_AdamAy" WHERE "HesaplamaId" = ${id} ORDER BY id;`);
    
    if (rows) {
        var result = await foo(rows)
        return [result];
    }
}


///////////////////////////////////////iç/////////////////////////////////////
async function getByMonthAdamAyAllPersonel(MusteriId,ay,yil) { 
    try { 
       
        const {rows} = await db.query(sql`
        SELECT * FROM "Personel_Proje_AdamAy" WHERE "MusteriId" = ${MusteriId}  AND "Ay"= ${ay}   AND "Yil"= ${yil}    ORDER BY id;`);
   
        return rows;
    }
    catch (error) {
        throw error;
    }
}

///////////////////////////////////////iç/////////////////////////////////////
async function getIspaketininHepsi(adamAy) { 
    try { 
        const {rows} = await db.query(sql`
        SELECT * FROM "Personel_Proje_AdamAy" WHERE
         "MusteriId" = ${adamAy.MusteriId}  AND
         "PersonelId"= ${adamAy.PersonelId}   AND
         "IsPaketiId"= ${adamAy.IsPaketiId}   AND
          "ProjeId"= ${adamAy.ProjeId}   ORDER BY id;`); 
        return rows;
    }
    catch (error) {
        throw error;
    }
}

 

async function adamAyUpdate(record) 
{
    var q = sql`UPDATE "Personel_Proje_AdamAy" SET 
    "Yil" = ${record.Yil},
    "Ay" = ${record.Ay},
    "Oran" = ${record.Oran},
    "MusteriId" = ${record.MusteriId},
    "PersonelId" = ${record.PersonelId},
    "ProjeId" = ${record.ProjeId},
    "IsPaketiId" = ${record.IsPaketiId}, 
    "manualUpdateDate" = ${record.manualUpdateDate},
    "readOnly" = ${record.readOnly},
    "manualUpdate" = ${record.manualUpdate} 
    WHERE "id" = ${record.id};`
    console.log(q)
    const {rowsUpdata} = await db.query(q); 
        return []; 
}

async function removeByPersonel(id) {
    try {  
        var q = sql`Delete FROM "Personel_Proje_AdamAy" Where  "PersonelId"=${id} `
        const resultDeletePersonel_Proje_AdamAy = await db.query(q); 
        return true;
    }
    catch (error) {
        console.log(error)
        throw error;
    }
}
///////////////////////////////////////iç/////////////////////////////////////
module.exports ={
    getByHesaplamaId,
    getByMusteriCode,
    getByPersonelCode,
    getByProjeCode,
    adamAyHesapla,
    hesapla,
    getByMonthAdamAyAllPersonel,
    getIspaketininHepsi,
    Personel_Proje_AdamAy_create,
    adamAyUpdate,
    removeByPersonel
}