const sql = require('sql-template-strings');
const db = require('./db');


//-------------------------------------------------------------------------------------------------------

async function getAll() {
    const {rows} = await db.query(sql`
        SELECT * FROM "EgitimDurumu" ORDER BY id;`);
    return rows;
}


//-------------------------------------------------------------------------------------------------------

async function getById(id) {
    const result = await db.query(sql`
        SELECT * FROM "EgitimDurumu" WHERE id=${id} LIMIT 1;
    `);
    return result.rows
}

//-------------------------------------------------------------------------------------------------------

async function create(EgitimDurumu) 
{
    try {
        var q = sql`INSERT INTO "EgitimDurumu" ("EgitimDurumAdi") VALUES (${EgitimDurumu.EgitimDurumAdi})`
        console.log(q)
        const result= await db.query(q);
        //console.log("deneme "+JSON.stringify( deneme))
        return true;
    }
    catch(error) {
        throw error;
    }
}

//-------------------------------------------------------------------------------------------------------
// https://stackoverflow.com/questions/42876071/how-to-save-js-date-now-in-postgresql

async function update(id, EgitimDurumu) {
    try {
        var now = new Date().getTime().toString();
        var q = sql`Update "EgitimDurumu" Set 
                        "EgitimDurumAdi" = ${EgitimDurumu.EgitimDurumAdi}
                     Where id = ${id}`
        const {rows} = await db.query(q);
    }
    catch(error) {
        throw error;
    }
    return EgitimDurumu
}

//-------------------------------------------------------------------------------------------------------

async function remove(id) {
    try {
        var q = sql`Delete FROM "EgitimDurumu" Where id = ${id}`
        const result = await db.query(q);
        //console.log("deneme "+JSON.stringify( result))
        return true;
    }
    catch(error) {
        throw error;
    }
}

module.exports ={
    getAll,
    getById,
    create,
    update,
    remove
}