const sql = require('sql-template-strings');
const db = require('./db');

//-------------------------------------------------------------------------------------------------------

async function getAll() {
    //SELECT * FROM "Musteri" WHERE "Status" = 1 ORDER BY id;
    
    const {rows} = await db.query(sql`
        select M.id, M."FirmaAdi", S."SektorAdi", B."BolgeAdi" from "Musteri" as M
            left join "Sektor" as S
            on M."SektorId" = S.id
            left join "Bolge" as B
            on M."BolgeId" = B.id
            WHERE M."Status" = 1
            ORDER BY M.id`);
    return rows;
}


//-------------------------------------------------------------------------------------------------------

async function getById(id) {
    const {rows} = await db.query(sql`
        SELECT * FROM "Musteri" WHERE "id"=${id} LIMIT 1;
    `);
    return rows;
}

//-------------------------------------------------------------------------------------------------------

async function create(musteri) 
{
    try {
        var now = Date.now()
        var q = sql`INSERT INTO "Musteri" (
                        "FirmaAdi" , 
                        "TicariUnvan",
                        "FirmaAdresi",
                        "FirmaTuru",
                        "SektorId",
                        "BolgeId",
                        "FaturaAdresi",
                        "FabrikaAdresi",
                        "FirmaTelefonu",
                        "VergiDairesi",
                        "VergiNumarasi",
                        "AktifMi",
                        "WebSitesi",
                        "CreatedBy", 
                        "CreatedAt", 
                        "Status") 
                    VALUES ( 
                        ${musteri.FirmaAdi}, 
                        ${musteri.TicariUnvan},
                        ${musteri.FirmaAdresi},
                        ${musteri.FirmaTuru},
                        ${musteri.SektorId},
                        ${musteri.BolgeId},
                        ${musteri.FaturaAdresi},
                        ${musteri.FabrikaAdresi},
                        ${musteri.FirmaTelefonu},
                        ${musteri.VergiDairesi},
                        ${musteri.VergiNumarasi},
                        ${musteri.AktifMi},
                        ${musteri.WebSitesi},
                        1, 
                        ${now}, 
                        1)`
        const {rows} = await db.query(q);
        return true
    }
    catch(error) {
        console.log(error)
        throw error;
    }
}

//-------------------------------------------------------------------------------------------------------

async function update(id, musteri) {
    try {
        var now = Date.now()
        // check id varmi
        var q = sql`Update "Musteri" Set 
                        "FirmaAdi" = ${musteri.FirmaAdi}, 
                        "TicariUnvan" = ${musteri.TicariUnvan},
                        "FirmaAdresi" = ${musteri.FirmaAdresi},
                        "FirmaTuru" = ${musteri.FirmaTuru},
                        "SektorId" = ${musteri.SektorId},
                        "BolgeId" = ${musteri.BolgeId},
                        "FaturaAdresi" = ${musteri.FaturaAdresi},
                        "FabrikaAdresi" = ${musteri.FabrikaAdresi},
                        "FirmaTelefonu" = ${musteri.FirmaTelefonu},
                        "VergiDairesi" = ${musteri.VergiDairesi},
                        "VergiNumarasi" = ${musteri.VergiNumarasi},
                        "AktifMi" = ${musteri.AktifMi},
                        "WebSitesi" = ${musteri.WebSitesi},
                        "UpdatedAt" = ${now}, 
                        "UpdatedBy" = 1
                        Where id = ${id}`
        const {rows} = await db.query(q);
    }
    catch(error) {
        throw error;
    }
    return musteri
}

//-------------------------------------------------------------------------------------------------------

async function remove(id) {
    try {
        var q = sql`Update "Musteri" Set "Status" = 0 Where id = ${id}`
        const {rows} = await db.query(q);
        return true;
    }
    catch(error) {
        throw error;
    }
}

//-------------------------------------------------------------------------------------------------------

module.exports ={
    getAll,
    getById,
    create,
    update,
    remove
}