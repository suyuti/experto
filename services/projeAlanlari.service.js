const sql = require('sql-template-strings');
const db = require('./db');


//-------------------------------------------------------------------------------------------------------

async function getAll() {
    const { rows } = await db.query(sql`
        SELECT * FROM "ProjeAlanlari" ORDER BY id;`);
    return rows;
}


//-------------------------------------------------------------------------------------------------------

async function getById(id) {
    const result = await db.query(sql`
        SELECT * FROM "ProjeAlanlari" WHERE id=${id} LIMIT 1;
    `);
    return result.rows
}

//-------------------------------------------------------------------------------------------------------

async function create(ProjeAlanlari) {
    try {
        var q = sql`INSERT INTO "ProjeAlanlari" ("ProjeAlanAdi") VALUES (${ProjeAlanlari.ProjeAlanAdi})`
        console.log(q)
        const result = await db.query(q);
        //console.log("deneme "+JSON.stringify( deneme))
        return true;
    }
    catch (error) {
        throw error;
    }
}

//-------------------------------------------------------------------------------------------------------
// https://stackoverflow.com/questions/42876071/how-to-save-js-date-now-in-postgresql

async function update(id, ProjeAlanlari) {
    try {
        var now = new Date().getTime().toString();
        var q = sql`Update "ProjeAlanlari" Set 
                        "ProjeAlanAdi" = ${ProjeAlanlari.ProjeAlanAdi}
                     Where id = ${id}`
        const { rows } = await db.query(q);
    }
    catch (error) {
        throw error;
    }
    return ProjeAlanlari
}

//-------------------------------------------------------------------------------------------------------

async function remove(id) {
    try {
        var q = sql`Delete FROM "ProjeAlanlari" Where id = ${id}`
        const result = await db.query(q);
        //console.log("deneme "+JSON.stringify( result))
        return true;
    }
    catch (error) {
        throw error;
    }
}

module.exports = {
    getAll,
    getById,
    create,
    update,
    remove
}