const sql = require('sql-template-strings');
const db = require('./db');

//-------------------------------------------------------------------------------------------------------

async function getAll() {
    //const {rows} = await db.query(sql`
    //    SELECT * FROM "Proje" WHERE "Status" = 1 ORDER BY "id"`);

    const {rows} = await db.query(sql`select P.id, M."FirmaAdi", P."ProjeKodu", P."ProjeAdi", PT."ProjeTurAdi", PA."ProjeAlanAdi" from "Proje" as P
    left join "Musteri" as M
    on M.id = P."MusteriId"
    left join "ProjeTurleri" as PT
    on PT.id = P."ProjeTuru"
    left join "ProjeAlanlari" as PA
    on PA.id = P."ProjeAlani"
    WHERE P."Status" = 1
    ORDER BY P.id;`)

    return rows;
}


async function getAllByMusteriId(id) {
    const {rows} = await db.query(sql`
        SELECT P.id, M."FirmaAdi", P."ProjeKodu", P."ProjeAdi", PT."ProjeTurAdi", PA."ProjeAlanAdi",P."EkipId"  
        FROM "Proje" as P
        left join "Musteri" as M
        on M.id = P."MusteriId"
        left join "ProjeTurleri" as PT
        on PT.id = P."ProjeTuru"
        left join "ProjeAlanlari" as PA
        on PA.id = P."ProjeAlani" 
        WHERE "MusteriId" =${id} ORDER BY P.id;`); 
       
    return rows;
}

//-------------------------------------------------------------------------------------------------------

async function getById(id) {
    const result = await db.query(sql`
        SELECT * FROM "Proje" WHERE "id"=${id} LIMIT 1;
    `);
    return result.rows
}

//-------------------------------------------------------------------------------------------------------


Date.prototype.formatMMDDYYYY = function(){
    return (this.getMonth() + 1) + 
    "/" +  this.getDate() +
    "/" +  this.getFullYear();
}


async function create(proje) 
{  
    try {
        var q =sql`INSERT INTO "Proje" ( 
            "ProjeAdi", 
            "ProjeKodu", 
            "MusteriId", 
            "ProjeTuru", 
            "ProjeBaslangicTarihi", 
            "ProjeBitisTarihi", 
            "ProjeYazimBaslangicTarihi", 
            "ProjeAlani", 
            "SunulanProjeButcesi", 
            "OnaylananProjeButcesi", 
            "CreatedAt", 
            "CreatedBy", 
            "UpdatedAt",  
            "UpdatedBy",  
            "Status", 
            "SorumluDepartman",
            "EkipId")
        VALUES ( 
            ${proje.ProjeAdi}, 
            ${proje.ProjeKodu}, 
            ${proje.MusteriId},  
            ${proje.ProjeTuru ? proje.ProjeTuru:null}, 
            ${proje.ProjeBaslangicTarihi?  proje.ProjeBaslangicTarihi  :null }, 
            ${proje.ProjeBitisTarihi ? proje.ProjeBitisTarihi  :null }, 
            ${proje.ProjeYazimBaslangicTarihi ?  proje.ProjeYazimBaslangicTarihi   :null}, 
            ${proje.ProjeAlani ? proje.ProjeAlani:null},  
            ${proje.SunulanProjeButcesi ? proje.SunulanProjeButcesi:null},  
            ${proje.OnaylananProjeButcesi ? proje.OnaylananProjeButcesi:null}, 
            ${proje.CreatedAt ? proje.CreatedAt:null},
            ${proje.CreatedBy ? proje.CreatedBy:null},
            ${proje.UpdatedAt ? proje.UpdatedAt:null},  
            ${proje.UpdatedBy ? proje.UpdatedBy:null},
            ${proje.Status ? proje.Status:1}, 
            ${proje.SorumluDepartman ? proje.SorumluDepartman:null},
            ${proje.EkipId ? proje.EkipId:null})
            ` 
             
      //  var q = sql`INSERT INTO "Proje" ("ProjeAdi", "ProjeKodu", "MusteriId") VALUES (${proje.ProjeAdi}, ${proje.ProjeKodu}, ${proje.MusteriId} )`
        console.log(q)
        const {rows} = await db.query(q);
        return true
    }
    catch(error) {
        throw error;
    }
}

//-------------------------------------------------------------------------------------------------------
// https://stackoverflow.com/questions/42876071/how-to-save-js-date-now-in-postgresql

async function update(id, proje) {
    try {
        var QueryString = 'UPDATE "Proje" SET ';
        if (proje.ProjeAdi)                     { QueryString += '"ProjeAdi" = \'' + proje.ProjeAdi + '\','; }
        if (proje.ProjeKodu)                    { QueryString += '"ProjeKodu" = \'' + proje.ProjeKodu + '\','; }
        if (proje.MusteriId)                    { QueryString += '"MusteriId" = \'' + proje.MusteriId + '\','; }
        if (proje.ProjeTuru)                    { QueryString += '"ProjeTuru" = \'' + proje.ProjeTuru + '\','; }
        if (proje.ProjeAlani)                   { QueryString += '"ProjeAlani" = \'' + proje.ProjeAlani + '\','; }
        if (proje.SunulanProjeButcesi)          { QueryString += '"SunulanProjeButcesi" = \'' + proje.SunulanProjeButcesi + '\','; }
        if (proje.OnaylananProjeButcesi)        { QueryString += '"OnaylananProjeButcesi" = \'' + proje.OnaylananProjeButcesi + '\','; }
        if (proje.Status)                       { QueryString += '"Status" = \'' + proje.Status + '\','; }
        if (proje.SorumluDepartman)             { QueryString += '"SorumluDepartman" = \'' + proje.SorumluDepartman + '\','; }
        if (proje.ProjeBaslangicTarihi)         { QueryString += '"ProjeBaslangicTarihi" = \'' + proje.ProjeBaslangicTarihi + '\','; }
        if (proje.ProjeBitisTarihi)             { QueryString += '"ProjeBitisTarihi" = \'' + proje.ProjeBitisTarihi + '\','; }
        if (proje.ProjeYazimBaslangicTarihi)    { QueryString += '"ProjeYazimBaslangicTarihi" = \'' + proje.ProjeYazimBaslangicTarihi + '\','; }
        if (proje.EkipId)    { QueryString += '"EkipId" = \'' + proje.EkipId + '\','; }
        //if (proje.CreatedAt)                    { QueryString += '"" = \'' + proje.CreatedAt + '\','; }
        //if (proje.CreatedBy)                    { QueryString += '"" = \'' + proje.CreatedBy + '\','; }
        //if (proje.UpdatedAt)                    { QueryString += '"UpdatedAt" = \'' + proje.UpdatedAt + '\','; }
        if (proje.UpdatedBy)                    { QueryString += '"UpdatedBy" = \'' + proje.UpdatedBy + '\', '; }
        QueryString += ' "UpdatedAt" = \'' + Date.now() + '\' WHERE  "id" = ' + id;

        const {rows} = await db.query(QueryString);
    }
    catch(error) {
        console.log(error)
        throw error;
    }
    return proje
}

//-------------------------------------------------------------------------------------------------------

async function remove(id) {
    try {
        var q = sql`DELETE FROM "Proje" WHERE id = ${id};`
        await db.query(q);
        q = sql`DELETE FROM "Personel_Proje_AdamAy" WHERE "ProjeId"= ${id};`
        await db.query(q);
        return true
    }
    catch(error) {
        throw error;
    }
}

async function getIsPaketleri(projeId) {
    return ["a"+projeId, "b"+projeId, "c"+projeId]
}

async function GetProjeFromtByProjeId(projeId) {
    const {rows} = await db.query(sql`
        SELECT * FROM "Proje" WHERE id =${projeId}`); 
    return rows[0];
}


//-------------------------------------------------------------------------------------------------------

async function getByProjeKodu(ProjeKodu) {
    const result = await db.query(sql`
        SELECT * FROM "Proje" WHERE "ProjeKodu"=${ProjeKodu} LIMIT 1;
    `);
    return result.rows
}

module.exports ={
    getAll,
    getById,
    create,
    update,
    remove,
    getAllByMusteriId,
    getIsPaketleri,
    GetProjeFromtByProjeId,
    getByProjeKodu
}