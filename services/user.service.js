const config = require('../config.json');
const jwt = require('jsonwebtoken');
const Role = require('../_helpers/role');
const sql = require('sql-template-strings');
const db = require('./db');
const command = require('./command')

async function authenticate({ username, password }) {
    
    const user = await getBy_Username(username, password);
    
    let veryfy=command.VerifyHash(password,user.PasswordHash);
    console.log(veryfy);
    if (veryfy) {
        //const roles = await getUserRoles(user.id)
        const permissions = await getUserPermissions(user.id)
        //user['Roles'] = roles.map(r => r.RoleName)
        user['Permissions'] = [...new Set(permissions.map(p =>p ))];
        const token = jwt.sign({ userId: user.id, role: user.PasswordHash }, config.secret);
        const { PasswordHash, PaswordSalt, ...userValues } = user
        return { ...userValues, token }
    }
}

async function getAll() {
    const { rows } = await db.query(sql`
        SELECT * FROM "Users" WHERE "Status" = 1 ORDER BY id;`);
    return rows;
}

async function getById(id) {
    const { rows } = await db.query(sql`
        SELECT * FROM "Users" WHERE "id" =${id} ORDER BY id;`);
        delete rows[0].PasswordHash;
        delete rows[0].PaswordSalt
    return rows;
}

async function getBy_Username(username) {
    const { rows } = await db.query(sql`
        SELECT * FROM "Users" WHERE "UserName" =${username}  ORDER BY id;`); // TODO hash
    return rows[0]; // TODO
}

async function getUserRoles(userId) {
  /* const { rows } = await db.query(sql`
        select R."RoleName" from "Users" as U
        inner join "UserRoles" as UR
        on U.id = UR."UserId"
        inner join "Roles" as R
        on R.id = UR."RoleId"
        where U.id = ${userId};`);*/

        const { rows } = await db.query(sql`
        select  * from "Roles" as R 
        inner join (select UR."RoleId" from "UserRoles" as UR where UR."UserId"= ${userId}) as P
        on R.id = P."RoleId";`);



    return rows;
}

async function getUserPermissions(userId) {
    /*const { rows } = await db.query(sql`select "PermissionKey" from "UserRoles" as UR
    inner join "Permissions" as P on P."RoleId" = UR."RoleId"
    where UR."UserId" = ${userId}`)*/

    const { rows } = await db.query(sql`
    select  * from "RolesPermissions" as RP
    inner join "Permissions" as P 
    on RP."PermissionId" = P."id" 
    where RP."RoleId" IN (select UR."RoleId" from "UserRoles" as UR where UR."UserId"= ${userId});`);

    return rows;
}

async function update(id,user) { 

    var q =  `Update "Users" Set   
    "UserName"= '${user.UserName }',  
    "FirstName"= '${user.FirstName }',
    "LastName"= '${user.LastName }',
    "EMail"= '${user.EMail }',
    "DisplayName"= '${user.DisplayName }',
    "Dashboard"= '${user.Dashboard }'  
    Where id = ${id};`
    console.log(q) 
    const result= await db.query( q );
    return true; 
    //return "Not implemented yet";
}
async function create(user) {
    //user.PasswordHash
    var now=new Date();
    var q = `INSERT INTO "Users"
     ("UserName",
     "FirstName",
     "LastName",
     "EMail",
     "DisplayName",
     "Dashboard",
     "PasswordHash",
     "CreatedDate",
     "CreatedBy",
     "Status",
     "PaswordSalt") 
    VALUES 
    ('${user.UserName ? user.UserName:""}',
    '${user.FirstName?user.FirstName  :""}',
    '${user.LastName?user.LastName  :""}',
    '${user.EMail?user.EMail  :""}',
    '${user.DisplayName?user.DisplayName  :""}',
    '${user.Dashboard?user.Dashboard  :""}',
    '${command.HashPassword(user.PasswordHash)}',
     now(),
     ${user.CreatedBy ?user.CreatedBy:"" },
     1,
     'on' )`;

    console.log(q)
    await db.query( q ); 

    q =`SELECT * FROM "Users" WHERE 
    "UserName" ='${user.UserName ? user.UserName :""}' and
    "FirstName" ='${user.FirstName ? user.FirstName :""}' and
    "LastName" ='${user.LastName ? user.LastName :""}' and
    "EMail" ='${user.EMail ? user.EMail :""}' and
    "DisplayName" ='${user.DisplayName ? user.DisplayName :""}' and
    "Dashboard" ='${user.Dashboard ? user.Dashboard :""}' `
  
    console.log(q)
    const result= await db.query( q );

    return result.rows;  
}

async function remove(id) {
    try {
        var q =  `Update "Users" Set   
        "Status"= 0  
        Where id = ${id};`
        console.log(q)
        const result= await db.query( q );
        return true;
    }
    catch(error) {
        throw error;
    }
     
}

async function setUserRole(UserId,roles) {

        try {
            var q=  `DELETE FROM "UserRoles" WHERE "UserId"=${UserId};`
            q+=`INSERT INTO "UserRoles" ("UserId","RoleId") 
            VALUES`;
            for (let index = 0; index < roles.length; index++) {
                const element = roles[index];
                q += `(${UserId},${element.id}),`
            }

            var temp = q.split("");
            temp[temp.length-1] = ";";
            q= temp.join("");

            console.log(q) 
            const result= await db.query(q);
            return true;
        }
        catch(error) {
            throw error;
        } 
}
async function getUserRoles(UserId) {
    const { rows } = await db.query(sql`
        SELECT * FROM "Roles" WHERE "Status" = 1 ORDER BY id;`);
    return rows;
}

async function getUserRoles(User_id) {
 
    const { rows } = await db.query(sql`
    SELECT * FROM "Roles" WHERE "id" IN ( SELECT "RoleId" FROM "UserRoles" WHERE "UserId"=${User_id} ORDER BY id);
        
        `); 
    return rows;
}
  

module.exports = {
    authenticate,
    getAll,
    getById,
    getUserPermissions,
    update,
    create,
    remove,
    setUserRole,
    getUserRoles
};
