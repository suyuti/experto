const sql = require('sql-template-strings');
const db = require('./db');


//-------------------------------------------------------------------------------------------------------

async function getAll() {
    // const {rows} = await db.query(sql`
    //     SELECT * FROM "Pdks" ORDER BY id;`);
    // return rows;
    return ""
}


//-------------------------------------------------------------------------------------------------------


async function getById(id) {
    // const result = await db.query(sql`
    //     SELECT * FROM "Pdks" WHERE id=${id} LIMIT 1;
    // `);
    // return result.rows
    
    return ""
}



//-------------------------------------------------------------------------------------------------------

async function create(Pdks) 
{
    // try {
    //     var q = sql`INSERT INTO "Pdks" ("PdksAdi","MusteriId") 
    //     VALUES (${Pdks.PdksAdi},${Pdks.MusteriId})`
    //     console.log(q)
    //     const result= await db.query(q);
    //     //console.log("deneme "+JSON.stringify( deneme))
    //     return true;
    // }
    // catch(error) {
    //     throw error;
    // }
    return ""
}

//-------------------------------------------------------------------------------------------------------
// https://stackoverflow.com/questions/42876071/how-to-save-js-date-now-in-postgresql

async function update(id, Pdks) {
    // try {
    //     var now = new Date().getTime().toString();
    //     var q = sql`Update "Pdks" Set 
    //                     "PdksAdi"       = ${Pdks.PdksAdi}
    //                     "MusteriId	"   = ${Pdks.MusteriId	}
    //                  Where id = ${id}`
    //     const {rows} = await db.query(q);
    // }
    // catch(error) {
    //     throw error;
    // }
    // return Pdks
    return ""
}

//-------------------------------------------------------------------------------------------------------

async function remove(id) {
    // try {
    //     var q = sql`Delete FROM "Pdks" Where id = ${id}`
    //     const result = await db.query(q);
    //     //console.log("deneme "+JSON.stringify( result))
    //     return true;
    // }
    // catch(error) {
    //     throw error;
    // }
    return ""
}



//-------------------------------------------------------------------------------------------------------

 function resmiTatiller() {
   
let AyaAitResmiTatil=[ 
    { id: 0,Tarih :"2019-01-01", Name:"Yılbaşı"  }, 
    { id: 1,Tarih :"2019-04-23", Name:"23 Nisan Ulusal Egemenlik ve Çocuk Bayramı"  }, 
    { id: 2,Tarih :"2019-05-01", Name:"1 Mayıs Emek ve Dayanışma Günü"  }, 
    { id: 3,Tarih :"2019-05-19", Name:"19 Mayıs Atatürk`ü Anma ve Gençlik ve Spor Bayramı"  }, 
    { id: 4,Tarih :"2019-06-03", Name:"Ramazan Bayramı"  }, 
    { id: 5,Tarih :"2019-06-04", Name:"Ramazan Bayramı"  }, 
    { id: 6,Tarih :"2019-06-05", Name:"Ramazan Bayramı"  }, 
    { id: 7,Tarih :"2019-06-06", Name:"Ramazan Bayramı"  },  
    { id: 8,Tarih :"2019-07-15", Name:"15 Temmuz Demokrasi Bayramı"  },  
    { id: 9,Tarih :"2019-08-10", Name:"Kurban Bayramı"  }, 
    { id: 10,Tarih :"2019-08-11", Name:"Kurban Bayramı"  }, 
    { id: 11,Tarih :"2019-08-12", Name:"Kurban Bayramı"  }, 
    { id: 12,Tarih :"2019-08-13", Name:"Kurban Bayramı"  }, 
    { id: 13,Tarih :"2019-08-14", Name:"Kurban Bayramı"  }, 
    { id: 14,Tarih :"2019-08-30", Name:"30 Ağustos Zafer Bayramı"  }, 
    { id: 15,Tarih :"2019-10-29", Name:"29 Ekim Cumhuriyet Bayramı"  }, 

]
    return AyaAitResmiTatil
}

 //-------------------------------------------------------------------------------------------------------


async function getShemaByMusteriId(id) {
   // return 1
    const result = await db.query(sql`
        SELECT * FROM "PdksMusteriShema" WHERE "MusteriId"=${id} LIMIT 1;
    `); 
    if(result.rows.length>0 ){
       return result.rows[0].PdksShemaId 
    }else{
        return ""
    }
    
     
}
 //-------------------------------------------------------------------------------------------------------


module.exports ={
    getAll,
    getById,
    create,
    update,
    remove, 
    resmiTatiller,
    getShemaByMusteriId
}