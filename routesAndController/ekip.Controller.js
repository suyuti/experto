var     express         = require('express');
var     router          = express.Router();
const httpResult = require('../config');
const returnTemplate    = require('../template/returnTemplate');
const   EkipService    = require('../services/ekip.service');



router.get      ('/',       getAll);    // Ekip listesi doner
router.post     ('/',       create);    // Ekip oluşturur
router.get      ('/:id',    getById);   // idsi verilen Ekip getirir
router.put      ('/:id',    update);    // Ekip gunceller
router.delete   ('/:id',    remove);    // Ekip siler (Status = 0)

//-------------------------------------------------------------------------------

function getAll(req, res, next) {
    console.log("getAll");
    EkipService.getAll()
        .then(data => {
            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Kayıt bulunamamıştır"))
            }
        })
        .catch(err => next(err));
}




//-------------------------------------------------------------------------------

function create(req, res, next) {
    console.log("create");
    EkipService.create(req.body)
        .then(status => {
            if (status) {
                res.json(returnTemplate(status, "Basarı ile eklenmiştir"))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function getById(req, res, next) {
    console.log("getById");
    const id = parseInt(req.params.id);

    EkipService.getById(id)
        .then(data => {

            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Bu id ye ait kayıt bulunamamıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function update(req, res, next) {
    console.log("update");
    const id = parseInt(req.params.id);

    EkipService.update(id, req.body)
        .then(data => {
            if (data) {
                res.json(returnTemplate(data, "Basarı ile güncellenmiştir"))
            } else {
                res.json(returnTemplate([], "güncelleme yapılamammıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function remove(req, res, next) {
    console.log("remove");
    const id = parseInt(req.params.id)

    EkipService.remove(id)
        .then(status => {
            if (status) {
                res.json(returnTemplate(status, "Basarı ile silinmiştir"))
            }
            else {
                res.json(returnTemplate([], "silme yapılamammıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------



module.exports = router;
