var express = require('express');
var router = express.Router();
const returnTemplate    = require('../template/returnTemplate');
const userService = require('../services/user.service');
const httpResult = require('../config');

 

router.post    ('/authenticate', authUser)      //user bilgilerini Kontrol eder
router.get    ('/',             getAll)         //user bilgilerini getirir
router.get    ('/:id',          getById)        //user id'si verilen user'ı getirir
router.put    ('/:id',          updateUser)     //user bilgilerini günceller
router.post   ('/',             createUser)     //yeni bir user ekler
router.delete ('/:id',          removeUser)     // user bilgisini siler
router.put    ('/:id/role',     setUserRole)    //user id'si verilen kişinin rollerini günceller
router.get    ('/:id/role',     getUserRole)    //user id'si verilen kişinin rollerini getirir


//-------------------------------------------------------------------------------

function authUser(req, res, next) {
    console.log("authUser");
  userService.authenticate(req.body).then(data => {
    if (data) {
      res.json(returnTemplate(data, ""))
    } else {
      res.json(returnTemplate([], "Username or password is incorrect",httpResult.error))
    }
  })
    .catch(err => next(err));
}


//-------------------------------------------------------------------------------

function getAll(req, res, next) {
    console.log("getAll");
    userService.getAll().then(data => {
        if (data.length) {
            res.json(returnTemplate(data, ""))
        } else {
            res.json(returnTemplate([], "Kayıt bulunamamıştır"))
        }
    })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function getById(req, res, next) {
    console.log("getById");
    const id = parseInt(req.params.id);

    userService.getById(id)
        .then(data => {
            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Bu id ye ait kayıt bulunamamıştır",httpResult.error))
            }
        })
        .catch(err => next(err));
}


//-------------------------------------------------------------------------------

function updateUser(req, res, next) 
{ 
    console.log("updateUser");
    const id = parseInt(req.params.id);
   /* res.json(returnTemplate([], "Not implemented yet.", httpResult.error))
*/
req.body.UpdatedBy=req.TokenUser.id;
console.log(req.TokenUser)
    userService.update(id, req.body)
        .then(data => {
            if (data) {
                res.json(returnTemplate(data, "Basarı ile güncellenmiştir"))
            } else {
                res.json(returnTemplate([], "güncelleme yapılamammıştır",httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function createUser(req, res, next) 
{
    console.log("createUser"); 

   req.body.CreatedBy=req.TokenUser.id;  
    userService.create(req.body)
        .then(status => {
            if(status){
                res.json(returnTemplate(status,"Basarı ile eklenmiştir"))
            }
        })
        .catch(err => next(err));
    
}

//-------------------------------------------------------------------------------

function removeUser(req, res, next) {
    console.log("removeUser");
    const id = parseInt(req.params.id)
   /* res.json(returnTemplate([], "Not implemented yet.", httpResult.error))
*/
    userService.remove(id)
        .then(status => {
            if (status) {
                res.json(returnTemplate(status, "Basarı ile silinmiştir"))
            }
            else {
               // res.sendStatus(404).json(returnTemplate(null, "silme yapılamammıştır"))
                res.json(returnTemplate([], "silme yapılamammıştır",httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function setUserRole(req, res, next) {
    console.log("setUserRole");
    const User_id = parseInt(req.params.id);

 
    userService.setUserRole(User_id,req.body)
        .then(status => {
            if (status) {
                res.json(returnTemplate(status, "Basarı ile işlem gerçekleşmiştir"))
            }
            else {
                res.json(returnTemplate([], "Atama yapılamammıştır",httpResult.error))
            }
        })
        .catch(err => next(err));
}

function getUserRole(req, res, next) {
    console.log("getUserRole");
    const User_id = parseInt(req.params.id);


   // res.json(returnTemplate([], "Not implemented yet.", httpResult.error))
    userService.getUserRoles(User_id).then(data => {
        if (data.length) {
            res.json(returnTemplate(data, ""))
        } else {
            res.json(returnTemplate([], "Kayıt bulunamamıştır",httpResult.error))
        }
    })
    .catch(err => next(err));
}

 
//-------------------------------------------------------------------------------

module.exports = router;
