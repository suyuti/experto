var express = require('express');
var router = express.Router();
const httpResult = require('../config');
const returnTemplate = require('../template/returnTemplate');
const projeService = require('../services/proje.service');
const isPaketleriService = require('../services/isPaketleri.service');
const adamAyService = require('../services/adamay.service');
const personelService = require('../services/personel.service');


router.get('/', getAll);                        //projeleri getiri
router.get('/:id', getById);                       //proje id'si verilen projenin bilgilerini getirir
router.put('/:id', updateProje);                   // proje id'si verilen projenin bilgilerini günceller
router.delete('/:id', removeProje);                   //proje id'si verilen projeyi siler
router.get('/:pid/ispaketi', getByIdIsPaketleri);  // projenin iş paketlerini getirir
router.post('/:pid/ispaketi', createByIdIsPaketleri);  // projenin iş paketi ekler r
router.post('/:pid/ispaketi/delete', deleteByIdIsPaketleri);  // projenin iş paketlerini siler




//-------------------------------------------------------------------------------

function getAll(req, res, next) {
    console.log("proje.getAll");
    projeService.getAll()
        .then(data => {
            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Kayıt bulunamamıştır"))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function getById(req, res, next) {
    console.log("proje.getById");
    const id = parseInt(req.params.id);

    projeService.getById(id)
        .then(data => {

            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Bu id ye ait kayıt bulunamamıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function updateProje(req, res, next) {
    console.log("proje.updateProje");
    const id = parseInt(req.params.id);

    projeService.update(id, req.body)
        .then(data => {
            if (data) {
                res.json(returnTemplate(data, "Basarı ile güncellenmiştir"))
            } else {
                res.json(returnTemplate([], "güncelleme yapılamammıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function removeProje(req, res, next) {
    console.log("proje.removeProje");
    const id = parseInt(req.params.id)
    projeService.remove(id)
        .then(status => {
            if (status) {
                res.json(returnTemplate(status, "Basarı ile silinmiştir"))
            }
            else {
                res.json(returnTemplate([], "silme yapılamammıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}


//-------------------------------------------------------------------------------

function getByIdIsPaketleri(req, res, next) {
    console.log("proje.getByIdIsPaketleri");
    const Projeid = parseInt(req.params.pid);

    isPaketleriService.getByProjeId(Projeid)
        .then(data => {

            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Bu id ye ait kayıt bulunamamıştır", httpResult.error))
            }
        })
        .catch(err => next(err));

}

//-------------------------------------------------------------------------------
function oranHatalimiKontrol(MusteriId, Projeid, isPaketiId, Oran) {
    if (Oran <= 0) {  // buraya gerekli kontroller eklenecek
        return true
    }
    return false

}

//iş paketi ekleme 
async function hesaplama(Projeid, payload) {
    //  if (id < 0) throw "negative id";
    payload.ProjeId = Projeid;
    payload.Adi = payload.IsPaketiAdi;


    //eksik veri kontrolünü yapar 
    if (!payload.Adi || !payload.Aciklama || !payload.ProjeId || !payload.BaslangicTarihi || !payload.BitisTarihi || !payload.ProfilId) {
        return [{}, " Eksik veri girdiniz."]
    }
    //projenin iş paketi kontrolü eger varsa null  yoksa oluşturur ve id dönderir
    let a = await isPaketleriService.checkByDateIsPaketiAndCreate(payload)
    let IsPaketiId = a[0];
    let IsPaketiColor = a[1];

    //iş paketi kontrolünü saglar 
    if (IsPaketiId == null || IsPaketiId == undefined) {
        return [{}, "Bu Tarihler Arasında İş Paketi Vardır "]
    }
    let TempFisrtAdamAy = []
    let Hatali = []
    let AtananPersonelAdamAy = []
    let guncellenecektAdamAyList = [];


    //proje idsinden musteri idsini getiren servistir
    let proje = await projeService.GetProjeFromtByProjeId(Projeid)
    let musteriId = proje.MusteriId;//projeye ait musteri
    let EkipId = proje.EkipId; //projenin tanımlı oldugu ekipler
    let IsPaketiBaslangicTarihi = new Date(payload.BaslangicTarihi); // iş paketi başlangıç tarihi
    let IsPaketiBaslangicAyi = IsPaketiBaslangicTarihi.getMonth() + 1; // iş paketi başlangıç ayi
    let IsPaketiBaslangicYili = IsPaketiBaslangicTarihi.getFullYear(); // iş paketi başlangıç yili


    let IsPaketiBitisTarihi = new Date(payload.BitisTarihi); // iş paketi bitiş tarihi
    let IsPaketiBitisAyi = IsPaketiBitisTarihi.getMonth() + 1; // iş paketi bitiş ayi
    let IsPaketiBitisYili = IsPaketiBitisTarihi.getFullYear(); // iş paketi bitiş yili
    //let IsPaketiId = await isPaketleriService.getIsPaketiId(Projeid); //projeye ait iş paketlerinin sayısı

    //musteriye ait ekip ve profiline uygun   personellerin listesini getirir
    let ProfileUygunMusterininPersonelleri = await personelService.getByMusteriFromProfilId(musteriId, payload.ProfilId, EkipId)
    // iş paketinin bitiş  tarihi ile başlangıc tarihleri arasında kaç  ay var omu bulur
    let IsPaketiBitisTarihiAyOlarak = IsPaketiBitisAyi + ((IsPaketiBitisYili - IsPaketiBaslangicYili) * 12);

    //kaç ay varsa o ay kadar donecek döngü
    for (let Ayindex = IsPaketiBaslangicAyi; Ayindex <= IsPaketiBitisTarihiAyOlarak; Ayindex++) {
        //    console.log(Ayindex  +" ay");// ay ay alınacak
        var AyAyIsPaketiBaslangicTarihi = new Date(IsPaketiBaslangicTarihi) // iş paketi başlangıç tarihi
        AyAyIsPaketiBaslangicTarihi.setMonth(Ayindex - 1);





        let Oran = 0;
        //dongu için oluşturulmus ay sırasıyla baslangıç ayından bitiş tarihindeki aya kadar
        let TempAyindex = (((Ayindex - 1) % 12) + 1);
        //dongu için oluşturuluş yıl başlangıç yılından bıtış yılına kadar
        let TempBaslangicYili = IsPaketiBaslangicYili + Math.floor((Ayindex - 1) / 12)
        //  Personel_Proje_AdamAy  veritabanında  musteriye ve   bu aya ve yıla ait atanmış kayıtları getirir
        let AyaGoreVeritabanindakiAdamayListesi = await adamAyService.getByMonthAdamAyAllPersonel(musteriId, TempAyindex, TempBaslangicYili)
        // console.log(AyaGoreVeritabanindakiAdamayListesi);


        AyaGoreVeritabanindakiAdamayListesi = AyaGoreVeritabanindakiAdamayListesi.map(function (adamAy) {
            // Personel_Proje_AdamAy  veritabanından bulunan kayıtların işpaketi boyunca 
            // hepsini  guncellenecektAdamAyList listesinde varsa hepsini aynı yapıyor
            var GuncelAdamay = guncellenecektAdamAyList.filter(function (element) {
                return element.PersonelId === adamAy.PersonelId
                    && element.ProjeId === adamAy.ProjeId
                    && element.IsPaketiId === adamAy.IsPaketiId;
            });
            if (GuncelAdamay.length > 0) {
                adamAy.Oran = GuncelAdamay[0].Oran;
                adamAy.readOnly = 1;
            }
            return adamAy
        });


        //iş paketine uygun personellerin listesini filtreliyor işe başlama tarihi kontrol ediyor
        var ProfileUygunMusterininPersonelleri_fitreli = ProfileUygunMusterininPersonelleri.filter((personel) => {
            var personelIseBaslamaTarihi = personel.IseBaslamaTarihi
            if (personelIseBaslamaTarihi) { // personelin işe başlama tarihi      
                if (personelIseBaslamaTarihi.ConvertMonth() <= AyAyIsPaketiBaslangicTarihi.ConvertMonth()) {
                    return true
                } else { return false }
            } else { return false } //işe başlama tarihi yoksa personel işleme dahil edilmeyecek  
        })

        //////ProfileUygunMusterininPersonelleri degiştirilecek ProfileUygunMusterininPersonelleri_fitreli
        let AtananAdamAy = ProfileUygunMusterininPersonelleri_fitreli.map(personel => {
            var personelIseBaslamaTarihi = personel.IseBaslamaTarihi

            // fitrelenmiş iş paketine uygun personel listesininin daha once atanmişdegiştirilemeyen adamay sayısını buluyor
            let sabitOran = AyaGoreVeritabanindakiAdamayListesi.filter((adamay) => {
                return adamay.PersonelId === personel.id && adamay.readOnly === 1;
            }).map((newPersonel) => {
                return newPersonel.Oran;
            }).reduce((toplam, Oran) => { return toplam + Oran; }, 0);

            //personelin onceden atanmış ama degiştirilebilir olan adamay sayısını getirir
            let DegiskenOranCount = AyaGoreVeritabanindakiAdamayListesi.filter((adamay) => {
                return adamay.PersonelId === personel.id && adamay.readOnly === null;
            }).map((newPersonel) => {
                return newPersonel.Oran;
            }).reduce((toplam, Oran) => { return toplam + 1; }, 0);


            //kaça bölecegimizi bulmak için 
            var tempDegiskenOranCount = DegiskenOranCount + 1;
            //atanması gereken oran hesaplanıyor
            Oran = Math.floor((10 - sabitOran) / (DegiskenOranCount + 1))

            //oran guncellemesi
            if (TempAyindex != IsPaketiBaslangicAyi && personelIseBaslamaTarihi.ConvertMonth() < AyAyIsPaketiBaslangicTarihi.ConvertMonth()) {
                //atanan iş paketinin tamamına aynı oran ataması yapmak için yazıldı
                let firstAdamAy = TempFisrtAdamAy.find((item) => {
                    return item.MusteriId === musteriId &&
                        item.PersonelId === personel.id &&
                        item.ProjeId === Projeid &&
                        item.IsPaketiId === IsPaketiId
                });
                Oran = firstAdamAy.Oran;
            }






            let tempOran = (10 - sabitOran) - Oran;
            tempDegiskenOranCount--
            if (DegiskenOranCount > 0) {
                //onceden atanmış olan adamay oranlarını guncellemek içinkullanıldı
                let degistirilecekler = AyaGoreVeritabanindakiAdamayListesi.filter(function (MonthPersonal) {
                    return MonthPersonal.PersonelId === personel.id && MonthPersonal.readOnly === null;
                }).map(function (newPersonel) {
                    newPersonel.Oran = Math.floor(tempOran / tempDegiskenOranCount)
                    tempDegiskenOranCount--
                    tempOran -= newPersonel.Oran
                    if (oranHatalimiKontrol(newPersonel.MusteriId, newPersonel.ProjeId, newPersonel.IsPaketiId, newPersonel.Oran)) {
                        Hatali.push(newPersonel);
                    }
                    return newPersonel;
                });
                //guncellenecek listesi oluşturuluyor
                guncellenecektAdamAyList = [...guncellenecektAdamAyList, ...degistirilecekler]
                //console.log(guncellenecektAdamAyList);
            }
            //adamay seması
            let newAdamAy = {
                Yil: TempBaslangicYili,
                Ay: TempAyindex,
                Oran: Oran,
                MusteriId: musteriId,
                PersonelId: personel.id,
                ProjeId: Projeid,
                IsPaketiId: IsPaketiId,
                "manualUpdateDate": null,
                "createDate": new Date(),
                "readOnly": 1,//defautl olarak bloklu oluyor
                "manualUpdate": null,
                "IsPaketiColor": IsPaketiColor
            }
            // readOnly olup olmadıgı burda kontrol ediliyor
            if ((TempAyindex == IsPaketiBaslangicAyi && TempBaslangicYili == IsPaketiBaslangicYili) || (personelIseBaslamaTarihi.ConvertMonth() == AyAyIsPaketiBaslangicTarihi.ConvertMonth())) {
                newAdamAy.readOnly = null
            }

            // console.log("personel  ",personel.Adi ) ;    
            // console.log("sabitOran ",sabitOran ) ;
            // console.log("DegiskenOranCount ",DegiskenOranCount ) ; 
            //oran hatalı mı kontrolü yapılıyor hatalı iise hatalı arrayına ekleniyor
            if (oranHatalimiKontrol(newAdamAy.MusteriId, newAdamAy.ProjeId, newAdamAy.IsPaketiId, newAdamAy.Oran)) {
                Hatali.push(newAdamAy);
            }

            ///   iş paketini ortasında çalışmaya baslayan kişilerin  TempFisrtAdamAy eklemesi
            if (TempAyindex == IsPaketiBaslangicAyi || personelIseBaslamaTarihi.ConvertMonth() == AyAyIsPaketiBaslangicTarihi.ConvertMonth()) {
                TempFisrtAdamAy.push(newAdamAy); // ilk elemanları tuttugumuz yer
            }
            return newAdamAy
        });
        //adamay atanmiş degerleri tuttugumuz array degişkenidir hesaplanan adamaydır
        AtananPersonelAdamAy = [...AtananPersonelAdamAy, ...AtananAdamAy]
        //let getByMounthAllIsPaketi = await isPaketleriService.getByMounthAllIsPaketi(index,musteriId) 

    }
    ///veri tabanındaki işpaketlerinin sonuna kadar  aynı oran olmasını saglayan işlem
    var guncellenecektAdamAyListTamammı = []
    for (let indexGuncellenecektAdamAyList = 0; indexGuncellenecektAdamAyList < guncellenecektAdamAyList.length; indexGuncellenecektAdamAyList++) {
        const item = guncellenecektAdamAyList[indexGuncellenecektAdamAyList];
        let tempGetIspaketininHepsi = await adamAyService.getIspaketininHepsi(item); //projeye ait iş paketlerinin sayısı 
        for (let indextempGetIspaketininHepsi = 0; indextempGetIspaketininHepsi < tempGetIspaketininHepsi.length; indextempGetIspaketininHepsi++) {
            const element = tempGetIspaketininHepsi[indextempGetIspaketininHepsi];
            element.Oran = item.Oran
            guncellenecektAdamAyListTamammı.push(element);
        }
    }
    //console.log(guncellenecektAdamAyListTamammı);


    //hata durumu var mı kontol eder yoksa adamay hesaplanan degerleri veritabanına yazılır
    if (Hatali.length == 0) {
        [...guncellenecektAdamAyListTamammı, ...AtananPersonelAdamAy].map(x => {
            adamAyService.Personel_Proje_AdamAy_create(x);
        });
        //let IsPaketiId = await isPaketleriService.create(payload);  
        return [{}, "Başarıyla  eklenmiştir"]
    } else {
        //hata durumu varsa veritabanına yazılan iş paketini siler
        await isPaketleriService.remove(IsPaketiId)
    }

    console.log(guncellenecektAdamAyListTamammı);
    //hata durumu varsa servisten donecek deger
    return [{
        guncellenecekTamamı: guncellenecektAdamAyListTamammı,
        //guncellenecek:guncellenecektAdamAyList,
        atanan: AtananPersonelAdamAy,
        hataliOlanlar: Hatali
    }, "Hatalı Durum Var"]
}
//projeye iş paketi ekleme yapar
function createByIdIsPaketleri(req, res, next) {
    try {
        console.log("proje.createByIdIsPaketleri");
        const Projeid = parseInt(req.params.pid);

        hesaplama(Projeid, req.body).then(([result, message]) => {

            res.json(returnTemplate(result, message))
            console.log(JSON.stringify(result))
        })
    }
    catch (e) {
        console.log(e)
    }
}



//-------------------------------------------------------------------------------



async function deleteHesaplama(Projeid, payload) {
    //  if (id < 0) throw "negative id";
    payload.ProjeId = Projeid;
    if (!payload.id) {
        return [{}, " Eksik veri girdiniz"]
    }
    let IsPaketiId = payload.id;
    await isPaketleriService.remove(IsPaketiId)

    let proje = await projeService.GetProjeFromtByProjeId(Projeid)
    let musteriId = proje.MusteriId;
    let EkipId = proje.EkipId;
    let BaslangicTarihi = new Date(payload.BaslangicTarihi);
    let BaslangicYili = new Date(payload.BaslangicTarihi).getFullYear();
    let BitisTarihi = new Date(payload.BitisTarihi);

    let Hatali = []
    let AtananPersonelAdamAy = []
    let ProfileUygunMusterininPersonelleri = await personelService.getByMusteriFromProfilId(musteriId, payload.ProfilId, EkipId)
    let guncellenecektAdamAyList = [];



    for (let Ayindex = BaslangicTarihi.getMonth() + 1; Ayindex < BitisTarihi.getMonth() + 1; Ayindex++) {
        console.log(Ayindex + " ay");// ay ay alınacak
        let oran = 0
        let AyaGoreVeritabanindakiAdamayListesi = await adamAyService.getByMonthAdamAyAllPersonel(musteriId, Ayindex, BaslangicYili)
        console.log(AyaGoreVeritabanindakiAdamayListesi);


        let AtananAdamAy = ProfileUygunMusterininPersonelleri.map(function (personel, indexCurrentPersonelList) {

            let sabitOran = AyaGoreVeritabanindakiAdamayListesi.filter(function (MonthPersonal) {
                return MonthPersonal.PersonelId === personel.id && MonthPersonal.readOnly === 1;
            })
                .map(function (newPersonel) {
                    return newPersonel.Oran;
                })
                .reduce(function (toplam, oran) {
                    return toplam + oran;
                }, 0);

            let DegiskenOranCount = AyaGoreVeritabanindakiAdamayListesi.filter(function (MonthPersonal) {
                return MonthPersonal.PersonelId === personel.id && MonthPersonal.readOnly === null;
            })
                .map(function (newPersonel) {
                    return newPersonel.Oran;
                })
                .reduce(function (toplam, oran) {
                    return toplam + 1;
                }, 0);

            var tempDegiskenOranCount = DegiskenOranCount;
            let tempOran = (10 - sabitOran);
            if (DegiskenOranCount > 0) {
                let degistirilecekler = AyaGoreVeritabanindakiAdamayListesi.filter(function (MonthPersonal) {
                    return MonthPersonal.PersonelId === personel.id && MonthPersonal.readOnly === null;
                }).map(function (newPersonel) {
                    newPersonel.Oran = Math.floor(tempOran / tempDegiskenOranCount)
                    tempDegiskenOranCount--
                    tempOran -= newPersonel.Oran

                    if (oranHatalimiKontrol(newPersonel.MusteriId, newPersonel.ProjeId, newPersonel.IsPaketiId, newPersonel.Oran)) {
                        Hatali.push(newPersonel);
                    }
                    return newPersonel;
                });
                guncellenecektAdamAyList = [...guncellenecektAdamAyList, ...degistirilecekler]
                //console.log(guncellenecektAdamAyList);
            }
            return null
        });
    }
    ///işpaketlerinin sonuna kadar ilem yapacagını gösterir 
    var guncellenecektAdamAyListTamammı = []

    for (let indexGuncellenecektAdamAyList = 0; indexGuncellenecektAdamAyList < guncellenecektAdamAyList.length; indexGuncellenecektAdamAyList++) {
        const item = guncellenecektAdamAyList[indexGuncellenecektAdamAyList];
        let tempGetIspaketininHepsi = await adamAyService.getIspaketininHepsi(item); //projeye ait iş paketlerinin sayısı
        for (let indextempGetIspaketininHepsi = 0; indextempGetIspaketininHepsi < tempGetIspaketininHepsi.length; indextempGetIspaketininHepsi++) {
            const element = tempGetIspaketininHepsi[indextempGetIspaketininHepsi];
            element.Oran = item.Oran
            guncellenecektAdamAyListTamammı.push(element);
        }
    }
    if (Hatali.length == 0) {
        guncellenecektAdamAyListTamammı.map(x => {
            adamAyService.Personel_Proje_AdamAy_create(x);
        });
        //let IsPaketiId = await isPaketleriService.create(payload);  
        return [{ guncellenecekTamamı: guncellenecektAdamAyListTamammı, }, "Başarıyla  silinmiştir"]
    } else {
        await isPaketleriService.remove(IsPaketiId)
    }

    //  console.log(guncellenecektAdamAyListTamammı); 
    return [{
        guncellenecekTamamı: guncellenecektAdamAyListTamammı,
        guncellenecek: guncellenecektAdamAyList,
        hataliOlanlar: Hatali
    }, "Hatalı Durum Var"]
}



function deleteByIdIsPaketleri(req, res, next) {
    try {
        console.log("proje.createByIdIsPaketleri");
        const Projeid = parseInt(req.params.pid);

        deleteHesaplama(Projeid, req.body).then(([result, message]) => {

            res.json(returnTemplate(result, message))
            console.log(JSON.stringify(result))
        })
    }
    catch (e) {
        console.log(e)
    }
}


module.exports = router;
