var express = require('express');
var router = express.Router();
const httpResult = require('../config');
const returnTemplate    = require('../template/returnTemplate');
const personelService = require('../services/personel.service'); 

 

router.get('/',                     getAll);            //Personel listesi doner
router.get('/:id',                  getById);           //Belli bir personeli doner
router.delete('/:id',               remove);            //Belli bir personeli siler
router.post('/:pid/musteri/:mid',   createForMusteri);  //Personeli musteride olusturur
router.post('/',                    createPersonel);    //Yeni Bir Personel oluşturur
router.put('/:pid/departman/:did',  setDepartment);     //Personelin departmanini set eder
router.put('/:pid/ekip/:eid',       setEkip);           //Personeli ekibe atar
router.put('/:pid/musteri/:mid',    setMusteri);        //Personeli musteriye atar
router.put('/:pid/profil/:prid',    setProfile);        //Personelin profilini set eder
router.put('/:id',                  update);            //Belli bir personeli günceller


//-------------------------------------------------------------------------------

function getAll(req, res, next)
{ 
    console.log("personel.getAll");
    personelService.getAllForList()
        .then(data => {
            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Kayıt bulunamamıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function getById(req, res, next)
{ 
    console.log("personel.getById");
    const id = parseInt(req.params.id);

    personelService.getById(id)
        .then(data => {

            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Bu id ye ait kayıt bulunamamıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function remove(req, res, next)
{ 
    console.log("personel.remove");
    const id = parseInt(req.params.id);

    personelService.remove(id)
        .then(status => {
            if (status) {
                res.json(returnTemplate(status, "Basarı ile silinmiştir"))
            }
            else {
                res.json(returnTemplate([], "silme yapılamammıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function createForMusteri(req, res, next)
{ 
    console.log("personel.createForMusteri");
    res.json(returnTemplate([], "Not implemented yet.", httpResult.error))
}


//-------------------------------------------------------------------------------

function setDepartment(req, res, next)
{ 
    console.log("personel.setDepartment");
    const personelId    = parseInt(req.params.pid);
    const departmentId  = parseInt(req.params.did);
    
    res.json(returnTemplate([], "Not implemented yet.", httpResult.error))
}

//-------------------------------------------------------------------------------

function setEkip(req, res, next)
{ 
    console.log("personel.setEkip");
    const personelId    = parseInt(req.params.pid);
    const ekipId        = parseInt(req.params.eid);
    
    res.json(returnTemplate([], "Not implemented yet.", httpResult.error))
}

//-------------------------------------------------------------------------------

function setMusteri(req, res, next)
{ 
    console.log("personel.setMusteri");
    const personelId    = parseInt(req.params.pid);
    const musteriId     = parseInt(req.params.mid);
    
    res.json(returnTemplate([], "Not implemented yet.", httpResult.error))
}

//-------------------------------------------------------------------------------

function setProfile(req, res, next) 
{ 
    console.log("personel.setProfile");
    const personelId    = parseInt(req.params.pid);
    const profileId     = parseInt(req.params.prid);
    
    // check profile

    
    res.json(returnTemplate([], "Not implemented yet.", httpResult.error))
}

//-------------------------------------------------------------------------------

function update(req, res, next) 
{ 
    console.log("personel.update");
    const personelId    = parseInt(req.params.id);

    personelService.update(personelId, req.body)
                  .then(status => {
                    if(status){
                        res.json(returnTemplate(status,"Basarı ile güncellenmiştir"))
                    }
                }).catch(err => next(err));    
}


//------------------------------------------------------------------

function createPersonel(req, res, next) {
    console.log("personel.createPersonel");
    //console.log(req); 
        if(!req.body.TcNo ){
            res.json(returnTemplate([],"TcNo Eksik. Lütfen Verilerinizi Kontrol Ediniz", httpResult.error));
            return;
        }else if(!req.body.Adi){
            res.json(returnTemplate([],"Adi  Eksik. Lütfen Verilerinizi Kontrol Ediniz", httpResult.error))
            return;
        }else if(!req.body.Soyadi){
            res.json(returnTemplate([],"Soyadi  Eksik. Lütfen Verilerinizi Kontrol Ediniz", httpResult.error))
            return;
        }else if(!req.body.MusteriId){
            res.json(returnTemplate([],"MusteriId  Eksik. Lütfen Verilerinizi Kontrol Ediniz", httpResult.error))
            return;
        }else if(!req.body.EkipId){
            res.json(returnTemplate([],"EkipId  Eksik. Lütfen Verilerinizi Kontrol Ediniz", httpResult.error))
            return;
        }  else if(!req.body.ProfilId){
            res.json(returnTemplate([],"ProfilId  Eksik. Lütfen Verilerinizi Kontrol Ediniz", httpResult.error))
            return;
        }    
 
        personelService.create(req.body)
        .then(status => {
            if(status){
                res.json(returnTemplate(status,"Basarı ile eklenmiştir"))
            }else{
                res.json(returnTemplate([],"beklenmeyen bir Hata oluştu",httpResult.error))
            }
        })
        .catch(err => next(err));


}
module.exports = router;
