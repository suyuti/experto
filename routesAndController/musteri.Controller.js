var     express             = require('express');
var     router              = express.Router(); 
const   returnTemplate    = require('../template/returnTemplate');
const   musteriService      = require('../services/musteri.service');
const   projeService        = require('../services/proje.service');
const   personelService     = require('../services/personel.service');
const   profilService     = require('../services/profil.service');
const   ekipService         = require('../services/ekip.service'); 
const   adamAyService         = require('../services/adamay.service'); 

router.get      ('/',                   getAll);                    // Musteri listesini getirir
router.get      ('/:id',                getById);                   // Musteri id'si verilen musteri bilgilerini getirir
router.put      ('/:id',                updateMusteri);             // Musteri id'si verilen musterinin bilgilerini günceller
router.post     ('/',                   createMusteri);             // yeni bir müşteri oluşturur
router.delete   ('/:id',                remove);                    // musteri id'si verilen Musteriyi siler
router.get      ('/:id/proje',          getProjeler);               // musteriye ait projeleri getirir
router.get      ('/:id/personel',       getPersoneller);            // musteriye ait personelleri getirir
router.post     ('/:id/personel/:pid',  addPersonel);               //musteriye ait personel bilgisini günceller.
router.get      ('/:id/ekip',           getEkipler);                // musteriye ait ekip listesini getirir
router.get      ('/:id/departman',      getDepartmanlar);           // musteriye ait departman listesini getirir 
router.post     ('/:id/proje',          createProjeForMusteri);     // musteriye yeni bir proje ekler
router.post     ('/:id/departman',      createDepartmanForMusteri); // musteriye yeni bir departman ekler
router.post     ('/:id/ekip',           createEkipForMusteri);      // musteriye yeni bir ekip ekler
router.post     ('/:id/personel',       createPersonelForMusteri);  // musteriye yeni bir personel ekler
//router.get      ('/:id/adamay',         getAdamAy);               // Musterinin adam aylarini getirir
router.post     ('/:id/adamay',         adamAyHesapla);             // Musteriye adam ay hesaplar
router.put      ('/adamay/:AAid/update',         adamAyUpdate);  // Musteriye adam ay günceller
router.get     ('/:id/personelProfil',          getPersonelProfilForMusteri);     // musteriye yeni bir proje ekler




//-------------------------------------------------------------------------------

function getAll(req, res, next) 
{
    console.log("musteri.getAll");
    musteriService.getAll().then(data => {
        if(data.length){
            res.json(returnTemplate(data,""))
        }else{
            res.json(returnTemplate([],"Kayıt bulunamamıştır"))
        }
    })
    .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function getPersonelProfilForMusteri(req, res, next) 
{   
    console.log("getPersonelProfilForMusteri");
    
    const Musteri_ID = parseInt(req.params.id);  
    profilService.getAllByMusteriId(Musteri_ID).then(data => {
        if(data.length){
            res.json(returnTemplate(data,""))
        }else{
            res.json(returnTemplate([],"Kayıt bulunamamıştır"))
        }
    })
    .catch(err => next(err));
}
//-------------------------------------------------------------------------------

function getById (req, res, next) 
{
    const id = parseInt(req.params.id);
    console.log(`musteri.getById ${id}`);

    musteriService.getById(id)
        .then(data => {
            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Bu id ye ait kayıt bulunamamıştır",httpResult.error))
            }
        })
        .catch(err => next(err));
}


//-------------------------------------------------------------------------------

function updateMusteri (req, res, next) 
{ 
    const id = parseInt(req.params.id);
    console.log(`musteri.updateMusteri ${id} ${req.body}`);

    musteriService.update(id, req.body)
        .then(data => {
            if (data) {
                res.json(returnTemplate(data, "Basarı ile güncellenmiştir"))
            } else {
                res.json(returnTemplate([], "güncelleme yapılamammıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function createMusteri(req, res, next) {
    console.log("createMusteri");
   // console.log('Musteri create: ' + JSON.stringify(req.body))
    musteriService.create(req.body)
        .then(status => {
            if (status) {
                res.json(returnTemplate(status, "Basarı ile eklenmiştir"))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function remove (req, res, next) 
{ 
    console.log("deleteMusteri");
    const id = parseInt(req.params.id);
    musteriService.remove(id)
        .then(status => {
            if (status) {
                res.json(returnTemplate(status, "Basarı ile silinmiştir"))
            }
            else {
                res.json(returnTemplate([], "silme yapılamammıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function getProjeler (req, res, next) 
{ 
    const id = parseInt(req.params.id);
    console.log(`musteri.getProjeler ${id}`)

    projeService.getAllByMusteriId(id)
        .then(data => {
            console.log(data)
            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Bu id ye ait kayıt bulunamamıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function getPersoneller (req, res, next) 
{ 
    console.log("getPersoneller");
    const id = parseInt(req.params.id);

    personelService.getAllByMusteriId(id)
        .then(data => {
            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Bu id ye ait kayıt bulunamamıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function addPersonel (req, res, next) 
{ 
    console.log("addPersonel");
    res.json(returnTemplate([], "Not implemented yet.", httpResult.error))
    /*
    personelService.create(req.body)
        .then(status => {
            if (status) {
                res.json(returnTemplate(status, "Basarı ile eklenmiştir"))
            }
        })
        .catch(err => next(err));
 */
}

//-------------------------------------------------------------------------------

function getEkipler (req, res, next) 
{ 
    console.log("getEkipler");
    const id = parseInt(req.params.id);
    // res.json(returnTemplate([], "Not implemented yet.", httpResult.error))

    ekipService.getEkipByMusteriId(id)
    .then(data => {
        if (data.length) {
            res.json(returnTemplate(data, ""))
        } else {
            res.json(returnTemplate([], "Bu id ye ait kayıt bulunamamıştır"))
        }
    })
    .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function getDepartmanlar (req, res, next) 
{ 
    console.log("getDepartmanlar");
    res.json(returnTemplate([], "Not implemented yet.", httpResult.error)) 
}

//-------------------------------------------------------------------------------

function createProjeForMusteri (req, res, next) 
{  
    console.log("createProjeForMusteri");  
    const id = parseInt(req.params.id);  
    req.body.MusteriId=id;

    if(!req.body.MusteriId || !req.body.ProjeAdi || !req.body.ProjeKodu || !req.body.EkipId){
        res.json(returnTemplate([], "Verileriniz Eksik Lütfen Kontrol Edin", httpResult.error)) 
    } 


    projeService.getByProjeKodu(req.body.ProjeKodu)
        .then(result => {
            if (result.length >0) {
                res.json(returnTemplate([], "Aynı  ProjeKodu na Sahip Proje Vardır", httpResult.error));
            } else {
                projeService.create(req.body)
                    .then(result => {
                        if (result) {
                            res.json(returnTemplate(result, ""))
                        } else {
                            res.json(returnTemplate([], "Bu musteriye Proje eklenememiştir", httpResult.error))
                        }
                    })
                    .catch(err => next(err)); 
            }
        }) 
}

//-------------------------------------------------------------------------------

function createDepartmanForMusteri (req, res, next) 
{ 
    console.log("createDepartmanForMusteri");
    res.json(returnTemplate([], "Not implemented yet.", httpResult.error))
}

//-------------------------------------------------------------------------------

function createEkipForMusteri (req, res, next) 
{ 
    console.log("createEkipForMusteri");
    res.json(returnTemplate([], "Not implemented yet.", httpResult.error))
}

//-------------------------------------------------------------------------------

function createPersonelForMusteri (req, res, next) 
{ 
    
    console.log("createPersonelForMusteri");
    res.json(returnTemplate([], "Not implemented yet.", httpResult.error))
}

//-------------------------------------------------------------------------------

function adamAyHesapla(req, res, next) 
{
    console.log("AdamAyHesapla") 
    const musteriId = parseInt(req.params.id);
     console.log(req.body ,musteriId )
/*
    adamAyService.hesapla(req.body.baslangicYil, req.body.baslangicAy, req.body.sure, id)
        .then(result => {
            res.json(returnTemplate([result], ""))
        })
        .catch(err => next(err));
*/ 
console.log(new Date(req.body.startDate).getFullYear());

        adamAyService.adamAyHesapla(musteriId,req.body.startDate, req.body.endDate )
        .then(result => {
            res.json(returnTemplate(result, ""))
        })
        .catch(err => next(err));    
}

function getAdamAy(req, res, next) {
    console.log("getAdamAy")
    const id = parseInt(req.params.id); 
    adamAyService.getByHesaplamaId(id)
        .then(data => {
            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Bu id ye ait kayıt bulunamamıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}


function adamAyUpdate(req, res, next) 
{
    console.log("adamAyUpdate") 
    const adamayId = parseInt(req.params.AAid); 
/*
    adamAyService.hesapla(req.body.baslangicYil, req.body.baslangicAy, req.body.sure, id)
        .then(result => {
            res.json(returnTemplate([result], ""))
        })
        .catch(err => next(err));
*/ 
 
        adamAyService.adamAyUpdate(req.body)
        .then(result => { 
            res.json(returnTemplate(result, "başarıyla güncellenmiştir"))
        })
        .catch(err => next(err));  

}


module.exports = router;
