var     express         = require('express');
var     router          = express.Router();
const httpResult = require('../config');
const returnTemplate    = require('../template/returnTemplate');
const   PermissionService    = require('../services/permission.service');

 
router.get      ('/',       getAll);// Permissions listesi doner
router.post     ('/',       create);// Permissions Oluşturur
router.get      ('/:id',    getById); // idsi verilen Permissions getirir
router.put      ('/:id',    update);// Permissions gunceller
router.delete   ('/:id',    remove); // Permissions siler (Status = 0)

//-------------------------------------------------------------------------------

function getAll(req, res, next) 
{
    console.log("getAll");
    PermissionService.getAll().then(data => {
        if(data.length){
            res.json(returnTemplate(data,""))
        }else{
            res.json(returnTemplate([],"Bu id ye ait kayıt bulunamamıştır"))
        }
    })
    .catch(err => next(err));}

//-------------------------------------------------------------------------------

function create(req, res, next) 
{  
    console.log("create");
    PermissionService.create(req.body)
        .then(status => {
            if(status){
                res.json(returnTemplate(status,"Basarı ile eklenmiştir"))
            }
        })
        .catch(err => next(err));
}
//-------------------------------------------------------------------------------

function getById(req, res, next) 
{
    console.log("getById");
    const id = parseInt(req.params.id);
    PermissionService.getById(id) 
        .then(data => {
            if(data.length){
                res.json(returnTemplate(data,""))
            }else{
                res.json(returnTemplate([],"Bu id ye ait kayıt bulunamamıştır", httpResult.error))
            }
        })
        .catch(err => next(err));}

//-------------------------------------------------------------------------------

function update(req, res, next) 
{
    console.log("update");
    const id = parseInt(req.params.id);

    PermissionService.update(id, req.body)
                    .then(data => {
                        if(data){
                            res.json(returnTemplate(data,"Basarı ile güncellenmiştir"))
                        }else{
                            res.json(returnTemplate([],"güncelleme yapılamammıştır", httpResult.error))
                        }
                    })
                     .catch(err => next(err));}

//-------------------------------------------------------------------------------

function remove(req, res, next) 
{
    console.log("remove");
    const id = parseInt(req.params.id) 

    PermissionService.remove(id)
    .then(status => {
        if(status){
            res.json(returnTemplate(status,"Basarı ile silinmiştir"))
        }
        else{
            res.json(returnTemplate([],"silme yapılamammıştır", httpResult.error))
        }
    })
    .catch(err => next(err));}

//-------------------------------------------------------------------------------



module.exports = router;
