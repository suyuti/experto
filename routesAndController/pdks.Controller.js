var express = require('express');
var router = express.Router();
const httpResult = require('../config');
const returnTemplate = require('../template/returnTemplate');
const PdksService = require('../services/pdks.service');
const PersonelService = require('../services/personel.service');
const command = require('../services/command')
var moment = require('moment');



router.get('/', getAll);    // Pdks listesi doner
router.post('/', create);    // Pdks oluşturur
router.get('/:id', getById);   // idsi verilen Pdks getirir
router.put('/:id', update);    // Pdks gunceller
router.delete('/:id', remove);    // Pdks siler (Status = 0)
router.post('/check', check);    // Pdks kontrol edip frontente düzenlenmesi gereken verileri gönderiri
router.post('/hesapla', hesapla);    // Pdks siler (Status = 0)


var pdksMethod = {}

// let pdksType=[
//     { id: 0, name: "NORMAL ÇALIŞMA  " }, 
//     { id: 1, name: "HAFTA TATİLİ   " },
//     { id: 2, name: "GENEL TATİLLER   " },
//     { id: 3, name: "YILLIK ÜCRETLİ İZİNLER  " },
//     { id: 4, name: "ÜCRET FARKI " },
//     { id: 5, name: " PRİM - İKRAMİYE  " },
//     { id: 6, name: "BAYRAM - YEMEK - KİRA " },
//     { id: 7, name: "İZİN HARÇLIĞI" },
//     { id: 8, name: "KIDEM ZAMMI " },
//     { id: 9, name: "ÇOCUK ZAMMI " },
//     { id: 10, name: "YAKACAK YARDIMI " },
//     { id: 11, name: "FAZLA MESAİ ÜCRETİ" },
//     { id: 12, name: "DOĞUM İZNİ " },
//     { id: 13, name: "SÜT İZNİ   " },
//     { id: 14, name: "EVLİLİK İZNİ  " },
//     { id: 15, name: "ÖLÜM İZNİ " },
//     { id: 16, name: "MAZERET İZNİ  " },
//     { id: 17, name: "RAPORLU OLAN SÜRELER" },
//     { id: 18, name: "Dış Görev" }
// ];

//-------------------------------------------------------------------------------

function getAll(req, res, next) {
    console.log("getAll");
    PdksService.getAll()
        .then(data => {
            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Kayıt bulunamamıştır"))
            }
        })
        .catch(err => next(err));
}



//-------------------------------------------------------------------------------
//bu servis pdks den gelen verilerin kontrol edildigi bolumdur. 
function check(req, res, next) {
    console.log("check");

    var base64 = req.body.files.base64;
    var MusteriId = req.body.MusteriId;
    if (MusteriId == undefined) { // musteri ıd kontrolü
        res.json(returnTemplate([], "Lutfen Musteri Seçiniz", httpResult.error))
    }


    var excelData = command.get_From_base64_To_xmlsData(base64); //base 64 formatında gelen excel verisini bilgisayarın anlayacagı formata dondurmek için kullanıldı

    PdksService.getShemaByMusteriId(MusteriId)//musteri hangı shemaya tanımlandıysa onun bilgisini getirir
        .then(data => {
            if (data == null) {
                res.json(returnTemplate([], "Bu Musteriye ait shema bulunamanıştır. lutfen musteriye shema atayınız", httpResult.error))
            } else {
                try {
                    //bu musteriye ait olan aktif çalışan personellerini listesini  getirir 
                    PersonelService.getAllByMusteriIdPDKS(MusteriId).then((personelList) => {
                        //musteri hangi shemaya tanımlandıysa o pdks ornek functionuna excel ve personler listesi datasını gondermek için kullanılır.
                        // ve  duzenlenmiş pdks ve personel listesi geri döndürür 
                        var [Pdks, PersonelError] = pdksMethod[`PDKS_Ornek_${data}`](excelData, personelList);

                        if (typeof Pdks === 'string') /// hata durumunda dönecek veri
                        { return res.json(returnTemplate([], Pdks, httpResult.error)); }
                        //res.json(returnTemplate(Pdks, `PDKS_Ornek_${data}`)); 
                        // kaç tane eşleşmiş personel var onun kontrolüdur
                        var Array_PersonelError_SadelestirilmisAdSoyad = PersonelError.eslesmis.map(item => item.SadelestirilmisAdSoyad);

                        for (let index = 0; index < Array_PersonelError_SadelestirilmisAdSoyad.length; index++) {
                            const element = Array_PersonelError_SadelestirilmisAdSoyad[index];
                            personelList = personelList.filter((item) => { //musterinin personeller tablosunda var mı diye kontrol ediyor
                                //pdks deki eşlesem personellerin listesini eşleşmeyenleri bulmak için personel listesinden cıkarıyor
                                return item.SadelestirilmisAdSoyad !== element
                            })
                        }

                        //ust tarafta cıkardıgı pdks ile eşleşmeyenleri dbVarPdksYok tablosuna atıyor
                        PersonelError.dbVarPdksYok = personelList;

                        //eger uyuşmayanlar var ise hata olarak donuyor ve ui kusmında model açılıp kimlerin eşleşmedigini gösteriyor
                        if (PersonelError.pdksVarDbYok.length > 0 || PersonelError.dbVarPdksYok.length > 0) {
                            res.json(returnTemplate(PersonelError, `PDKS_Ornek_${data}`, httpResult.error));
                        } else {
                            //hesaplanması için frontende duzenlenmiş veriyi gönderiyor.
                            res.json(returnTemplate(Pdks, `PDKS_Ornek_${data}`));
                        }
                    })
                        .catch(err => next(err));
                } catch (error) {
                    res.json(returnTemplate([], "Beklenmeyen bir hata oluştu", httpResult.error));
                }
            }
        })
        .catch(err => next(err));


}
//------------------------------------------------------------------------------- 
function create(req, res, next) {
    console.log("create");
    var base64 = req.body.files.base64;

    var excelData = command.get_From_base64_To_xmlsData(base64);
    //res.json(returnTemplate(PDKS_Ornek_1(excelData), "PDKS_Ornek_1"))


    res.json(returnTemplate(PDKS_Ornek_1(excelData), "PDKS_Ornek_1"))

    // PdksService.create(req.body)
    //     .then(status => {
    //         if (status) {
    //             res.json(returnTemplate(status, "Basarı ile eklenmiştir"))
    //         }
    //     })
    //     .catch(err => next(err));
}
//------------------------------------------------------------------------------- 
function getById(req, res, next) {
    console.log("getById");
    const id = parseInt(req.params.id);

    PdksService.getById(id)
        .then(data => {

            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Bu id ye ait kayıt bulunamamıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}
//------------------------------------------------------------------------------- 
function update(req, res, next) {
    console.log("update");
    const id = parseInt(req.params.id);

    PdksService.update(id, req.body)
        .then(data => {
            if (data) {
                res.json(returnTemplate(data, "Basarı ile güncellenmiştir"))
            } else {
                res.json(returnTemplate([], "güncelleme yapılamammıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}
//------------------------------------------------------------------------------- 
function remove(req, res, next) {
    console.log("remove");
    const id = parseInt(req.params.id)

    PdksService.remove(id)
        .then(status => {
            if (status) {
                res.json(returnTemplate(status, "Basarı ile silinmiştir"))
            }
            else {
                res.json(returnTemplate([], "silme yapılamammıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}
//------------------------------------------------------------------------------- 
var personeller = []

class PDKSPERSONEL {
    constructor(AdSoyad, Sure, CalistigiGun, ResmiTatil, YillikIzin, DisGorev, ArgeGunu) {
        this.AdSoyad = AdSoyad ? AdSoyad : "";
        this.SadelestirilmisAdSoyad = command.Pdks_sadelestir(this.AdSoyad)
        this.Sure = Sure ? Sure : 0;
        this.CalistigiGun = CalistigiGun ? CalistigiGun : 0;
        this.GunListesi = [];
        this.ResmiTatil = ResmiTatil ? ResmiTatil : 0;
        this.YillikIzin = YillikIzin ? YillikIzin : 0;
        this.DisGorev = DisGorev ? DisGorev : 0;
        this.ArgeGunu = ArgeGunu ? ArgeGunu : 0;
        this.cumartesiHakki = 0;
        this.AyinhaftalikToplamlari = {
            "1": null,
            "2": null,
            "3": null,
            "4": null,
            "5": null
        }
    }

}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sayfa1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sayfa1' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {
        var tempPDKS = {}
        var TcNo = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var ColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnAciklama = ""

        var ColumnGirisTarih = Sheets["C" + index] ? Sheets["C" + index].w : "";
        var ColumnGiris = Sheets["D" + index] ? Sheets["D" + index].w : "";



        var ColumnCikisTarih = Sheets["E" + index] ? Sheets["E" + index].w : "";
        var ColumnCikis = Sheets["F" + index] ? Sheets["F" + index].w : "";



        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";
        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;
        if (TarihRegex.test(ColumnGirisTarih)) {
            ColumnGirisTarih = new Date(moment(ColumnGirisTarih, "MM.DD.YYYY"));
            ColumnCikisTarih = new Date(moment(ColumnCikisTarih, "MM.DD.YYYY"));
            tempPDKS["Tarih"] = ColumnGirisTarih
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            // var c = ColumnGirisTarih.split("[./]");
            // var d = ColumnCikisTarih.split("[./]");

            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")

            var gunfark = 0
            var CikisAy = ColumnCikisTarih.getMonth() * 12 + ColumnCikisTarih.getDate(),
                girisAy = ColumnGirisTarih.getMonth() * 12 + ColumnGirisTarih.getDate()
            gunfark = CikisAy - girisAy

            if (gunfark > 0) {
                a = "0" + (parseInt(temp[0]) + (gunfark * 24));
                b = "0" + temp[1];
            } else {
                a = "0" + temp[0];
                b = "0" + temp[1];
            }
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)
            continue;
        } else {
            var t = command.Pdks_sadelestir(ColumnAciklama)
            if (t == "yillikizi̇n") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_1"] = (excelData, personelList) => {
    var Sheets = excelData.Sheets["Sayfa1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sayfa1' olmalı "];
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    // let d = new Date(Sheets["Sayfa1"]["I2"].w);//ilk rasgele alacagımız tarih 
    // var AydaHaftadaKacGunVar = d.haftadaKacGunVarHaftasonuHaric();
    // console.log(AydaHaftadaKacGunVar);
    var ObjectKeyList = Object.keys(Sheets)
    // console.log(ObjectKeyList)

    //ObjectKeyList = ObjectKeyList.splice(1, ObjectKeyList.length - 2)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    console.log(ObjectKeyList)

    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));

    console.log(excelboyutu)
    // console.log(command.Pdks_sadelestir(Sheets["C2"].v )) 
    for (let index = 2; index <= excelboyutu; index++) {
        var tempPDKS = {}
        var ColumnAdi = Sheets["C" + index] ? Sheets["C" + index].v : "";
        var ColumnAciklama = Sheets["L" + index] ? Sheets["L" + index].v : "";
        var ColumnGiris = Sheets["J" + index] ? Sheets["J" + index].w : "";
        var ColumnCikis = Sheets["K" + index] ? Sheets["K" + index].w : "";
        var ColumnTarih = Sheets["I" + index] ? Sheets["I" + index].w : "";
        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";
        var TarihRegex = /[0-9]+[/][0-9]+[/][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;
        if (TarihRegex.test(ColumnTarih)) {
            tempPDKS["Tarih"] = new Date(ColumnTarih);
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            a = "0" + temp[0];
            b = "0" + temp[1];
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);
            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)
        } else {
            var t = command.Pdks_sadelestir(ColumnAciklama)
            if (t == "yillikizi̇n") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        //if (!ColumnTarih.haftaSonuMu()) {
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));

        personelControl(personelList, tempPDKS, PersonelError)
        console.log(PersonelError);
        //} 
    }






    // for (let index = 2; index < excelboyutu; index++) {
    //     var adiSoyadiColumn = Sheets["C" + index].v;
    //     var tarihColumn = new Date(Sheets["E" + index].w);
    //     var now = new Date();
    //     var tarihColumnDateObject = new Date(tarihColumn.getTime() + 10800000);
    //     var hafta = tarihColumnDateObject.getWeekOfMonth().toString();//kaçıncı hafta oldugunu buluyoruz
    //     var girisColumn = Sheets["F" + index].w;
    //     var cıkısColumn = Sheets["G" + index].w;
    //     var aciklamaColumn = Sheets["H" + index];
    //     if (aciklamaColumn !== undefined) {
    //         aciklamaColumn = aciklamaColumn.w
    //     }
    //     var pattSaat = /[0-9]*:[0-9]*:[0-9]*/i;

    //     var IndexPersonel = personeller.findIndex((item) => {
    //         return item.SadelestirilmisAdSoyad === command.Pdks_sadelestir(adiSoyadiColumn);
    //     });

    //     if (IndexPersonel > -1) {
    //         new_PDKSPERSONEL = personeller[IndexPersonel];

    //     } else {
    //         var new_PDKSPERSONEL = new PDKSPERSONEL();
    //         new_PDKSPERSONEL.AdSoyad = adiSoyadiColumn;
    //         new_PDKSPERSONEL.SadelestirilmisAdSoyad = command.Pdks_sadelestir(adiSoyadiColumn);
    //     }
    //     if (pattSaat.test(girisColumn) && pattSaat.test(cıkısColumn)) {
    //         var tempSure = command.gunSaatCıkart(girisColumn, cıkısColumn)
    //         if (command.gunSaatKarsılastır(tempSure, "0:10:30") == 1) {/// günlük çalışması 10:30 saatten buyyuk ise 10:30 saat yapma
    //             tempSure = "0:10:30";
    //         }
    //         //new_PDKSPERSONEL.Sure = command.gunSaatTopla(new_PDKSPERSONEL.Sure, tempSure)  
    //         var IndexCalistigiGun = new_PDKSPERSONEL.GunListesi.findIndex((item) => {
    //             return item === tarihColumnDateObject
    //         });
    //         if (IndexCalistigiGun == -1) {   //  aynı günden den çalışan yoksa  
    //             new_PDKSPERSONEL.GunListesi.push({ tarih: tarihColumnDateObject, saat: tempSure, hafta: hafta, type: "" });
    //         } else { //eger aynı günden 2 tane varsa bu calıştıgı saatleri toplayıp 10:30 saatten buyuk mu diye kontrol yapıyor
    //             new_PDKSPERSONEL.GunListesi[IndexCalistigiGun].saat = command.gunSaatTopla(new_PDKSPERSONEL.GunListesi[IndexCalistigiGun].saat, tempSure)
    //             if (command.gunSaatKarsılastır(new_PDKSPERSONEL.GunListesi[IndexCalistigiGun].saat, "0:10:30") == 1) {/// günlük çalışması 10:30 saatten buyyuk ise 10:30 saat yapma
    //                 new_PDKSPERSONEL.GunListesi[IndexCalistigiGun].saat = "0:10:30";
    //             }
    //         }

    //     } else if (pattSaat.test(girisColumn) || pattSaat.test(cıkısColumn)) {
    //         console.warn("error", command.Pdks_sadelestir(adiSoyadiColumn), tarihColumnDateObject, girisColumn, cıkısColumn, "\n")
    //     } else {
    //         var t = command.Pdks_sadelestir(aciklamaColumn)
    //         if (t == "yillikizi̇n") {
    //             new_PDKSPERSONEL.YillikIzin++
    //             new_PDKSPERSONEL.GunListesi.push({ tarih: tarihColumnDateObject, saat: "0:09:00", hafta: hafta, type: t });

    //         }
    //         else if (t == "resmitati̇l") {
    //             new_PDKSPERSONEL.ResmiTatil++
    //             new_PDKSPERSONEL.GunListesi.push({ tarih: tarihColumnDateObject, saat: "0:09:00", hafta: hafta, type: t });

    //         }
    //         else {
    //             new_PDKSPERSONEL.DisGorev++
    //             new_PDKSPERSONEL.GunListesi.push({ tarih: tarihColumnDateObject, saat: "0:09:00", hafta: hafta, type: t });

    //         }
    //         //console.log("error", command.Pdks_sadelestir(adiSoyadiColumn), tarihColumn, girisColumn, cıkısColumn)
    //     }
    //     if (IndexPersonel == -1) { ///personel eklenecekse kontrolü
    //         personeller.push(new_PDKSPERSONEL);
    //     }
    // }

    // for (let j = 0; j < personeller.length; j++) {
    //     var personel = personeller[j];

    //     var haftalar = Object.keys(AydaHaftadaKacGunVar);
    //     for (let index = 0; index < haftalar.length; index++) {
    //         const hafta = haftalar[index];
    //         AydaHaftadaKacGunVar[hafta]; //haftadaki gün sayısı
    //         var haftalikKontrolSaati = command.haftalikKontrolSaati(AydaHaftadaKacGunVar[hafta])//cumartesiyide cıkarttık -1 yaparak
    //         var haftalikKontrolSaatininYarisi = command.haftalikKontrolSaatininYarisi(AydaHaftadaKacGunVar[hafta])

    //         var personelGunListesi = personel.GunListesi.filter(item => item.hafta === hafta)
    //         for (let tempp = 0; tempp < personelGunListesi.length; tempp++) {
    //             const element = personelGunListesi[tempp];
    //             var ttemp = personeller[j].AyinhaftalikToplamlari[hafta];
    //             var ss = command.gunSaatTopla(ttemp, element.saat);
    //             if (command.gunSaatKarsılastır(ss, haftalikKontrolSaati) == 1) {
    //                 ss = haftalikKontrolSaati;
    //             }
    //             personeller[j].AyinhaftalikToplamlari[hafta] = ss;
    //         }

    //         if (command.gunSaatKarsılastır(ss, haftalikKontrolSaatininYarisi) == 1) {
    //             personeller[j].cumartesiHakki++
    //         }
    //     }
    //     for (let index = 0; index < haftalar.length; index++) {
    //         const hafta = haftalar[index];
    //         var temps = personeller[j].AyinhaftalikToplamlari[hafta]
    //         var ss = command.gunSaatTopla(personeller[j].Sure, temps);
    //         personeller[j].Sure = ss
    //     }
    //     personeller[j].Sure = ss

    //     if (!d.sonHaftaCumartesiVarMi()) { //son hafta cumartesi varmı yok mu kontorlü
    //         personeller[j].cumartesiHakki--
    //     }

    //     var toplamGun = command.gunSaatcevirDakika(personeller[j].Sure) / (9 * 60);
    //     if ((toplamGun + personeller[j].cumartesiHakki) > 26.5) {
    //         //personel tam çalışmış sayılıyor
    //         personeller[j].CalistigiGun = 30
    //     } else {
    //         //resmi tatil ve yıllık izin cıkartılıp ve cumartesi hakkı eklenip hesaplanıyor yuvarlanıyor
    //         personeller[j].CalistigiGun = Math.round(toplamGun - personeller[j].YillikIzin - personeller[j].ResmiTatil + personeller[j].cumartesiHakki);
    //     }


    // }


    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_2"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Süreler"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Süreler' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    //ObjectKeyList = ObjectKeyList.sort();

    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));

    var chars = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ'
    ]



    console.log(excelboyutu)


    var tarihIndex = null

    for (let index = 1; index < excelboyutu; index++) {
        var tempPDKS = {}
        var TcNo = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var ColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnAciklama = ""
        var ColumnGirisTarih = "";
        var ColumnGiris = "";
        var ColumnCikisTarih = "";
        var ColumnCikis = "";
        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;

        if (command.Pdks_sadelestir(ColumnAdi) !== "adisoyadi" && tarihIndex == null) {
            continue;
        } else if (command.Pdks_sadelestir(ColumnAdi) == "adisoyadi") {
            tarihIndex = index;
            continue;
        } else if (command.Pdks_sadelestir(ColumnAdi) == "resmitatil") {
            break;
        }

        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";

        for (let sutun = 2; sutun < chars.length; sutun++) {
            const sutunChar = chars[sutun];
            var sutunCharValue = Sheets[sutunChar + tarihIndex] ? Sheets[sutunChar + tarihIndex].w : undefined
            if (sutunCharValue == undefined || !TarihRegex.test(sutunCharValue)) {
                continue;
            } else {
                ColumnGirisTarih = new Date(moment(sutunCharValue, "MM.DD.YYYY"));  //"9/1/19"

                tempPDKS["Tarih"] = ColumnGirisTarih
                var calismaDurumu = Sheets[sutunChar + index] ? Sheets[sutunChar + index].w : ""

                if (!pattSaat.test(calismaDurumu)) {
                    calismaDurumu = command.Pdks_sadelestir(calismaDurumu)
                }

                if (calismaDurumu == "rt") {
                    console.log();
                }

                if (pattSaat.test(calismaDurumu)) {
                    tempPDKS["Giris"] = "09:00";
                    tempPDKS["Cikis"] = command.gunSaatTopla(tempPDKS["Giris"], calismaDurumu);
                    tempPDKS["Type"] = 0


                } else if (!calismaDurumu || calismaDurumu == "ht" || calismaDurumu == "de") {
                    continue
                } else if (calismaDurumu == "m") {
                    tempPDKS["Giris"] = "00:00";
                    tempPDKS["Cikis"] = "00:00";
                    tempPDKS["Type"] = 16
                } else if (calismaDurumu == "g") {
                    tempPDKS["Giris"] = "00:00";
                    tempPDKS["Cikis"] = "00:00";
                    tempPDKS["Type"] = 18
                } else if (calismaDurumu == "yi") {
                    tempPDKS["Giris"] = "00:00";
                    tempPDKS["Cikis"] = "00:00";
                    tempPDKS["Type"] = 3
                } else if (calismaDurumu == "r") {
                    tempPDKS["Giris"] = "00:00";
                    tempPDKS["Cikis"] = "00:00";
                    tempPDKS["Type"] = 17
                } else {
                    tempPDKS["Type"] = null;
                }


                var resmiTatil = AyaAitResmiTatil.filter((item) => {
                    return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
                });
                if (resmiTatil.length > 0) {
                    tempPDKS["Type"] = 2
                }


                pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
                personelControl(personelList, tempPDKS, PersonelError)

            }

        }


    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_3"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sayfa1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sayfa1' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {
        var tempPDKS = {}
        var ColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnSoyadi = Sheets["C" + index] ? Sheets["C" + index].v : "";
        var ColumnAciklama = ""
        var ColumnGiris = Sheets["E" + index] ? Sheets["E" + index].w : "";
        var ColumnCikis = Sheets["I" + index] ? Sheets["I" + index].w : "";

        var ColumnGirisTarih = Sheets["D" + index] ? Sheets["D" + index].w : "";
        var ColumnCikisTarih = Sheets["H" + index] ? Sheets["H" + index].w : "";


        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi + " " + ColumnSoyadi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"]);
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";
        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;
        if (TarihRegex.test(ColumnGirisTarih)) {

            tempPDKS["Tarih"] = ColumnGirisTarih.convertTrDateToGlobalDate();
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            var c = ColumnGirisTarih.split("[./]");
            var d = ColumnCikisTarih.split("[./]");

            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")

            var gunfark = 0
            var CikisAy = parseInt(d[1]) * 12 + parseInt(d[0]),
                girisAy = parseInt(c[1]) * 12 + parseInt(c[0])
            gunfark = CikisAy - girisAy


            if (gunfark > 0) {
                a = "0" + (parseInt(temp[0]) + (gunfark * 24));
                b = "0" + temp[1];
            } else {
                a = "0" + temp[0];
                b = "0" + temp[1];
            }
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)
        } else {
            var t = command.Pdks_sadelestir(ColumnAciklama)
            if (t == "yillikizi̇n") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_4"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sayfa1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sayfa1' olmalı "];
    var TarihRegex = /[0-9]+[.\/][0-9]+[.\/][0-9]+/i;
    var pattSaat = /[0-9]+:[0-9]+/i;
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {

        if (index == 317) {
            console.log("28")
        }
        var tempPDKS = {}
        var ColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnSoyadi = Sheets["C" + index] ? Sheets["C" + index].v : "";
        var ColumnAciklama = pattSaat.test(Sheets["E" + index]) !== undefined ? "" : Sheets["E" + index].w;
        var ColumnGiris = Sheets["E" + index] !== undefined ? Sheets["E" + index].w : "";
        var ColumnCikis = Sheets["G" + index] !== undefined ? Sheets["G" + index].w : "";

        var ColumnGirisTarih = Sheets["D" + index] ? Sheets["D" + index].w : "";
        // var ColumnCikisTarih = Sheets["H" + index] ? Sheets["H" + index].w : "";


        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi + " " + ColumnSoyadi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";

        if (TarihRegex.test(ColumnGirisTarih)) {

            ColumnGirisTarih.convertTrDateToGlobalDate();
            var arr = ColumnGirisTarih.split(/[:./]/);
            arr[2] = "20" + arr[2];
            tempPDKS["Tarih"] = new Date(Date.UTC(arr[2].substr(-4), arr[0] - 1, arr[1]));


        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            a = "0" + temp[0];
            b = "0" + temp[1];
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)

        } else {

            var t = command.Pdks_sadelestir(ColumnAciklama)

            if (t == "izinli") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_5"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    // let d = new Date(excelData.Sheets["RAW"]["E2"].w);//ilk rasgele alacagımız tarih 
    // var AydaHaftadaKacGunVar = d.haftadaKacGunVarHaftasonuHaric();
    // console.log(AydaHaftadaKacGunVar); 
    var Sheets = excelData.Sheets["RAW"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'RAW' olmalı "];
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    // console.log(command.Pdks_sadelestir(Sheets["C2"].v )) 
    for (let index = 2; index < excelboyutu; index++) {
        var tempPDKS = {}
        var AColumnAdiKey = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var BColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var GColumn2 = Sheets["G" + (index - 2)] ? Sheets["G" + (index - 2)].v : "";
        var GColumn1 = Sheets["G" + (index - 1)] ? Sheets["G" + (index - 1)].v : ""
        var GColumn = GColumn1 + " " + GColumn2;
        if (command.Pdks_sadelestir(AColumnAdiKey) == 'adi') {
            tempPDKS["Aciklama"] = GColumn;
            tempPDKS["AdSoyad"] = BColumnAdi;
            index++
            var BColumnSoyadi = Sheets["B" + index].v;
            tempPDKS["AdSoyad"] += " " + BColumnSoyadi;
            tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
            tempPDKS["Giris"] = "";
            tempPDKS["Cikis"] = "";
            index = index + 2
            while (command.Pdks_sadelestir(Sheets["A" + index].v) !== "toplam") {
                var AColumnTarih = Sheets["A" + index] ? Sheets["A" + index].w : "";
                var TarihRegex = /[0-9]+[.][0-9]+[.][0-9]+/i;
                AColumnTarih = AColumnTarih.match(TarihRegex);
                AColumnTarih = AColumnTarih[0];
                var BColumnGiris = Sheets["B" + index] ? Sheets["B" + index].w : "";
                var DColumnCikis = Sheets["D" + index] ? Sheets["D" + index].w : "";
                tempPDKS["Tarih"] = AColumnTarih.convertTrDateToGlobalDate();
                tempPDKS["Giris"] = BColumnGiris;
                tempPDKS["Cikis"] = DColumnCikis;
                if (BColumnGiris == "" || DColumnCikis == "") {
                    tempPDKS["Type"] = null
                } else {
                    tempPDKS["Type"] = 0
                }

                var resmiTatil = AyaAitResmiTatil.filter((item) => {
                    return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
                });
                if (resmiTatil.length > 0) {
                    tempPDKS["Type"] = 2
                }


                if (!AColumnTarih.haftaSonuMu()) {
                    pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
                    personelControl(personelList, tempPDKS, PersonelError)
                }
                index++
            }

        }
    }
    return [pdks, PersonelError]
}
//-------------------------------------------------------------------------------  
pdksMethod["PDKS_Ornek_6"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sayfa1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sayfa1' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {
        var tempPDKS = {}
        var TcNo = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var ColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnSoyadi = Sheets["C" + index] ? Sheets["C" + index].v : "";
        var ColumnAciklama = ""

        var ColumnGirisTarih = Sheets["D" + index] ? Sheets["D" + index].w : "";
        var ColumnGiris = Sheets["E" + index] ? Sheets["E" + index].w : "";



        var ColumnCikisTarih = Sheets["H" + index] ? Sheets["H" + index].w : "";
        var ColumnCikis = Sheets["I" + index] ? Sheets["I" + index].w : "";



        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi + " " + ColumnSoyadi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";
        if (ColumnCikis == "17:34:24") {
            console.log("debug");
            tempPDKS["debug"] = ColumnCikis
        }



        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;
        if (TarihRegex.test(ColumnGirisTarih) && TarihRegex.test(ColumnCikisTarih)) {
            ColumnGirisTarih = new Date(moment(ColumnGirisTarih, "MM.DD.YYYY"));
            ColumnCikisTarih = new Date(moment(ColumnCikisTarih, "MM.DD.YYYY"));
            tempPDKS["Tarih"] = ColumnGirisTarih
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            // var c = ColumnGirisTarih.split("[./]");
            // var d = ColumnCikisTarih.split("[./]");

            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            var gunfark = 0

            var CikisAy = ColumnCikisTarih.getMonth() * 12 + ColumnCikisTarih.getDate(),
                girisAy = ColumnGirisTarih.getMonth() * 12 + ColumnGirisTarih.getDate()
            gunfark = CikisAy - girisAy


            if (gunfark > 0) {
                a = "0" + (parseInt(temp[0]) + (gunfark * 24));
                b = "0" + temp[1];
            } else {
                a = "0" + temp[0];
                b = "0" + temp[1];
            }
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)
        } else {
            var t = command.Pdks_sadelestir(ColumnAciklama)
            if (t == "yillikizi̇n") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_7"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sayfa1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sayfa1' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {
        var tempPDKS = {}
        var TcNo = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var ColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnAciklama = ""
        var ColumnGirisTarih = Sheets["C" + index] ? Sheets["C" + index].w : "";
        var ColumnGiris = Sheets["D" + index] ? Sheets["D" + index].w : "";
        var ColumnCikisTarih = Sheets["E" + index] ? Sheets["E" + index].w : "";
        var ColumnCikis = Sheets["F" + index] ? Sheets["F" + index].w : "";

        if (!ColumnAdi || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnGirisTarih) || !/[0-9]+:[0-9]+/i.test(ColumnGiris) || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnCikisTarih) || !/[0-9]+:[0-9]+/i.test(ColumnCikis)) {
            continue
        }


        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";
        if (ColumnCikis == "17:34:24") {
            console.log("debug");
            tempPDKS["debug"] = ColumnCikis
        }



        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;
        if (TarihRegex.test(ColumnGirisTarih) && TarihRegex.test(ColumnCikisTarih)) {
            ColumnGirisTarih = new Date(moment(ColumnGirisTarih, "YYYY.MM.DD"));
            ColumnCikisTarih = new Date(moment(ColumnCikisTarih, "YYYY.MM.DD"));
            tempPDKS["Tarih"] = ColumnGirisTarih
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            // var c = ColumnGirisTarih.split("[./]");
            // var d = ColumnCikisTarih.split("[./]");

            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            var gunfark = 0

            var CikisAy = ColumnCikisTarih.getMonth() * 12 + ColumnCikisTarih.getDate(),
                girisAy = ColumnGirisTarih.getMonth() * 12 + ColumnGirisTarih.getDate()
            gunfark = CikisAy - girisAy


            if (gunfark > 0) {
                a = "0" + (parseInt(temp[0]) + (gunfark * 24));
                b = "0" + temp[1];
            } else {
                a = "0" + temp[0];
                b = "0" + temp[1];
            }
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)
        } else {
            var t = command.Pdks_sadelestir(ColumnAciklama)
            if (t == "yillikizi̇n") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_8"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sayfa1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sayfa1' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {

        var tempPDKS = {}
        var TcNo = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var ColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnAciklama = ""
        var ColumnTarihSaat = Sheets["D" + index] ? Sheets["D" + index].w : "";
        var ColumnGirisCikis = Sheets["E" + index] ? Sheets["E" + index].w : "";

        if (!ColumnAdi || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnTarihSaat) || command.Pdks_sadelestir(ColumnGirisCikis) == "argecikis") {
            continue
        }

        var ColumnGirisTarih = Sheets["D" + index] ? Sheets["D" + index].w.trim() : "";
        ColumnGirisTarih = ColumnGirisTarih.split(" ")
        var ColumnGiris = ColumnGirisTarih[1]

        var ColumnCikisTarih = Sheets["D" + (index + 1)] ? Sheets["D" + (index + 1)].w.trim() : "";
        ColumnCikisTarih = ColumnCikisTarih.split(" ")
        var ColumnCikis = ColumnCikisTarih[1]


        if (typeof Sheets["D" + index].v === 'number') {
            ColumnGirisTarih = moment(ColumnGirisTarih[0], "M/DD/YY").format("DD.MM.YYYY");
        } else {
            ColumnGirisTarih = moment(ColumnGirisTarih[0], "DD.MM.YYYY").format("DD.MM.YYYY");
        }

        ColumnGirisCikis = Sheets["E" + (index + 1)] ? Sheets["E" + (index + 1)].w : "";
        if (command.Pdks_sadelestir(ColumnGirisCikis) !== "argecikis") {
            continue
        }
        if (typeof Sheets["D" + (index + 1)].v === 'number') {
            ColumnCikisTarih = moment(ColumnCikisTarih[0], "M/DD/YY").format("DD.MM.YYYY");
        } else {
            ColumnCikisTarih = moment(ColumnCikisTarih[0], "DD.MM.YYYY").format("DD.MM.YYYY");
        }

        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";

        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;
        if (TarihRegex.test(ColumnGirisTarih) && TarihRegex.test(ColumnCikisTarih)) {
            ColumnGirisTarih = new Date(moment(ColumnGirisTarih, "DD.MM.YYYY"));
            ColumnCikisTarih = new Date(moment(ColumnCikisTarih, "DD.MM.YYYY"));
            tempPDKS["Tarih"] = ColumnGirisTarih
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            // var c = ColumnGirisTarih.split("[./]");
            // var d = ColumnCikisTarih.split("[./]");

            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            var gunfark = 0

            var CikisAy = ColumnCikisTarih.getMonth() * 12 + ColumnCikisTarih.getDate(),
                girisAy = ColumnGirisTarih.getMonth() * 12 + ColumnGirisTarih.getDate()
            gunfark = CikisAy - girisAy


            if (gunfark > 0) {
                a = "0" + (parseInt(temp[0]) + (gunfark * 24));
                b = "0" + temp[1];
            } else {
                a = "0" + temp[0];
                b = "0" + temp[1];
            }
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)
        } else {
            var t = command.Pdks_sadelestir(ColumnAciklama)
            if (t == "yillikizi̇n") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_9"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sheet1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sheet1' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {
        var tempPDKS = {}
        var TcNo = "";
        var ColumnAdi = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var ColumnAciklama = ""
        var ColumnGirisTarih = Sheets["B" + index] ? Sheets["B" + index].w : "";
        var ColumnGiris = Sheets["C" + index] ? Sheets["C" + index].w : "";
        var ColumnCikisTarih = ColumnGirisTarih
        var ColumnCikis = Sheets["D" + index] ? Sheets["D" + index].w : "";

        // if(!ColumnAdi || !/[0-9]+[./-][0-9]+[./-][0-9]+/i.test(ColumnGirisTarih) || !/[0-9]+:[0-9]+/i.test(ColumnGiris) ||  !/[0-9]+[./-][0-9]+[./-][0-9]+/i.test(ColumnCikisTarih) || !/[0-9]+:[0-9]+/i.test(ColumnCikis)){
        //     continue
        // } 
        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";


        var TarihRegex = /[0-9]+[./-][0-9]+[./-][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;
        if (TarihRegex.test(ColumnGirisTarih) && TarihRegex.test(ColumnCikisTarih)) {
            ColumnGirisTarih = new Date(moment(ColumnGirisTarih, "DD-MM-YYYY"));
            ColumnCikisTarih = new Date(moment(ColumnCikisTarih, "DD-MM-YYYY"));
            tempPDKS["Tarih"] = ColumnGirisTarih
        } else {
            console.log("tarih hatalı")
            continue;
        }


        var findpdksIndex = pdks.findIndex((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(ColumnGirisTarih).format("DD.MM.YYYY") && item.SadelestirilmisAdSoyad == tempPDKS["SadelestirilmisAdSoyad"]
        });

        if (findpdksIndex > -1) {
            if (ColumnGiris !== "") {
                var karsılastirSaatDakika = pdks[findpdksIndex].Giris.split(":")
                karsılastirSaatDakika = (parseInt(karsılastirSaatDakika[0]) * 24) + parseInt(karsılastirSaatDakika[1])
                var karsılastirSaatDakikaT = ColumnGiris.split(":")
                karsılastirSaatDakikaT = (parseInt(karsılastirSaatDakikaT[0]) * 24) + parseInt(karsılastirSaatDakikaT[1])
                if (karsılastirSaatDakikaT < karsılastirSaatDakika) {
                    var temp = ColumnGiris.split(":");
                    var a = "0" + temp[0];
                    var b = "0" + temp[1];
                    pdks[findpdksIndex].Giris = a.substr(-2) + ":" + b.substr(-2);
                }
            }
            if (ColumnCikis !== "") {
                var karsılastirSaatDakika = pdks[findpdksIndex].Cikis.split(":")
                karsılastirSaatDakika = (parseInt(karsılastirSaatDakika[0]) * 24) + parseInt(karsılastirSaatDakika[1])
                var karsılastirSaatDakikaT = ColumnCikis.split(":")
                karsılastirSaatDakikaT = (parseInt(karsılastirSaatDakikaT[0]) * 24) + parseInt(karsılastirSaatDakikaT[1])
                if (karsılastirSaatDakikaT > karsılastirSaatDakika) {
                    var temp = ColumnCikis.split(":");
                    var a = "0" + temp[0];
                    var b = "0" + temp[1];
                    pdks[findpdksIndex].Cikis = a.substr(-2) + ":" + b.substr(-2);
                }
            }
            continue;
        } else {
            if (ColumnGiris == "") continue;
            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnGiris;
            tempPDKS["Type"] = 0

        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_10"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    // let d = new Date(excelData.Sheets["RAW"]["E2"].w);//ilk rasgele alacagımız tarih 
    // var AydaHaftadaKacGunVar = d.haftadaKacGunVarHaftasonuHaric();
    // console.log(AydaHaftadaKacGunVar); 
    var Sheets = excelData.Sheets["Sayfa1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sayfa1' olmalı "];
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    // console.log(command.Pdks_sadelestir(Sheets["C2"].v )) 
    for (let index = 2; index < excelboyutu; index++) {
        var tempPDKS = {}
        var AColumnAdiKey = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var BColumnAdi = Sheets["C" + index] ? Sheets["C" + index].v : "";
        var GColumn = Sheets["AG" + index] ? Sheets["AG" + index].v : "";

        if (command.Pdks_sadelestir(AColumnAdiKey) == 'adi') {

            tempPDKS["AdSoyad"] = BColumnAdi;
            index++
            var BColumnSoyadi = Sheets["C" + index].v;
            tempPDKS["AdSoyad"] += " " + BColumnSoyadi;
            tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
            tempPDKS["Giris"] = "";
            tempPDKS["Cikis"] = "";


            while (command.Pdks_sadelestir(Sheets["A" + index] ? Sheets["A" + index].w : "") !== "netalacak") {
                var GColumn = Sheets["AG" + index] ? Sheets["AG" + index].v : "";
                tempPDKS["Aciklama"] = GColumn;
                var AColumnTarih = Sheets["A" + index] ? Sheets["A" + index].w : "";
                var TarihRegex = /[0-9]+[.][0-9]+[.][0-9]+/i;
                AColumnTarih = AColumnTarih.match(TarihRegex);
                var BColumnGiris = Sheets["D" + index] ? Sheets["D" + index].w : "";
                var DColumnCikis = Sheets["E" + index] ? Sheets["E" + index].w : "";

                if (!AColumnTarih) {
                    index++
                    continue;
                }


                AColumnTarih = AColumnTarih[0];
                tempPDKS["Tarih"] = AColumnTarih.convertTrDateToGlobalDate();
                tempPDKS["Giris"] = BColumnGiris;
                tempPDKS["Cikis"] = DColumnCikis;
                if (BColumnGiris == "" || DColumnCikis == "") {
                    if (command.Pdks_sadelestir(tempPDKS["Aciklama"]) == "yillikizin") {
                        tempPDKS["Type"] = 3
                    } else {
                        tempPDKS["Type"] = null
                    }
                } else {
                    tempPDKS["Type"] = 0
                }



                var resmiTatil = AyaAitResmiTatil.filter((item) => {
                    return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
                });
                if (resmiTatil.length > 0) {
                    tempPDKS["Type"] = 2
                }


                if (!AColumnTarih.haftaSonuMu()) {
                    pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
                    personelControl(personelList, tempPDKS, PersonelError)
                }
                index++
            }

        }
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_11"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sayfa1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sayfa1' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {
        var tempPDKS = {}
        var TcNo = "";
        var ColumnAdi = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var ColumnAciklama = ""
        var ColumnGirisTarih = Sheets["B" + index] ? Sheets["B" + index].w : "";
        var ColumnGiris = Sheets["C" + index] ? Sheets["C" + index].w : "";
        var ColumnCikis = Sheets["D" + index] ? Sheets["D" + index].w : "";

        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;


        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";


        if (!ColumnAdi || !TarihRegex.test(ColumnGirisTarih) || !pattSaat.test(ColumnGiris) || !pattSaat.test(ColumnCikis)) {

            if (command.Pdks_sadelestir(ColumnGiris) == "resmitati̇l") {
                var a = command.Pdks_sadelestir(ColumnGiris)
                ColumnGiris = "00:00"
                ColumnCikis = "00:00"
            } else {
                continue
            }
        }
        ColumnGirisTarih = ColumnGirisTarih.match(TarihRegex)[0];

        ColumnGirisTarih = new Date(moment(ColumnGirisTarih, "DD.MM.YYYY"));
        tempPDKS["Tarih"] = ColumnGirisTarih
        var temp = ColumnGiris.split(":")
        var a = "0" + temp[0];
        var b = "0" + temp[1];
        ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
        temp = []
        temp = ColumnCikis.split(":")
        a = "0" + temp[0];
        b = "0" + temp[1];

        ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

        tempPDKS["Giris"] = ColumnGiris;
        tempPDKS["Cikis"] = ColumnCikis;
        tempPDKS["Type"] = 0

        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_12"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sheet"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sheet' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {
        var tempPDKS = {}
        var TcNo = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var ColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnSoyadi = Sheets["C" + index] ? Sheets["C" + index].v : "";
        var ColumnAciklama = ""
        var ColumnGirisTarih = Sheets["D" + index] ? Sheets["D" + index].w : "";
        var ColumnGiris = Sheets["E" + index] ? Sheets["E" + index].w : "";
        var ColumnCikisTarih = Sheets["F" + index] ? Sheets["F" + index].w : "";
        var ColumnCikis = Sheets["G" + index] ? Sheets["G" + index].w : "";

        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;

        if (!ColumnAdi || !ColumnSoyadi || !TarihRegex.test(ColumnGirisTarih) || !pattSaat.test(ColumnGiris) || !TarihRegex.test(ColumnCikisTarih) || !pattSaat.test(ColumnCikis)) {
            continue
        }

        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi + " " + ColumnSoyadi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";
        ColumnGirisTarih = new Date(moment(ColumnGirisTarih, "DD.MM.YYYY"));
        ColumnCikisTarih = new Date(moment(ColumnCikisTarih, "DD.MM.YYYY"));
        tempPDKS["Tarih"] = ColumnGirisTarih


        var temp = ColumnGiris.split(":")
        var a = "0" + temp[0];
        var b = "0" + temp[1];
        ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
        temp = []
        temp = ColumnCikis.split(":")
        var gunfark = 0

        var CikisAy = ColumnCikisTarih.getMonth() * 12 + ColumnCikisTarih.getDate(),
            girisAy = ColumnGirisTarih.getMonth() * 12 + ColumnGirisTarih.getDate()
        gunfark = CikisAy - girisAy


        if (gunfark > 0) {
            a = "0" + (parseInt(temp[0]) + (gunfark * 24));
            b = "0" + temp[1];
        } else {
            a = "0" + temp[0];
            b = "0" + temp[1];
        }
        ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

        tempPDKS["Giris"] = ColumnGiris;
        tempPDKS["Cikis"] = ColumnCikis;
        tempPDKS["Type"] = 0

        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_13"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sayfa1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sayfa1' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    //ObjectKeyList = ObjectKeyList.sort();

    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));

    var chars = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ'
    ]



    console.log(excelboyutu)


    var tarihIndex = null

    for (let index = 1; index < excelboyutu; index++) {
        var tempPDKS = {}
        var TcNo = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var ColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnAciklama = ""
        var ColumnGirisTarih = "";
        var ColumnGiris = "";
        var ColumnCikisTarih = "";
        var ColumnCikis = "";
        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;

        if (command.Pdks_sadelestir(ColumnAdi) !== "adisoyadi" && tarihIndex == null) {
            continue;
        } else if (command.Pdks_sadelestir(ColumnAdi) == "adisoyadi") {
            tarihIndex = index;
            continue;
        } else if (command.Pdks_sadelestir(ColumnAdi) == "resmitatil") {
            break;
        }

        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";

        for (let sutun = 2; sutun < chars.length; sutun++) {
            const sutunChar = chars[sutun];
            var sutunCharValue = Sheets[sutunChar + tarihIndex] ? Sheets[sutunChar + tarihIndex].w : undefined
            if (sutunCharValue == undefined || !TarihRegex.test(sutunCharValue)) {
                continue;
            } else {
                ColumnGirisTarih = new Date(moment(sutunCharValue, "MM.DD.YYYY"));  //"9/1/19"

                tempPDKS["Tarih"] = ColumnGirisTarih
                var calismaDurumu = Sheets[sutunChar + index] ? Sheets[sutunChar + index].w : ""

                if (command.Pdks_sadelestir(calismaDurumu) == "ht") {
                    continue
                } else if (command.Pdks_sadelestir(calismaDurumu) == "x") {
                    tempPDKS["Giris"] = "09:00";
                    tempPDKS["Cikis"] = "18:00";
                    tempPDKS["Type"] = 0
                } else if (command.Pdks_sadelestir(calismaDurumu) == "dg" || command.Pdks_sadelestir(calismaDurumu) == "g") {
                    tempPDKS["Giris"] = "09:00";
                    tempPDKS["Cikis"] = "18:00";
                    tempPDKS["Type"] = 18

                } else if (command.Pdks_sadelestir(calismaDurumu) == "yi") {
                    tempPDKS["Giris"] = "09:00";
                    tempPDKS["Cikis"] = "18:00";
                    tempPDKS["Type"] = 3

                } else if (command.Pdks_sadelestir(calismaDurumu) == "sp" || command.Pdks_sadelestir(calismaDurumu) == "r") {
                    tempPDKS["Giris"] = "09:00";
                    tempPDKS["Cikis"] = "18:00";
                    tempPDKS["Type"] = 17
                } else {
                    continue;
                }


                var resmiTatil = AyaAitResmiTatil.filter((item) => {
                    return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
                });
                if (resmiTatil.length > 0) {
                    tempPDKS["Type"] = 2
                }
                if (tempPDKS["SadelestirilmisAdSoyad"] == "muratkarasungur") {
                    console.log();
                }

                pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
                personelControl(personelList, tempPDKS, PersonelError)

            }

        }


    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_14"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["RAW"];
    var TarihRegex = /[0-9]+[.\/][0-9]+[.\/][0-9]+/i;
    var pattSaat = /[0-9]+:[0-9]+/i;
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'RAW' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {

        // if (index == 317) {
        //     console.log("28")
        // }
        var tempPDKS = {}
        var ColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnSoyadi = Sheets["C" + index] ? Sheets["C" + index].v : "";
        var ColumnAciklama = pattSaat.test(Sheets["K" + index]) !== undefined ? "" : Sheets["K" + index].w;
        var ColumnGiris = Sheets["H" + index] !== undefined ? Sheets["H" + index].w : "";
        var ColumnCikis = Sheets["I" + index] !== undefined ? Sheets["I" + index].w : "";

        var ColumnGirisTarih = Sheets["G" + index] ? Sheets["G" + index].w : "";
        // var ColumnCikisTarih = Sheets["H" + index] ? Sheets["H" + index].w : "";


        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi + " " + ColumnSoyadi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";
        if (TarihRegex.test(ColumnGirisTarih)) {

            ColumnGirisTarih = ColumnGirisTarih.split(" ")[0];
            var arr = ColumnGirisTarih.split(/[:./]/);
            arr[2] = "20" + arr[2];
            tempPDKS["Tarih"] = new Date(Date.UTC(arr[2].substr(-4), arr[1] - 1, arr[0]));


        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            a = "0" + temp[0];
            b = "0" + temp[1];
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS);
            continue;

        } else {

            var t = command.Pdks_sadelestir(ColumnAciklama)

            if (t == "izinli") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
                continue;
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_15"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sheet"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sheet' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {
        var tempPDKS = {}
        var ColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnSoyadi = Sheets["C" + index] ? Sheets["C" + index].v : "";
        var ColumnAciklama = ""
        var ColumnGiris = Sheets["E" + index] ? Sheets["E" + index].w : "";
        var ColumnCikis = Sheets["G" + index] ? Sheets["G" + index].w : "";

        var ColumnGirisTarih = Sheets["D" + index] ? Sheets["D" + index].w : "";
        var ColumnCikisTarih = Sheets["F" + index] ? Sheets["F" + index].w : "";


        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi + " " + ColumnSoyadi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"]);
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";
        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;
        if (TarihRegex.test(ColumnGirisTarih)) {

            tempPDKS["Tarih"] = ColumnGirisTarih.convertTrDateToGlobalDate();
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            var c = ColumnGirisTarih.split("[./]");
            var d = ColumnCikisTarih.split("[./]");

            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")

            var gunfark = 0
            var CikisAy = parseInt(d[1]) * 12 + parseInt(d[0]),
                girisAy = parseInt(c[1]) * 12 + parseInt(c[0])
            gunfark = CikisAy - girisAy


            if (gunfark > 0) {
                a = "0" + (parseInt(temp[0]) + (gunfark * 24));
                b = "0" + temp[1];
            } else {
                a = "0" + temp[0];
                b = "0" + temp[1];
            }
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)
        } else {
            var t = command.Pdks_sadelestir(ColumnAciklama)
            if (t == "yillikizi̇n") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_16"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    // let d = new Date(excelData.Sheets["RAW"]["E2"].w);//ilk rasgele alacagımız tarih 
    // var AydaHaftadaKacGunVar = d.haftadaKacGunVarHaftasonuHaric();
    // console.log(AydaHaftadaKacGunVar); 
    var Sheets = excelData.Sheets["Sheet"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sheet' olmalı "];
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    // console.log(command.Pdks_sadelestir(Sheets["C2"].v )) 
    for (let index = 1; index < excelboyutu; index++) {
        var tempPDKS = {}
        var AColumnAdiKey = Sheets["A" + index] ? Sheets["A" + index].w : "";
        var BColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var Aciklama = Sheets["O" + index] ? Sheets["O" + index].v : "";
        if (command.Pdks_sadelestir(AColumnAdiKey).search("personeladi") >= 0) {
            tempPDKS["Aciklama"] = Aciklama;
            tempPDKS["AdSoyad"] = AColumnAdiKey.trim().split(":")[1].trim().split("-")[0].trim();
            tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
            tempPDKS["Giris"] = "";
            tempPDKS["Cikis"] = "";
            //index++
            //var TarihRegex = /[0-9]+[.][0-9]+[.][0-9]+/i;
            AColumnAdiKey = Sheets["A" + index + 1] ? Sheets["A" + index + 1].w : "";
            while (command.Pdks_sadelestir(Sheets["A" + index + 1] ? Sheets["A" + index + 1].w : undefined).search("personeladi") == -1) {
                if (index > excelboyutu) { break }
                AColumnAdiKey = Sheets["A" + index + 1] ? Sheets["A" + index + 1].w : "";

                var temp = AColumnAdiKey
                var sp = temp.trim().split(" ")
                if (sp[0] && sp[1] && sp[2]) {
                    AColumnAdiKey = new Date(`${sp[0].trim()} ${sp[1].trim()} ${sp[2].trim()}`);
                } else {
                    console.log(index + 1, "tarih hatalı")
                    index++
                    continue;
                }
                tempPDKS["Tarih"] = AColumnAdiKey
                tempPDKS["Giris"] = "";
                tempPDKS["Cikis"] = "";

                var ColumnGiris = Sheets["D" + index + 1] ? Sheets["D" + index + 1].w : "";
                var ColumnCikis = Sheets["E" + index + 1] ? Sheets["E" + index + 1].w : "";
                if (ColumnGiris) {
                    var ColumnGirisTarih = ColumnGiris.trim().split(" ")[0].trim()
                    ColumnGiris = ColumnGiris.trim().split(" ")[1].trim()
                }
                if (ColumnCikis) {
                    var ColumnCikisTarih = ColumnCikis.trim().split(" ")[0].trim()
                    ColumnCikis = ColumnCikis.trim().split(" ")[1].trim()
                }
                //var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
                var pattSaat = /[0-9]+:[0-9]+/i;

                if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
                    var c = ColumnGirisTarih.split("[./]");
                    var d = ColumnCikisTarih.split("[./]");

                    var temp = ColumnGiris.split(":")
                    var a = "0" + temp[0];
                    var b = "0" + temp[1];
                    ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
                    temp = []
                    temp = ColumnCikis.split(":")

                    var gunfark = 0
                    var CikisAy = parseInt(d[1]) * 12 + parseInt(d[0]),
                        girisAy = parseInt(c[1]) * 12 + parseInt(c[0])
                    gunfark = CikisAy - girisAy


                    if (gunfark > 0) {
                        a = "0" + (parseInt(temp[0]) + (gunfark * 24));
                        b = "0" + temp[1];
                    } else {
                        a = "0" + temp[0];
                        b = "0" + temp[1];
                    }
                    ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

                    tempPDKS["Giris"] = ColumnGiris;
                    tempPDKS["Cikis"] = ColumnCikis;
                    tempPDKS["Type"] = 0

                } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
                    console.log(index + 1, "hatalı giriş var ")
                    console.log(tempPDKS)
                } else {
                    var t = command.Pdks_sadelestir(Aciklama)
                    if (t == "yillikizi̇n") {
                        tempPDKS["Type"] = 3
                    } else if (t == "resmitati̇l") {
                        tempPDKS["Type"] = 2
                    } else if (t == "devamsiz") {
                        tempPDKS["Type"] = null;
                        continue
                    } else if (t == "raporlu") {
                        tempPDKS["Type"] = 17
                    } else if (t == "disgorev") {
                        tempPDKS["Type"] = 18
                    } else if (t !== "") {
                        tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
                    } else {
                        tempPDKS["Type"] = null
                    }
                }
                var resmiTatil = AyaAitResmiTatil.filter((item) => {
                    return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
                });
                if (resmiTatil.length > 0) {
                    tempPDKS["Type"] = 2
                }
                pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
                personelControl(personelList, tempPDKS, PersonelError)
                index++

            }

        }

    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_17"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sayfa1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sayfa1' olmalı "];
    var TarihRegex = /[0-9]+[.\/][0-9]+[.\/][0-9]+/i;
    var pattSaat = /[0-9]+:[0-9]+/i;
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {

        if (index == 317) {
            console.log("28")
        }
        var tempPDKS = {}
        var ColumnAdi = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var ColumnSoyadi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnGirisTarih = Sheets["D" + index] ? Sheets["D" + index].w : "";
        var ColumnGiris = Sheets["E" + index] !== undefined ? Sheets["E" + index].w : "";
        var ColumnCikis = Sheets["F" + index] !== undefined ? Sheets["F" + index].w : "";
        var ColumnAciklama = Sheets["G" + index] ? Sheets["G" + index].v : "";

        // var ColumnCikisTarih = Sheets["H" + index] ? Sheets["H" + index].w : "";


        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi + " " + ColumnSoyadi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";

        if (TarihRegex.test(ColumnGirisTarih)) {

            ColumnGirisTarih.convertTrDateToGlobalDate();
            var arr = ColumnGirisTarih.split(/[:./]/);
            arr[2] = "20" + arr[2];
            tempPDKS["Tarih"] = new Date(Date.UTC(arr[2].substr(-4), arr[0] - 1, arr[1]));


        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            a = "0" + temp[0];
            b = "0" + temp[1];
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)

        } else {

            var t = command.Pdks_sadelestir(ColumnAciklama)

            if (t == "izinli") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "haftatatili") {
                tempPDKS["Type"] = null
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_19"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sheet1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sheet1' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {
        var tempPDKS = {}
        var ColumnAdi = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var ColumnSoyadi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnAciklama = Sheets["D" + index] ? Sheets["D" + index].w : "";
        var ColumnIlkGirisTarihSaat = Sheets["D" + index] ? Sheets["D" + index].w : "";
        var ColumnSonCıkısTarihSaat = Sheets["E" + index] ? Sheets["E" + index].w : "";

        var ColumnGirisTarih = ""
        var ColumnGiris = ""
        var ColumnCikisTarih = ""
        var ColumnCikis = ""
        if (ColumnIlkGirisTarihSaat) {
            ColumnGirisTarih = ColumnIlkGirisTarihSaat.trim().split(" ")[0].trim()
            ColumnGiris = ColumnIlkGirisTarihSaat.trim().split(" ")[1].trim()
        }
        if (ColumnSonCıkısTarihSaat) {
            ColumnCikisTarih = ColumnSonCıkısTarihSaat.trim().split(" ")[0].trim()
            ColumnCikis = ColumnSonCıkısTarihSaat.trim().split(" ")[1].trim()
        }



        if (!ColumnAdi || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnGirisTarih) || !/[0-9]+:[0-9]+/i.test(ColumnGiris) || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnCikisTarih) || !/[0-9]+:[0-9]+/i.test(ColumnCikis)) {

            continue
        }


        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi + " " + ColumnSoyadi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";
        if (ColumnCikis == "17:34:24") {
            console.log("debug");
            tempPDKS["debug"] = ColumnCikis
        }



        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;
        if (TarihRegex.test(ColumnGirisTarih) && TarihRegex.test(ColumnCikisTarih)) {
            ColumnGirisTarih = new Date(moment(ColumnGirisTarih, "MM/DD/YYYY"));
            ColumnCikisTarih = new Date(moment(ColumnCikisTarih, "MM/DD/YYYY"));
            tempPDKS["Tarih"] = ColumnGirisTarih
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            // var c = ColumnGirisTarih.split("[./]");
            // var d = ColumnCikisTarih.split("[./]");

            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            var gunfark = 0

            var CikisAy = ColumnCikisTarih.getMonth() * 12 + ColumnCikisTarih.getDate(),
                girisAy = ColumnGirisTarih.getMonth() * 12 + ColumnGirisTarih.getDate()
            gunfark = CikisAy - girisAy


            if (gunfark > 0) {
                a = "0" + (parseInt(temp[0]) + (gunfark * 24));
                b = "0" + temp[1];
            } else {
                a = "0" + temp[0];
                b = "0" + temp[1];
            }
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)
        } else {
            var t = command.Pdks_sadelestir(ColumnAciklama)
            if (t == "yillikizi̇n") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else {
                //  tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_20"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["SAYFA1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'SAYFA1' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 3; index < excelboyutu; index++) {
        var tempPDKS = {}
        var ColumnAdi = Sheets["D" + index] ? Sheets["D" + index].v : "";
        var ColumnAciklama = Sheets["G" + index] ? Sheets["G" + index].w : "";
        var ColumnIlkGirisTarihSaat = Sheets["E" + index] ? Sheets["E" + index].w : "";
        var ColumnSonCıkısTarihSaat = Sheets["F" + index] ? Sheets["F" + index].w : "";

        var ColumnGirisTarih = ""
        var ColumnGiris = ""
        var ColumnCikisTarih = ""
        var ColumnCikis = ""
        if (ColumnIlkGirisTarihSaat) {
            ColumnGirisTarih = ColumnIlkGirisTarihSaat.trim().split(" ")[0].trim()
            ColumnGiris = ColumnIlkGirisTarihSaat.trim().split(" ")[1].trim()
        }
        if (ColumnSonCıkısTarihSaat) {
            ColumnCikisTarih = ColumnSonCıkısTarihSaat.trim().split(" ")[0].trim()
            ColumnCikis = ColumnSonCıkısTarihSaat.trim().split(" ")[1].trim()
        }



        if (!ColumnAdi || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnGirisTarih) || !/[0-9]+:[0-9]+/i.test(ColumnGiris) || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnCikisTarih) || !/[0-9]+:[0-9]+/i.test(ColumnCikis)) {

            continue
        }


        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";
        if (ColumnAciklama != "") {
            console.log("debug");
            tempPDKS["debug"] = ColumnCikis
        }



        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;
        if (TarihRegex.test(ColumnGirisTarih) && TarihRegex.test(ColumnCikisTarih)) {
            ColumnGirisTarih = new Date(moment(ColumnGirisTarih, "DD.MM.YYYY"));
            ColumnCikisTarih = new Date(moment(ColumnCikisTarih, "DD.MM.YYYY"));
            tempPDKS["Tarih"] = ColumnGirisTarih
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            // var c = ColumnGirisTarih.split("[./]");
            // var d = ColumnCikisTarih.split("[./]");

            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            var gunfark = 0

            var CikisAy = ColumnCikisTarih.getMonth() * 12 + ColumnCikisTarih.getDate(),
                girisAy = ColumnGirisTarih.getMonth() * 12 + ColumnGirisTarih.getDate()
            gunfark = CikisAy - girisAy


            if (gunfark > 0) {
                a = "0" + (parseInt(temp[0]) + (gunfark * 24));
                b = "0" + temp[1];
            } else {
                a = "0" + temp[0];
                b = "0" + temp[1];
            }
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)
        }


        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_28"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["SAYFA1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'SAYFA1' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {
        var tempPDKS = {}
        var ColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnAciklama = "";
        var ColumnIlkGirisTarihSaat = Sheets["C" + index] ? Sheets["C" + index].w : "";
        var ColumnSonCıkısTarihSaat = Sheets["D" + index] ? Sheets["D" + index].w : "";

        var ColumnGirisTarih = ""
        var ColumnGiris = ""
        var ColumnCikisTarih = ""
        var ColumnCikis = ""
        if (ColumnIlkGirisTarihSaat) {
            ColumnGirisTarih = ColumnIlkGirisTarihSaat.trim().split(" ")[0].trim()
            ColumnGiris = ColumnIlkGirisTarihSaat.trim().split(" ")[1].trim()
        }
        if (ColumnSonCıkısTarihSaat) {
            ColumnCikisTarih = ColumnSonCıkısTarihSaat.trim().split(" ")[0].trim()
            ColumnCikis = ColumnSonCıkısTarihSaat.trim().split(" ")[1].trim()
        }



        if (!ColumnAdi || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnGirisTarih) || !/[0-9]+:[0-9]+/i.test(ColumnGiris) || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnCikisTarih) || !/[0-9]+:[0-9]+/i.test(ColumnCikis)) {

            continue
        }


        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";
        if (ColumnAciklama != "") {
            console.log("debug");
            tempPDKS["debug"] = ColumnCikis
        }



        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;
        if (TarihRegex.test(ColumnGirisTarih) && TarihRegex.test(ColumnCikisTarih)) {
            ColumnGirisTarih = new Date(moment(ColumnGirisTarih, "MM/DD/YYYY"));
            ColumnCikisTarih = new Date(moment(ColumnCikisTarih, "MM/DD/YYYY"));
            tempPDKS["Tarih"] = ColumnGirisTarih
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            // var c = ColumnGirisTarih.split("[./]");
            // var d = ColumnCikisTarih.split("[./]");

            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            var gunfark = 0

            var CikisAy = ColumnCikisTarih.getMonth() * 12 + ColumnCikisTarih.getDate(),
                girisAy = ColumnGirisTarih.getMonth() * 12 + ColumnGirisTarih.getDate()
            gunfark = CikisAy - girisAy


            if (gunfark > 0) {
                a = "0" + (parseInt(temp[0]) + (gunfark * 24));
                b = "0" + temp[1];
            } else {
                a = "0" + temp[0];
                b = "0" + temp[1];
            }
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)
        }


        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_30"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sheet1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sheet1' olmalı "];
    var TarihRegex = /[0-9]+[.\/][0-9]+[.\/][0-9]+/i;
    var pattSaat = /[0-9]+:[0-9]+/i;
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index <= excelboyutu; index++) {

        if (index == 317) {
            console.log("28")
        }
        var tempPDKS = {}
        var ColumnAdi = Sheets["D" + index] ? Sheets["D" + index].v : "";
        var ColumnAciklama = pattSaat.test(Sheets["L" + index]) !== undefined ? "" : Sheets["L" + index].w;
        var ColumnGiris = Sheets["E" + index] !== undefined ? Sheets["E" + index].w : "";
        var ColumnCikis = Sheets["F" + index] !== undefined ? Sheets["F" + index].w : "";

        var ColumnGirisTarih = Sheets["A" + index] ? Sheets["A" + index].w : "";
        // var ColumnCikisTarih = Sheets["H" + index] ? Sheets["H" + index].w : "";
        ColumnGirisTarih = ColumnGirisTarih.split(" ")[0]

        ColumnAdi = ColumnAdi.split(" ");
        var tempColumnAdi = ""
        for (let index = 1; index < ColumnAdi.length; index++) {
            tempColumnAdi += ColumnAdi[index] + " ";
        }
        ColumnAdi = tempColumnAdi.trim()
        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";

        if (TarihRegex.test(ColumnGirisTarih)) {
            tempPDKS["Tarih"] = new Date(moment(ColumnGirisTarih, "DD/MM/YYYY"));
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            a = "0" + temp[0];
            b = "0" + temp[1];
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)

        } else {

            var t = command.Pdks_sadelestir(ColumnAciklama)

            if (t == "izinli") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_32"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sayfa1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sayfa1' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {
        var tempPDKS = {}
        var TcNo = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var ColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnAciklama = ""
        var ColumnGirisTarih = Sheets["C" + index] ? Sheets["C" + index].w : "";
        var ColumnGiris = Sheets["D" + index] ? Sheets["D" + index].w : "";
        var ColumnCikisTarih = Sheets["E" + index] ? Sheets["E" + index].w : "";
        var ColumnCikis = Sheets["F" + index] ? Sheets["F" + index].w : "";

        if (!ColumnAdi || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnGirisTarih) || !/[0-9]+:[0-9]+/i.test(ColumnGiris) || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnCikisTarih) || !/[0-9]+:[0-9]+/i.test(ColumnCikis)) {
            continue
        }


        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";
        if (ColumnCikis == "17:34:24") {
            console.log("debug");
            tempPDKS["debug"] = ColumnCikis
        }



        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;
        if (TarihRegex.test(ColumnGirisTarih) && TarihRegex.test(ColumnCikisTarih)) {
            ColumnGirisTarih = new Date(moment(ColumnGirisTarih, "DD.MM.YYYY"));
            ColumnCikisTarih = new Date(moment(ColumnCikisTarih, "YYYY.MM.DD"));
            tempPDKS["Tarih"] = ColumnGirisTarih
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            // var c = ColumnGirisTarih.split("[./]");
            // var d = ColumnCikisTarih.split("[./]");

            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            var gunfark = 0

            var CikisAy = ColumnCikisTarih.getMonth() * 12 + ColumnCikisTarih.getDate(),
                girisAy = ColumnGirisTarih.getMonth() * 12 + ColumnGirisTarih.getDate()
            gunfark = CikisAy - girisAy


            if (gunfark > 0) {
                a = "0" + (parseInt(temp[0]) + (gunfark * 24));
                b = "0" + temp[1];
            } else {
                a = "0" + temp[0];
                b = "0" + temp[1];
            }
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)
        } else {
            var t = command.Pdks_sadelestir(ColumnAciklama)
            if (t == "yillikizi̇n") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_44"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sayfa1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sayfa1' olmalı "];
    var TarihRegex = /[0-9]+[.\/][0-9]+[.\/][0-9]+/i;
    var pattSaat = /[0-9]+:[0-9]+/i;
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {

        if (index == 317) {
            console.log("28")
        }
        var tempPDKS = {}
        var ColumnAdi = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var ColumnSoyadi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnAciklama = pattSaat.test(Sheets["E" + index]) !== undefined ? "" : Sheets["E" + index].w;
        var ColumnGiris = Sheets["F" + index] !== undefined ? Sheets["F" + index].w : "";
        var ColumnCikis = Sheets["G" + index] !== undefined ? Sheets["G" + index].w : "";

        var ColumnGirisTarih = Sheets["D" + index] ? Sheets["D" + index].w : "";
        // var ColumnCikisTarih = Sheets["H" + index] ? Sheets["H" + index].w : "";


        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi + " " + ColumnSoyadi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";

        if (TarihRegex.test(ColumnGirisTarih)) {

            tempPDKS["Tarih"] = new Date(moment(ColumnGirisTarih, "MM.DD.YYYY"));


        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            a = "0" + temp[0];
            b = "0" + temp[1];
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)

        } else {

            var t = command.Pdks_sadelestir(ColumnAciklama)

            if (t == "izinli") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_49"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sayfa1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sayfa1' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {
        var tempPDKS = {}
        var TcNo = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var ColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnSoyAdi = Sheets["C" + index] ? Sheets["C" + index].v : "";
        var ColumnAciklama = ""
        var ColumnGirisTarih = Sheets["F" + index] ? Sheets["F" + index].w : "";
        var ColumnGiris = Sheets["G" + index] ? Sheets["G" + index].w : "";
        var ColumnCikisTarih = Sheets["I" + index] ? Sheets["I" + index].w : "";
        var ColumnCikis = Sheets["J" + index] ? Sheets["J" + index].w : "";

        if (!ColumnAdi || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnGirisTarih) || !/[0-9]+:[0-9]+/i.test(ColumnGiris) || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnCikisTarih) || !/[0-9]+:[0-9]+/i.test(ColumnCikis)) {
            continue
        }


        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi + " " + ColumnSoyAdi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";
        if (ColumnCikis == "17:34:24") {
            console.log("debug");
            tempPDKS["debug"] = ColumnCikis
        }



        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;
        if (TarihRegex.test(ColumnGirisTarih) && TarihRegex.test(ColumnCikisTarih)) {
            ColumnGirisTarih = new Date(moment(ColumnGirisTarih, "MM/DD/YYYY"));
            ColumnCikisTarih = new Date(moment(ColumnCikisTarih, "MM/DD/YYYY"));
            tempPDKS["Tarih"] = ColumnGirisTarih
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            // var c = ColumnGirisTarih.split("[./]");
            // var d = ColumnCikisTarih.split("[./]");

            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            var gunfark = 0

            var CikisAy = ColumnCikisTarih.getMonth() * 12 + ColumnCikisTarih.getDate(),
                girisAy = ColumnGirisTarih.getMonth() * 12 + ColumnGirisTarih.getDate()
            gunfark = CikisAy - girisAy


            if (gunfark > 0) {
                a = "0" + (parseInt(temp[0]) + (gunfark * 24));
                b = "0" + temp[1];
            } else {
                a = "0" + temp[0];
                b = "0" + temp[1];
            }
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)
        } else {
            var t = command.Pdks_sadelestir(ColumnAciklama)
            if (t == "yillikizi̇n") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_51"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sheet1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sheet1' olmalı "];
    var TarihRegex = /[0-9]+[.\/][0-9]+[.\/][0-9]+/i;
    var pattSaat = /[0-9]+:[0-9]+/i;
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {

        if (index == 317) {
            console.log("28")
        }
        var tempPDKS = {}
        var ColumnAdi = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var ColumnAciklama = pattSaat.test(Sheets["E" + index]) !== undefined ? "" : Sheets["E" + index].w;
        var ColumnGiris = Sheets["C" + index] !== undefined ? Sheets["C" + index].w : "";
        var ColumnCikis = Sheets["D" + index] !== undefined ? Sheets["D" + index].w : "";

        var ColumnGirisTarih = Sheets["B" + index] ? Sheets["B" + index].w : "";
        // var ColumnCikisTarih = Sheets["H" + index] ? Sheets["H" + index].w : "";


        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";

        if (TarihRegex.test(ColumnGirisTarih)) {
            ColumnGirisTarih = new Date(moment(ColumnGirisTarih, "DD/MM/YYYY"));
            tempPDKS["Tarih"] = ColumnGirisTarih
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            a = "0" + temp[0];
            b = "0" + temp[1];
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)

        } else {

            var t = command.Pdks_sadelestir(ColumnAciklama)

            if (t == "izinli") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_55"] = (excelData, personelList) => {
    var Sheets = excelData.Sheets["Sheet1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sheet1' olmalı "];
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    console.log(ObjectKeyList)

    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));

    console.log(excelboyutu)
    for (let index = 2; index <= excelboyutu; index++) {
        var tempPDKS = {}
        var ColumnAdi = Sheets["C" + index] ? Sheets["C" + index].v : "";
        var ColumnAciklama = Sheets["E" + index] ? Sheets["E" + index].v : "";
        var ColumnGiris = Sheets["E" + index] ? Sheets["E" + index].w : "";
        var ColumnCikis = Sheets["F" + index] ? Sheets["F" + index].w : "";
        var ColumnTarih = Sheets["D" + index] ? Sheets["D" + index].w : "";
        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";
        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;
        if (TarihRegex.test(ColumnTarih)) {
            ColumnTarih = new Date(moment(ColumnTarih, "DD/MM/YYYY"));
            tempPDKS["Tarih"] = ColumnTarih
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            a = "0" + temp[0];
            b = "0" + temp[1];
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);
            if ((ColumnCikis.saatiDakikayaCevir() - ColumnGiris.saatiDakikayaCevir()) >= 0) {
                tempPDKS["Giris"] = ColumnGiris;
                tempPDKS["Cikis"] = ColumnCikis;
            } else {
                var geceVardiyasi = '24:00'.saatiDakikayaCevir() - ColumnGiris.saatiDakikayaCevir() + ColumnCikis.saatiDakikayaCevir()

                tempPDKS["Giris"] = "00:00";
                tempPDKS["Cikis"] = geceVardiyasi.dakikayiSaateCevir();
            }

            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)
        } else {
            var t = command.Pdks_sadelestir(ColumnAciklama)
            if (t == "yillikizi̇n") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));

        personelControl(personelList, tempPDKS, PersonelError)
        console.log(PersonelError);
    }


    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_57"] = (excelData, personelList) => {
    var Sheets = excelData.Sheets["Sayfa1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sayfa1' olmalı "];
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    console.log(ObjectKeyList)

    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));

    console.log(excelboyutu)
    for (let index = 2; index <= excelboyutu; index++) {
        var tempPDKS = {}
        var ColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnSoyAdi = Sheets["C" + index] ? Sheets["C" + index].v : "";
        var ColumnAciklama = Sheets["E" + index] ? Sheets["E" + index].v : "";
        var ColumnGiris = Sheets["E" + index] ? Sheets["E" + index].w : "";
        var ColumnCikis = Sheets["G" + index] ? Sheets["G" + index].w : "";
        var ColumnTarih = Sheets["D" + index] ? Sheets["D" + index].w : "";
        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi + " " + ColumnSoyAdi;
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";
        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;
        if (TarihRegex.test(ColumnTarih)) {
            ColumnTarih = new Date(moment(ColumnTarih, "MM/DD/YYYY"));
            tempPDKS["Tarih"] = ColumnTarih
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            a = "0" + temp[0];
            b = "0" + temp[1];
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);
            if ((ColumnCikis.saatiDakikayaCevir() - ColumnGiris.saatiDakikayaCevir()) >= 0) {
                tempPDKS["Giris"] = ColumnGiris;
                tempPDKS["Cikis"] = ColumnCikis;
            } else {
                var geceVardiyasi = '24:00'.saatiDakikayaCevir() - ColumnGiris.saatiDakikayaCevir() + ColumnCikis.saatiDakikayaCevir()

                tempPDKS["Giris"] = "00:00";
                tempPDKS["Cikis"] = geceVardiyasi.dakikayiSaateCevir();
            }

            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)
        } else {
            var t = command.Pdks_sadelestir(ColumnAciklama)
            if (t == "yillikizi̇n") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));

        personelControl(personelList, tempPDKS, PersonelError)
        console.log(PersonelError);
    }


    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
pdksMethod["PDKS_Ornek_59"] = (excelData, personelList) => {
    var PersonelError = {
        pdksVarDbYok: [],
        dbVarPdksYok: [],
        eslesmis: []
    }
    var Sheets = excelData.Sheets["Sayfa1"];
    if (Sheets == undefined) return ["Sheets adını kontrol ediniz. 'Sayfa1' olmalı "];
    pdks = []
    let AyaAitResmiTatil = PdksService.resmiTatiller();
    var ObjectKeyList = Object.keys(Sheets)
    ObjectKeyList = ObjectKeyList.filter((item) => {
        return /^[a-zA-Z]+[0-9]+$/gi.test(item);
    })
    var excelboyutu = parseInt(ObjectKeyList[ObjectKeyList.length - 1].replace(/([a-zA-Z]*)/g, ""));
    console.log(excelboyutu)
    for (let index = 2; index < excelboyutu; index++) {
        var tempPDKS = {}
        var TcNo = Sheets["A" + index] ? Sheets["A" + index].v : "";
        var ColumnAdi = Sheets["B" + index] ? Sheets["B" + index].v : "";
        var ColumnAciklama = Sheets["D" + index] ? Sheets["D" + index].v : "";
        var ColumnGirisTarih = Sheets["C" + index] ? Sheets["C" + index].w : "";
        var ColumnGiris = Sheets["D" + index] ? Sheets["D" + index].w : "";
        var ColumnCikisTarih = Sheets["E" + index] ? Sheets["E" + index].w : "";
        var ColumnCikis = Sheets["F" + index] ? Sheets["F" + index].w : "";

        // if (!ColumnAdi || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnGirisTarih) || !/[0-9]+:[0-9]+/i.test(ColumnGiris) || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnCikisTarih) || !/[0-9]+:[0-9]+/i.test(ColumnCikis)) {
        //     continue
        // }
        if (!ColumnAdi || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnGirisTarih) || !/[0-9]+[./][0-9]+[./][0-9]+/i.test(ColumnCikisTarih)) {
            continue
        }


        tempPDKS["Aciklama"] = ColumnAciklama;
        tempPDKS["AdSoyad"] = ColumnAdi
        tempPDKS["SadelestirilmisAdSoyad"] = command.Pdks_sadelestir(tempPDKS["AdSoyad"])
        tempPDKS["Giris"] = "";
        tempPDKS["Cikis"] = "";
        if (146 == index) {
            console.log("debug");
            tempPDKS["debug"] = ColumnCikis
        }



        var TarihRegex = /[0-9]+[./][0-9]+[./][0-9]+/i;
        var pattSaat = /[0-9]+:[0-9]+/i;
        if (TarihRegex.test(ColumnGirisTarih) && TarihRegex.test(ColumnCikisTarih)) {
            ColumnGirisTarih = new Date(moment(ColumnGirisTarih, "DD/MM/YYYY"));
            ColumnCikisTarih = new Date(moment(ColumnCikisTarih, "DD/MM/YYYY"));
            tempPDKS["Tarih"] = ColumnGirisTarih
        } else {
            console.log("tarih hatalı")
            continue;
        }
        if (pattSaat.test(ColumnGiris) && pattSaat.test(ColumnCikis)) {
            // var c = ColumnGirisTarih.split("[./]");
            // var d = ColumnCikisTarih.split("[./]");

            var temp = ColumnGiris.split(":")
            var a = "0" + temp[0];
            var b = "0" + temp[1];
            ColumnGiris = a.substr(-2) + ":" + b.substr(-2);
            temp = []
            temp = ColumnCikis.split(":")
            var gunfark = 0

            var CikisAy = ColumnCikisTarih.getMonth() * 12 + ColumnCikisTarih.getDate(),
                girisAy = ColumnGirisTarih.getMonth() * 12 + ColumnGirisTarih.getDate()
            gunfark = CikisAy - girisAy


            if (gunfark > 0) {
                a = "0" + (parseInt(temp[0]) + (gunfark * 24));
                b = "0" + temp[1];
            } else {
                a = "0" + temp[0];
                b = "0" + temp[1];
            }
            ColumnCikis = a.substr(-2) + ":" + b.substr(-2);

            tempPDKS["Giris"] = ColumnGiris;
            tempPDKS["Cikis"] = ColumnCikis;
            tempPDKS["Type"] = 0

        } else if (pattSaat.test(ColumnGiris) || pattSaat.test(ColumnCikis)) {
            console.log("hatalı giriş var ")
            console.log(tempPDKS)
        } else {
            var t = command.Pdks_sadelestir(ColumnAciklama)
            if (t == "yillikizi̇n") {
                tempPDKS["Type"] = 3
            } else if (t == "resmitati̇l") {
                tempPDKS["Type"] = 2
            } else if (t == "devamsiz") {
                tempPDKS["Type"] = null;
                continue
            } else if (t == "raporlu") {
                tempPDKS["Type"] = 17
            } else if (t == "disgorev") {
                tempPDKS["Type"] = 18
            } else if (t !== "") {
                tempPDKS["Type"] = 18 //giriş çıkıs hatalı ama acıklaması olan tum dataları dış görev var saydım
            } else {
                tempPDKS["Type"] = null
            }
        }
        var resmiTatil = AyaAitResmiTatil.filter((item) => {
            return moment(item.Tarih).format("DD.MM.YYYY") === moment(tempPDKS["Tarih"]).format("DD.MM.YYYY")
        });
        if (resmiTatil.length > 0) {
            tempPDKS["Type"] = 2
        }
        pdks.push(JSON.parse(JSON.stringify(tempPDKS)));
        personelControl(personelList, tempPDKS, PersonelError)
    }
    return [pdks, PersonelError]
}
//------------------------------------------------------------------------------- 
function hesapla(req, res, next) {
    console.log("hesapla");
    var Pdks = req.body;
    let d = new Date(Pdks[0].Tarih);//ilk rasgele alacagımız tarih 
    var AydaHaftadaKacGunVar = d.haftadaKacGunVarHaftasonuHaric();
    var AydaMaximumCumartesiSayisi = d.aydaKacCumartesiVar();
    console.log("AydaHaftadaKacGunVar", AydaHaftadaKacGunVar);
    personeller = []
    for (let index = 0; index < Pdks.length; index++) {
        var PersonelIndex = Pdks[index];
        var IndexPersonel = personeller.findIndex((item) => {
            return item.SadelestirilmisAdSoyad === PersonelIndex.SadelestirilmisAdSoyad;
        });
        if (IndexPersonel > -1) {
            new_PDKSPERSONEL = personeller[IndexPersonel];
        } else {
            var new_PDKSPERSONEL = new PDKSPERSONEL();
            new_PDKSPERSONEL.AdSoyad = PersonelIndex.AdSoyad;
            new_PDKSPERSONEL.SadelestirilmisAdSoyad = PersonelIndex.SadelestirilmisAdSoyad;
        }
        var tempSure = "00:00"
        if (PersonelIndex.Type == 0) { //id: 0, name: "NORMAL ÇALIŞMA  "
            tempSure = command.gunSaatCıkart(PersonelIndex.Giris, PersonelIndex.Cikis)
            if (command.gunSaatKarsılastır(tempSure, "10:30") == 1) {/// günlük çalışması 10:30 saatten buyyuk ise 10:30 saat yapma
                tempSure = "10:30";
            }
        } else if (PersonelIndex.Type == 1) {//     { id: 1, name: "HAFTA TATİLİ   " },
            tempSure = "00:00";
        } else if (PersonelIndex.Type == 2) {//     { id: 2, name: "GENEL TATİLLER   " },
            new_PDKSPERSONEL.ResmiTatil++
            tempSure = "09:00";
        } else if (PersonelIndex.Type == 3) {//     { id: 3, name: "YILLIK ÜCRETLİ İZİNLER  " },
            new_PDKSPERSONEL.YillikIzin++
            tempSure = "09:00";
        } else if (PersonelIndex.Type == 4) { //     { id: 4, name: "ÜCRET FARKI " },
            tempSure = "00:00";
        } else if (PersonelIndex.Type == 5) {//     { id: 5, name: " PRİM - İKRAMİYE  " },
            tempSure = "00:00";
        } else if (PersonelIndex.Type == 6) {//     { id: 6, name: "BAYRAM - YEMEK - KİRA " },
            tempSure = "00:00";
        } else if (PersonelIndex.Type == 7) {//     { id: 7, name: "İZİN HARÇLIĞI" },
            tempSure = "00:00";
        } else if (PersonelIndex.Type == 8) {//     { id: 8, name: "KIDEM ZAMMI " },
            tempSure = "00:00";
        } else if (PersonelIndex.Type == 9) {//     { id: 9, name: "ÇOCUK ZAMMI " },
            tempSure = "00:00";
        } else if (PersonelIndex.Type == 10) {//     { id: 10, name: "YAKACAK YARDIMI " },
            tempSure = "00:00";
        } else if (PersonelIndex.Type == 11) {//     { id: 11, name: "FAZLA MESAİ ÜCRETİ" },
            tempSure = "00:00";
        } else if (PersonelIndex.Type == 12) {//     { id: 12, name: "DOĞUM İZNİ " },
            tempSure = "00:00";
        } else if (PersonelIndex.Type == 13) {//     { id: 13, name: "SÜT İZNİ   " },
            tempSure = "00:00";
        } else if (PersonelIndex.Type == 14) {//     { id: 14, name: "EVLİLİK İZNİ  " },
            tempSure = "00:00";
        } else if (PersonelIndex.Type == 15) {//     { id: 15, name: "ÖLÜM İZNİ " }, 
            tempSure = "00:00";
        } else if (PersonelIndex.Type == 16) {//     { id: 16, name: "MAZERET İZNİ  " },
            tempSure = "00:00";
        } else if (PersonelIndex.Type == 17) {//     { id: 17, name: "RAPORLU OLAN SÜRELER" }
            tempSure = "00:00";
        } else if (PersonelIndex.Type == 18) {//     { id: 18, name: "Dis Gorev" }
            new_PDKSPERSONEL.DisGorev++
            tempSure = "09:00";
        } else {
            console.log(PersonelIndex);
            continue
        }
        var tarihColumnDateObject = new Date(PersonelIndex.Tarih);
        var hafta = tarihColumnDateObject.getWeekOfMonth().toString();//kaçıncı hafta oldugunu buluyoruz
        var IndexCalistigiGun = new_PDKSPERSONEL.GunListesi.findIndex((item) => {
            return moment(item.Tarih, "DD.MM.YYYY").format("DD.MM.YYYY") === moment(PersonelIndex.Tarih).format("DD.MM.YYYY")
        });
        if (IndexCalistigiGun == -1) {   //  aynı günden den çalışan yoksa   
            new_PDKSPERSONEL.GunListesi.push({ Tarih: tarihColumnDateObject, Saat: tempSure, Hafta: hafta, Type: PersonelIndex.Type });
        } else { //eger aynı günden 2 tane varsa bu calıştıgı saatleri toplayıp 10:30 saatten buyuk mu diye kontrol yapıyor
            new_PDKSPERSONEL.GunListesi[IndexCalistigiGun].Saat = command.gunSaatTopla(new_PDKSPERSONEL.GunListesi[IndexCalistigiGun].Saat, tempSure)
            if (command.gunSaatKarsılastır(new_PDKSPERSONEL.GunListesi[IndexCalistigiGun].Saat, "10:30") == 1) {/// günlük çalışması 10:30 saatten buyyuk ise 10:30 saat yapma
                new_PDKSPERSONEL.GunListesi[IndexCalistigiGun].Saat = "10:30";
            }
        }
        if (IndexPersonel == -1) { ///personel eklenecekse kontrolü
            personeller.push(new_PDKSPERSONEL);
        }
    }
    for (let j = 0; j < personeller.length; j++) {
        var personel = personeller[j];
        if (personeller[j].SadelestirilmisAdSoyad == "ezgidundar") {
            console.log("test")

        }
        var haftalar = Object.keys(AydaHaftadaKacGunVar);
        for (let index = 0; index < haftalar.length; index++) {
            const hafta = haftalar[index];
            AydaHaftadaKacGunVar[hafta]; //haftadaki gün sayısı
            var haftalikKontrolSaati = command.haftalikKontrolSaati(AydaHaftadaKacGunVar[hafta])//cumartesiyide cıkarttık -1 yaparak
            var haftalikKontrolSaatininYarisi = haftalikKontrolSaati / 2
            var personelGunListesi = personel.GunListesi.filter(item => item.Hafta === hafta)
            var ss = 0;
            for (let tempp = 0; tempp < personelGunListesi.length; tempp++) {
                const element = personelGunListesi[tempp];
                var ttemp = personeller[j].AyinhaftalikToplamlari[hafta];

                ss = ttemp + element.Saat.saatiDakikayaCevir();
                if (ss > haftalikKontrolSaati) {
                    ss = haftalikKontrolSaati;
                }
                personeller[j].AyinhaftalikToplamlari[hafta] = ss;
            }
            if (ss > haftalikKontrolSaatininYarisi) {
                if (hafta == haftalar[haftalar.length - 1]) {//son hafta mı

                    if (d.sonHaftaCumartesiVarMi()) { //son hafta cumartesi varmı yok mu kontorlü
                        personeller[j].cumartesiHakki++
                    }
                } else {
                    personeller[j].cumartesiHakki++
                }
                if (personeller[j].cumartesiHakki > AydaMaximumCumartesiSayisi) { // bir ayda maximum cumartesi sayısında buyuk olamaz
                    personeller[j].cumartesiHakki = AydaMaximumCumartesiSayisi
                }
            }
        }
        //************************************************************************************** */
        for (let index = 0; index < haftalar.length; index++) {
            const hafta = haftalar[index];
            var temps = personeller[j].AyinhaftalikToplamlari[hafta]
            var ss = personeller[j].Sure + temps;
            personeller[j].Sure = ss
        }
        var toplamGun = personeller[j].Sure / (9 * 60);
        personeller[j].CalistigiGun = Math.round(toplamGun - personeller[j].YillikIzin - personeller[j].ResmiTatil - personeller[j].DisGorev)
        if ((toplamGun + personeller[j].cumartesiHakki) > 26.5) {
            //personel tam çalışmış sayılıyor
            personeller[j].ArgeGunu = 30
        } else {
            //resmi tatil ve yıllık izin cıkartılıp ve cumartesi hakkı eklenip hesaplanıyor yuvarlanıyor
            personeller[j].ArgeGunu = Math.round(toplamGun - personeller[j].YillikIzin - personeller[j].ResmiTatil + personeller[j].cumartesiHakki);
        }


    }
    res.json(returnTemplate(personeller, "hesapla"))
}
//------------------------------------------------------------------------------- 
personelControl = (personelList, personel, PersonelError) => {
    var OrnekHataliPersonel = {}
    var PersonelErrorFind = PersonelError.pdksVarDbYok.filter((item) => { //eklenen hatalı personeller listesinde var mı diye kontrol ediyor
        return item.SadelestirilmisAdSoyad == personel.SadelestirilmisAdSoyad
    }).length
    PersonelErrorFind += PersonelError.dbVarPdksYok.filter((item) => { //eklenen hatalı personeller listesinde var mı diye kontrol ediyor
        return item.SadelestirilmisAdSoyad == personel.SadelestirilmisAdSoyad
    }).length
    PersonelErrorFind += PersonelError.eslesmis.filter((item) => { //eklenen hatalı personeller listesinde var mı diye kontrol ediyor
        return item.SadelestirilmisAdSoyad == personel.SadelestirilmisAdSoyad
    }).length


    if (PersonelErrorFind !== 0) {
        return
    }
    var find = personelList.filter((item) => { //musterinin personeller tablosunda var mı diye kontrol ediyor
        return item.SadelestirilmisAdSoyad == personel.SadelestirilmisAdSoyad
    })
    OrnekHataliPersonel = {
        AdSoyad: personel.AdSoyad,
        SadelestirilmisAdSoyad: personel.SadelestirilmisAdSoyad,
        Aciklama: personel.Aciklama
    }
    if (find.length == 0) {
        PersonelError.pdksVarDbYok.push(OrnekHataliPersonel);

    } else {
        PersonelError.eslesmis.push(OrnekHataliPersonel)
    }

}


module.exports = router;
