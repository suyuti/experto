var     express         = require('express');
var     router          = express.Router();
const httpResult = require('../config');
const returnTemplate    = require('../template/returnTemplate');
const   ProjeTurleriService    = require('../services/projeTurleri.service');


router.get      ('/',       getAll);    // ProjeTurleri listesi doner
router.post     ('/',       create);    // ProjeTurleri oluşturur
router.get      ('/:id',    getById);   // idsi verilen ProjeTurleri getirir
router.put      ('/:id',    update);    // ProjeTurleri gunceller
router.delete   ('/:id',    remove);    // ProjeTurleri siler (Status = 0)


//-------------------------------------------------------------------------------

function getAll(req, res, next) {
    console.log("getAll");
    ProjeTurleriService.getAll()
        .then(data => {
            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Kayıt bulunamamıştır"))
            }
        })
        .catch(err => next(err));
}




//-------------------------------------------------------------------------------

function create(req, res, next) {
    console.log("create");
    ProjeTurleriService.create(req.body)
        .then(status => {
            if (status) {
                res.json(returnTemplate(status, "Basarı ile eklenmiştir"))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function getById(req, res, next) {
    console.log("getById");
    const id = parseInt(req.params.id);

    ProjeTurleriService.getById(id)
        .then(data => {

            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Bu id ye ait kayıt bulunamamıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function update(req, res, next) {
    console.log("update");
    const id = parseInt(req.params.id);

    ProjeTurleriService.update(id, req.body)
        .then(data => {
            if (data) {
                res.json(returnTemplate(data, "Basarı ile güncellenmiştir"))
            } else {
                res.json(returnTemplate([], "güncelleme yapılamammıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function remove(req, res, next) {
    console.log("remove");
    const id = parseInt(req.params.id)

    ProjeTurleriService.remove(id)
        .then(status => {
            if (status) {
                res.json(returnTemplate(status, "Basarı ile silinmiştir"))
            }
            else {
                res.json(returnTemplate([], "silme yapılamammıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------



module.exports = router;
