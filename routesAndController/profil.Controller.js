var     express         = require('express');
var     router          = express.Router(); 
const httpResult = require('../config');
const returnTemplate    = require('../template/returnTemplate');
const   ProfilService    = require('../services/profil.service');


router.get      ('/',       getAll);        // Profil listesi doner
router.post     ('/',       create);        // Profil oluşturur
router.get      ('/:id',    getById);       // idsi verilen Profil getirir
router.put      ('/:id',    update);        // Profil gunceller
router.delete   ('/:id',    remove);        // Profil siler (Status = 0)

//-------------------------------------------------------------------------------

function getAll(req, res, next) 
{
    console.log("getAll");
    ProfilService.getAll().then(data => {
        if(data.length){
            res.json(returnTemplate(data,""))
        }else{
            res.json(returnTemplate([],"Kayıt bulunamamıştır"))
        }
    })
    .catch(err => next(err));}

//-------------------------------------------------------------------------------

function create(req, res, next) 
{  
    console.log("create");
    ProfilService.create(req.body)
        .then(status => {
            if(status){
                res.json(returnTemplate(status,"Basarı ile eklenmiştir"))
            }
        })
        .catch(err => next(err));}

//-------------------------------------------------------------------------------

function getById(req, res, next) 
{
    console.log("getById");
    const id = parseInt(req.params.id);

    ProfilService.getById(id)
        .then(data => {
              
            if(data.length){
                res.json(returnTemplate(data,""))
            }else{
                res.json(returnTemplate(null,"Bu id ye ait kayıt bulunamamıştır", httpResult.error))
            }
        })
        .catch(err => next(err));}

//-------------------------------------------------------------------------------

function update(req, res, next) 
{
    console.log("update");
    const id = parseInt(req.params.id);

    ProfilService.update(id, req.body)
                    .then(data => {
                        if(data){
                            res.json(returnTemplate(data,"Basarı ile güncellenmiştir"))
                        }else{
                            res.json(returnTemplate(null,"güncelleme yapılamammıştır", httpResult.error))
                        }
                    })
                     .catch(err => next(err));}

//-------------------------------------------------------------------------------

function remove(req, res, next) 
{
    console.log("remove");
    const id = parseInt(req.params.id) 

    ProfilService.remove(id)
    .then(status => {
        if(status){
            res.json(returnTemplate(status,"Basarı ile silinmiştir"))
        }
        else{
            res.json(returnTemplate(null,"silme yapılamammıştır", httpResult.error))
        }
    })
    .catch(err => next(err));}

//-------------------------------------------------------------------------------



module.exports = router;
