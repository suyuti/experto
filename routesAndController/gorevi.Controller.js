var     express         = require('express');
var     router          = express.Router();
const httpResult = require('../config');
const returnTemplate    = require('../template/returnTemplate');
const   GorevService    = require('../services/gorev.service');

 
router.get      ('/',       getAll);// Gorev listesi doner
router.post     ('/',       create);// Gorev Oluşturur
router.get      ('/:id',    getById); // idsi verilen Gorev getirir
router.put      ('/:id',    update);// Gorev gunceller
router.delete   ('/:id',    remove); // Gorev siler (Status = 0)

//-------------------------------------------------------------------------------

function getAll(req, res, next) 
{
    console.log("getAll");
    GorevService.getAll().then(data => {
        if(data.length){
            res.json(returnTemplate(data,""))
        }else{
            res.json(returnTemplate([],"Bu id ye ait kayıt bulunamamıştır"))
        }
    })
    .catch(err => next(err));}

//-------------------------------------------------------------------------------

function create(req, res, next) 
{  
    console.log("create");
    GorevService.create(req.body)
        .then(status => {
            if(status){
                res.json(returnTemplate(status,"Basarı ile eklenmiştir"))
            }
        })
        .catch(err => next(err));
}
//-------------------------------------------------------------------------------

function getById(req, res, next) 
{
    console.log("getById");
    const id = parseInt(req.params.id);
    GorevService.getById(id) 
        .then(data => {
            if(data.length){
                res.json(returnTemplate(data,""))
            }else{
                res.json(returnTemplate([],"Bu id ye ait kayıt bulunamamıştır", httpResult.error))
            }
        })
        .catch(err => next(err));}

//-------------------------------------------------------------------------------

function update(req, res, next) 
{
    console.log("update");
    const id = parseInt(req.params.id);

    GorevService.update(id, req.body)
                    .then(data => {
                        if(data){
                            res.json(returnTemplate(data,"Basarı ile güncellenmiştir"))
                        }else{
                            res.json(returnTemplate([],"güncelleme yapılamammıştır", httpResult.error))
                        }
                    })
                     .catch(err => next(err));}

//-------------------------------------------------------------------------------

function remove(req, res, next) 
{
    console.log("remove");
    const id = parseInt(req.params.id) 

    GorevService.remove(id)
    .then(status => {
        if(status){
            res.json(returnTemplate(status,"Basarı ile silinmiştir"))
        }
        else{
            res.json(returnTemplate([],"silme yapılamammıştır", httpResult.error))
        }
    })
    .catch(err => next(err));}

//-------------------------------------------------------------------------------



module.exports = router;
