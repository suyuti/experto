var     express         = require('express');
var     router          = express.Router();
const httpResult = require('../config');
const returnTemplate    = require('../template/returnTemplate');
const   RoleService    = require('../services/roles.service');



router.get      ('/',                   getAll);    // Role listesi doner
router.post     ('/',                   create);    // Role oluşturur
router.get      ('/:id',                getById);   // idsi verilen Role getirir
router.put      ('/:id',                update);    // Role gunceller
router.delete   ('/:id',                remove);    // Role siler (Status = 0)
router.get      ('/:id/permission',     getPermissionByRoleId);   // idsi verilen Rolun permissionlarını getirir

//-------------------------------------------------------------------------------

function getAll(req, res, next) {
    console.log("getAll");
    RoleService.getAll()
        .then(data => {
            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Kayıt bulunamamıştır"))
            }
        })
        .catch(err => next(err));
}




//-------------------------------------------------------------------------------

function create(req, res, next) {
    console.log("create");
    RoleService.create(req.body)
        .then(status => {
            if (status) {
                res.json(returnTemplate(status, "Basarı ile eklenmiştir"))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function getById(req, res, next) {
    console.log("getById");
    const id = parseInt(req.params.id);

    RoleService.getById(id)
        .then(data => {

            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Bu id ye ait kayıt bulunamamıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function update(req, res, next) {
    console.log("update");
    const id = parseInt(req.params.id);

    RoleService.update(id, req.body)
        .then(data => {
            if (data) {
                res.json(returnTemplate(data, "Basarı ile güncellenmiştir"))
            } else {
                res.json(returnTemplate([], "güncelleme yapılamammıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function remove(req, res, next) {
    console.log("remove");
    const id = parseInt(req.params.id)

    RoleService.remove(id)
        .then(status => {
            if (status) {
                res.json(returnTemplate(status, "Basarı ile silinmiştir"))
            }
            else {
                res.json(returnTemplate([], "silme yapılamammıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

function getPermissionByRoleId(req, res, next) {
    console.log("getById");
    const id = parseInt(req.params.id);

    RoleService.getPermissionByRoleId(id)
        .then(data => {

            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Bu id ye ait kayıt bulunamamıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}


//-------------------------------------------------------------------------------



module.exports = router;
