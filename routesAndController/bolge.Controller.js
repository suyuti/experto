var     express         = require('express');
var     router          = express.Router();
const httpResult = require('../config');
const returnTemplate    = require('../template/returnTemplate');
const   BolgeService    = require('../services/bolge.service');

router.get      ('/',       getAll);// Bolge listesi doner
router.post     ('/',       create);// Bolge oluşturur
router.get      ('/:id',    getById);// Belli bir Bolge doner
router.put      ('/:id',    update);// Bolge gunceller
router.delete   ('/:id',    remove);// Bolge siler (Status = 0)

//-------------------------------------------------------------------------------

function getAll(req, res, next) {
    console.log("getAll");
    BolgeService.getAll()
        .then(data => {
            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Kayıt bulunamamıştır"))
            }
        })
        .catch(err => next(err));
}




//-------------------------------------------------------------------------------

function create(req, res, next) {
    console.log("create");
    BolgeService.create(req.body)
        .then(status => {
            if (status) {
                res.json(returnTemplate(status, "Basarı ile eklenmiştir"))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function getById(req, res, next) {
    console.log("getById");
    const id = parseInt(req.params.id);

    BolgeService.getById(id)
        .then(data => {

            if (data.length) {
                res.json(returnTemplate(data, ""))
            } else {
                res.json(returnTemplate([], "Bu id ye ait kayıt bulunamamıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function update(req, res, next) {
    console.log("update");
    const id = parseInt(req.params.id);

    BolgeService.update(id, req.body)
        .then(data => {
            if (data) {
                res.json(returnTemplate(data, "Basarı ile güncellenmiştir"))
            } else {
                res.json(returnTemplate([], "güncelleme yapılamammıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------

function remove(req, res, next) {
    console.log("remove");
    const id = parseInt(req.params.id)

    BolgeService.remove(id)
        .then(status => {
            if (status) {
                res.json(returnTemplate(status, "Basarı ile silinmiştir"))
            }
            else {
                res.json(returnTemplate([], "silme yapılamammıştır", httpResult.error))
            }
        })
        .catch(err => next(err));
}

//-------------------------------------------------------------------------------



module.exports = router;
