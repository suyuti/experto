DROP TABLE IF EXISTS "Bolge";
DROP SEQUENCE IF EXISTS "Bolge_id_seq";
CREATE SEQUENCE "Bolge_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."Bolge" (
    "id" integer DEFAULT nextval('"Bolge_id_seq"') NOT NULL,
    "BolgeAdi" text NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "Bolumler";
DROP SEQUENCE IF EXISTS "Bolumler_id_seq";
CREATE SEQUENCE "Bolumler_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."Bolumler" (
    "id" integer DEFAULT nextval('"Bolumler_id_seq"') NOT NULL,
    "BolumAdi" text NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "Departman";
DROP SEQUENCE IF EXISTS "Departman_id_seq";
CREATE SEQUENCE "Departman_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."Departman" (
    "id" integer DEFAULT nextval('"Departman_id_seq"') NOT NULL,
    "DepartmanAdi" text NOT NULL,
    "MusteriId" integer NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "EgitimDurumu";
DROP SEQUENCE IF EXISTS "EgitimDurumu_id_seq";
CREATE SEQUENCE "EgitimDurumu_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."EgitimDurumu" (
    "id" integer DEFAULT nextval('"EgitimDurumu_id_seq"') NOT NULL,
    "EgitimDurumAdi" text NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "Ekip";
DROP SEQUENCE IF EXISTS "Ekip_id_seq";
CREATE SEQUENCE "Ekip_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."Ekip" (
    "id" integer DEFAULT nextval('"Ekip_id_seq"') NOT NULL,
    "MusteriId" integer NOT NULL,
    "EkipAdi" text NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "Gorev";
DROP SEQUENCE IF EXISTS "Gorev_id_seq";
CREATE SEQUENCE "Gorev_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."Gorev" (
    "id" integer DEFAULT nextval('"Gorev_id_seq"') NOT NULL,
    "GorevAdi" text NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "IsPaketleri";
DROP SEQUENCE IF EXISTS "IsPaketleri_id_seq";
CREATE SEQUENCE "IsPaketleri_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."IsPaketleri" (
    "id" integer DEFAULT nextval('"IsPaketleri_id_seq"') NOT NULL,
    "Adi" text NOT NULL,
    "Aciklama" text NOT NULL,
    "ProjeId" integer NOT NULL,
    "BaslangicTarihi" date,
    "BitisTarihi" date,
    "CalisacakProfilListesi" text,
    "ProfilId" integer
) WITH (oids = false);


DROP TABLE IF EXISTS "Musteri";
DROP SEQUENCE IF EXISTS "Musteri_id_seq";
CREATE SEQUENCE "Musteri_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."Musteri" (
    "id" integer DEFAULT nextval('"Musteri_id_seq"') NOT NULL,
    "FirmaAdi" text NOT NULL,
    "TicariUnvan" text,
    "WebSitesi" text,
    "FirmaTuru" text,
    "FirmaAdresi" text,
    "SektorId" integer,
    "BolgeId" integer,
    "FaturaAdresi" text,
    "FabrikaAdresi" text,
    "FirmaTelefonu" text,
    "VergiDairesi" text,
    "VergiNumarasi" text,
    "AktifMi" boolean,
    "CreatedBy" integer NOT NULL,
    "CreatedAt" bigint NOT NULL,
    "UpdatedBy" integer,
    "UpdatedAt" bigint,
    "Status" smallint NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "Okullar";
DROP SEQUENCE IF EXISTS "Okullar_id_seq";
CREATE SEQUENCE "Okullar_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."Okullar" (
    "id" integer DEFAULT nextval('"Okullar_id_seq"') NOT NULL,
    "OkulAdi" text NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "Permissions";
DROP SEQUENCE IF EXISTS "Permissions_id_seq";
CREATE SEQUENCE "Permissions_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."Permissions" (
    "id" integer DEFAULT nextval('"Permissions_id_seq"') NOT NULL,
    "Path" text,
    "Aciklama" text,
    "PermissionKey" text NOT NULL,
    "Method" text
) WITH (oids = false);


DROP TABLE IF EXISTS "PermissionsMethod";
CREATE TABLE "public"."PermissionsMethod" (
    "id" integer NOT NULL,
    "Name" text NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "Personel";
DROP SEQUENCE IF EXISTS "Personel_id_seq";
CREATE SEQUENCE "Personel_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."Personel" (
    "id" integer DEFAULT nextval('"Personel_id_seq"') NOT NULL,
    "Adi" text NOT NULL,
    "Soyadi" text NOT NULL,
    "EgitimDurumu" integer,
    "Gorevi" integer,
    "Durumu" integer,
    "EkipId" integer,
    "DepartmanId" integer,
    "MusteriId" integer NOT NULL,
    "Status" integer,
    "ProfilId" integer,
    "UpdatedAt" bigint,
    "UpdatedBy" bigint,
    "CreatedAt" bigint,
    "CreatedBy" bigint,
    "IseBaslamaTarihi" bigint,
    "IstenAyrilmaTarihi" bigint,
    "Bolum" bigint,
    "Okul" bigint
) WITH (oids = false);


DROP TABLE IF EXISTS "PersonelProfil";
DROP SEQUENCE IF EXISTS "PersonelProfil_id_seq";
CREATE SEQUENCE "PersonelProfil_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."PersonelProfil" (
    "id" integer DEFAULT nextval('"PersonelProfil_id_seq"') NOT NULL,
    "ProfilAdi" text NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "PersonelProjeCalisma";
DROP SEQUENCE IF EXISTS "PersonelProjeCalisma_id_seq";
CREATE SEQUENCE "PersonelProjeCalisma_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."PersonelProjeCalisma" (
    "id" integer DEFAULT nextval('"PersonelProjeCalisma_id_seq"') NOT NULL,
    "UserId" integer NOT NULL,
    "ProjeId" integer NOT NULL,
    "ProjeAsama" integer NOT NULL,
    "CalismaGunu" integer NOT NULL,
    "CalismaSaati" integer NOT NULL,
    "Aciklama" integer NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "Proje";
DROP SEQUENCE IF EXISTS "Proje_id_seq";
CREATE SEQUENCE "Proje_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."Proje" (
    "id" integer DEFAULT nextval('"Proje_id_seq"') NOT NULL,
    "ProjeAdi" text NOT NULL,
    "ProjeKodu" text NOT NULL,
    "MusteriId" integer NOT NULL,
    "ProjeTuru" integer,
    "ProjeAlani" integer,
    "SunulanProjeButcesi" integer,
    "OnaylananProjeButcesi" integer,
    "Status" smallint,
    "SorumluDepartman" integer,
    "ProjeBaslangicTarihi" date,
    "ProjeBitisTarihi" date,
    "ProjeYazimBaslangicTarihi" date,
    "CreatedAt" bigint,
    "CreatedBy" bigint,
    "UpdatedAt" bigint,
    "UpdatedBy" bigint
) WITH (oids = false);


DROP TABLE IF EXISTS "ProjeAlanlari";
DROP SEQUENCE IF EXISTS "ProjeAlanlari_id_seq";
CREATE SEQUENCE "ProjeAlanlari_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."ProjeAlanlari" (
    "id" integer DEFAULT nextval('"ProjeAlanlari_id_seq"') NOT NULL,
    "ProjeAlanAdi" text NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "ProjeSorumlulari";
DROP SEQUENCE IF EXISTS "ProjeSorumlulari_id_seq";
CREATE SEQUENCE "ProjeSorumlulari_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."ProjeSorumlulari" (
    "id" integer DEFAULT nextval('"ProjeSorumlulari_id_seq"') NOT NULL,
    "ProjeId" integer NOT NULL,
    "UserId" integer NOT NULL,
    "SorumlulukTipi" integer NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "ProjeTurleri";
DROP SEQUENCE IF EXISTS "ProjeTurleri_id_seq";
CREATE SEQUENCE "ProjeTurleri_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."ProjeTurleri" (
    "id" integer DEFAULT nextval('"ProjeTurleri_id_seq"') NOT NULL,
    "ProjeTurAdi" text NOT NULL
) WITH (oids = false);



DROP TABLE IF EXISTS "Roles";
DROP SEQUENCE IF EXISTS "Role_id_seq";
CREATE SEQUENCE "Role_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."Roles" (
    "id" integer DEFAULT nextval('"Role_id_seq"') NOT NULL,
    "RoleName" text NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "RolesPermissions";
DROP SEQUENCE IF EXISTS "RolesPermissions_Id_seq";
CREATE SEQUENCE "RolesPermissions_Id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."RolesPermissions" (
    "RoleId" integer NOT NULL,
    "PermissionId" integer NOT NULL,
    "id" integer DEFAULT nextval('"RolesPermissions_Id_seq"') NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "Sektor";
DROP SEQUENCE IF EXISTS "Sektor_id_seq";
CREATE SEQUENCE "Sektor_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."Sektor" (
    "id" integer DEFAULT nextval('"Sektor_id_seq"') NOT NULL,
    "SektorAdi" text NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "UserRoles";
DROP SEQUENCE IF EXISTS "UserRoles_id_seq";
CREATE SEQUENCE "UserRoles_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."UserRoles" (
    "id" integer DEFAULT nextval('"UserRoles_id_seq"') NOT NULL,
    "UserId" integer NOT NULL,
    "RoleId" integer NOT NULL
) WITH (oids = false);


DROP TABLE IF EXISTS "Users";
DROP SEQUENCE IF EXISTS "User_id_seq";
CREATE SEQUENCE "User_id_seq" INCREMENT BY 1  MINVALUE 1 START 1;

CREATE TABLE "public"."Users" (
    "id" integer DEFAULT nextval('"User_id_seq"') NOT NULL,
    "UserName" text NOT NULL,
    "PasswordHash" text NOT NULL,
    "FirstName" text NOT NULL,
    "LastName" text NOT NULL,
    "EMail" text NOT NULL,
    "CreatedDate" time without time zone NOT NULL,
    "UpdatedDate" time without time zone,
    "CreatedBy" integer NOT NULL,
    "UpdatedBy" integer,
    "Status" smallint NOT NULL,
    "DisplayName" text NOT NULL,
    "PaswordSalt" text NOT NULL,
    "UserImage" text,
    "Dashboard" text,
    "IsAdmin" boolean
) WITH (oids = false);
