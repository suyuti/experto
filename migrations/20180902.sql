-- Adminer 4.7.2 PostgreSQL dump

CREATE SEQUENCE "Bolge_id_seq"  START 1 ;

CREATE TABLE "public"."Bolge" (
    "id" integer DEFAULT nextval('"Bolge_id_seq"') NOT NULL,
    "BolgeAdi" text NOT NULL
) WITH (oids = false);

INSERT INTO "Bolge" ("id", "BolgeAdi") VALUES
(1,	'Istanbul'),
(2,	'Gebze'),
(3,	'Kocaeli');

CREATE SEQUENCE "Departman_id_seq"  START 1 ;

CREATE TABLE "public"."Departman" (
    "id" integer DEFAULT nextval('"Departman_id_seq"') NOT NULL,
    "DepartmanAdi" text NOT NULL
) WITH (oids = false);


CREATE SEQUENCE "EgitimDurumu_id_seq"  START 1 ;

CREATE TABLE "public"."EgitimDurumu" (
    "id" integer DEFAULT nextval('"EgitimDurumu_id_seq"') NOT NULL,
    "EgitimDurumAdi" text NOT NULL
) WITH (oids = false);

INSERT INTO "EgitimDurumu" ("id", "EgitimDurumAdi") VALUES
(2,	'Lisans'),
(3,	'Ön Lisans (Öğrenci)'),
(4,	'Ön Lisans'),
(5,	'Yüksek Lisans'),
(6,	'Doktora');

CREATE SEQUENCE "Ekip_id_seq"  START 1 ;

CREATE TABLE "public"."Ekip" (
    "id" integer DEFAULT nextval('"Ekip_id_seq"') NOT NULL,
    "MusteriId" integer NOT NULL,
    "EkipAdi" text NOT NULL
) WITH (oids = false);


CREATE SEQUENCE "Gorev_id_seq"  START 1 ;

CREATE TABLE "public"."Gorev" (
    "id" integer DEFAULT nextval('"Gorev_id_seq"') NOT NULL,
    "GorevAdi" text NOT NULL
) WITH (oids = false);

INSERT INTO "Gorev" ("id", "GorevAdi") VALUES
(1,	'Araştırmacı'),
(2,	'Teknisyen'),
(3,	'Destek Personeli'),
(1,	'sd');

CREATE SEQUENCE "Musteri_id_seq"  START 1 ;

CREATE TABLE "public"."Musteri" (
    "id" integer DEFAULT nextval('"Musteri_id_seq"') NOT NULL,
    "FirmaAdi" text NOT NULL,
    "CreatedBy" integer NOT NULL,
    "CreatedAt" bigint NOT NULL,
    "UpdatedBy" integer,
    "UpdatedAt" bigint,
    "Status" smallint NOT NULL,
    "TicariUnvan" text,
    "WebSitesi" text,
    "FirmaTuru" text,
    "FirmaAdresi" text,
    "SektorId" integer,
    "BolgeId" integer,
    "FaturaAdresi" text,
    "FabrikaAdresi" text,
    "FirmaTelefonu" text,
    "VergiDairesi" text,
    "VergiNumarasi" text,
    "AktifMi" boolean
) WITH (oids = false);

INSERT INTO "Musteri" ("id", "FirmaAdi", "CreatedBy", "CreatedAt", "UpdatedBy", "UpdatedAt", "Status", "TicariUnvan", "WebSitesi", "FirmaTuru", "FirmaAdresi", "SektorId", "BolgeId", "FaturaAdresi", "FabrikaAdresi", "FirmaTelefonu", "VergiDairesi", "VergiNumarasi", "AktifMi") VALUES
(3,	'Musteri 3',	1,	'0',	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(8,	'Akmetal Metalürji Endüstrisi A.ş.',	1,	1566920317,	1,	1567421260437,	1,	'akmet tic unv',	NULL,	'K',	'fad',	2,	3,	'akmetal fatura adresiddsfsdff',	'akmetal fabrika adresi',	NULL,	'Sariyer VD',	NULL,	'0'),
(3,	'Yeni Firma 3',	1,	1566910577460,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(7,	'Polısan',	1,	'0',	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(4,	'Musteri 4',	1,	'0',	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(4,	'Yeni Firma 4',	1,	1566910606623,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(6,	'Musteri 6',	1,	'0',	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(2,	'Musteri 2',	1,	'0',	1,	1567423127672,	'0',	NULL,	'wwwe',	'S',	NULL,	1,	2,	'fatt',	'fabadr',	'1111',	'sar',	'11113',	'1'),
(6,	'3-s Mühendislik Müşavirlik San.ve Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(7,	'3-s Mühendislik Müşavirlik San.ve Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(1,	'Musteri 1d',	1,	'0',	1,	1567458310906,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	2,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(5,	'Yeni Firma BES',	1,	'0',	1,	1566910916916,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(5,	'Yeni Firma BES',	1,	1566910611069,	1,	1566910916916,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(9,	'Akpa Kimya Ambalaj San. Ve Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(11,	'Archem Sağlık San. Tic. Ltd. Şti.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(12,	'Arsan Kauçuk Plastik Makina A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(13,	'Artyol Endüstri İnş. San. Dış. Tic. A. Ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(14,	'Arıkan Kriko Ve Makina Sanayi Ticaret Anonim Şirketi',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(15,	'Atmaca Elektronik San. Ve Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(16,	'Aydın Örme San. Ve Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(17,	'Aydın Örme San. Ve Tic. A.ş. TASARIM',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(18,	'Berko İlaç Ve Kimya Sanayi A. Ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(19,	'Binovist Bilişim Danışmanlık A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(20,	'Bircom Telekomünikasyon Ve Bilişim Hizm.san.ve Tic.a.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(21,	'Cenetric Yüksek Yaşam Tekn. San Ve Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(22,	'Dalgakıran',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(23,	'De-ka Elektroteknik Sanayi Ve Ticaret Anonim Şirketi',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(24,	'Deha Tech Makina San Ve Tic A.ş',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(25,	'Demirören Tv Digital Platform İşletmeciliği A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(26,	'Devanlay-Eren Tekstil Sanayi Anonim Şirketi',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(27,	'Diyenem Kumaş Global San.ve Tic. A.ş',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(28,	'Dorçe Prefabrik Yapı Ve İnşaat Sanayi Ticaret A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(29,	'Dorçe Prefabrik Yapı Ve İnşaat Sanayi Ticaret A.ş. tasarım',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(30,	'Doğuş Vana Ve Döküm San. Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(31,	'Döksan Isıl İşlem A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(32,	'Ece Boya Kimya Sanayi Ve Ticaret A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(33,	'Elk Motor San. Ve Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(34,	'Elkon Elevatör Konveyör Ve Makina San. Ve Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(35,	'Enfalt A.ş',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(36,	'Era Çevre Teknolojileri Anonim Şirketi',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(37,	'Erca Group Kimya Sanayi Tic. A. Ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(38,	'Erciyas Çelik Boru San. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(39,	'Eren Perakende Satış Ve Mağ. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(40,	'Ergin Otomasyon Kon. Sis. San. Tic. Ltd. Şti',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(41,	'Erk Pazarlama Ve Giyim San. Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(42,	'Ersa Mobilya Sanayi Ve Ticaret A.ş. Arge',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(43,	'Ersa Mobilya Sanayi Ve Ticaret A.ş. Tasarım',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(44,	'Ersan Kauçuk San. Ve Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(45,	'Erse Kablo San. Ve Tic A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(46,	'Esalba Metal Sanayi Ve Ticaret A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(47,	'Esan Akümülatör Ve Malzemeleri San. Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(48,	'Evi Metal İnoks Yapı Ürünleri Mim. Ve Müt. İnş. Taah. San. Dış Tic. Ltd. Şti.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(49,	'Genex Yazılım A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(50,	'Grafi2000 Prodüksiyon Reklamcılık Yay. Ve Tic. Ltd. Şti.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(51,	'Green Chemicals Kimyasal Maddeler Sanayi Ve Ticaret A.ş',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(52,	'Greiner Ambalaj Sanayi Ve Ticaret A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(53,	'Gürmen Giyim Sanayi Ve Ticaret A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(54,	'Güven Pres Döküm San. Ve Tic. Ltd. Şti.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(55,	'Hacı Ayvaz End.mam.san.ve Tic.aş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(56,	'Işık Plastik San. Ve Dış Tic. Paz. A.ş',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(57,	'İllüzyon İstanbul Prodüksiyon Reklam Ve Görüntü Ürünleri San. Tic. Ltd. Şti',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(58,	'İsnet İstanbul Nettelekomünikasyon Elektronik Servis Hizmetleri Ticaret Ve Sanayi A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(59,	'İzel Kimya San. Ve Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(60,	'Jotun Boya Sanayi Ve Ticaret Anonim Şirketi',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(61,	'Kale Balata Otomotiv Sanayi Ve Ticaret A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(62,	'Karcan Kesici Takım San. Ve Tic. Anonim Şirketi',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(63,	'Kitoko Aydınlatma Ve Mühendislik İnş. Ltd. Şti.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(64,	'Koleksiyon Mobilya San. A.ş. TEKİRDAĞ',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(65,	'Koleksiyon Mobilya San. A.ş. İSTANBUL',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(66,	'Koçak Petrol Ürünleri San. Ve Tic. Ltd. Şti.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(67,	'Küçükçalık Tekstil Sanayi Ve Ticaret A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(68,	'Lider Kozmetik San.ve Tic.a.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(69,	'Loft Mağazacılık A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(70,	'Marlateks Teknoloji Tekstil Makine Ve Kimya San. Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(1,	'Musteri 1d',	1,	1566910557183,	1,	1567458310906,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	2,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(10,	'Ante Grup Bilişim Ticaret A.ş.',	1,	1566920317,	1,	1567419833060,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(2,	'Musteri 2',	1,	1566910573631,	1,	1567423127672,	1,	NULL,	'wwwe',	'S',	NULL,	1,	2,	'fatt',	'fabadr',	'1111',	'sar',	'11113',	'1'),
(71,	'Nexans Türkiye Endüstri Ve Ticaret A.ş. İSTANBUL',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(72,	'Nexans Türkiye Endüstri Ve Ticaret A.ş. DENİZLİ',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(73,	'Nova Kalıp Sanayi A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(74,	'Orbit Mühendislik Ve Makina San. Ltd. Şti.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(75,	'Petroyağ Ve Kimyasalları San. Ve Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(76,	'Pilot Taşıt Koltukları Sanayi Ve Ticaret A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(77,	'Polisan Kimya Sanayii A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(78,	'Promarket Tasarım Ve Teknoloji Anonim Şirketi',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(79,	'Prometal Hafif Metaller Doküm San Ve Tic.ltd.şti.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(80,	'Pronet Güvenlik Hizmetleri A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(81,	'Provel Tüketim Ürünleri San. Ve Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(82,	'Pür-plast Poliüretan Plastik Kimya San.ve Tic.a.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(83,	'Radim Radyatör San. Ve Tic.ltd.şti.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(84,	'Rc Endustri Ulaşım Araçları A.ş',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(85,	'Rebul Kozmetik Sanayi Ve Ticaret A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(86,	'Seluz Kimya Ve Kozmetik Sanayi A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(87,	'Sema Plastik Kalıp Ve Makina Sanayi Ve Ticaret Ltd.şti',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(88,	'Sistem Teknik Sanayi Fırınları A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(89,	'Strata Restorasyon Proje İnşaat Ltd. Şti.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(90,	'Tabanlıoğlu Mimarlık Anonim Şirketi',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(91,	'Tanatar Kalıp Pres İşleri San.ve Tic.ltd.şti.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(92,	'Teknik Kimya Donatım Makine Enerji San. Ve Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(93,	'Tls Teknoloji Sistemleri San Ve Dış Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(94,	'Tst Rakor Ve Tıbbi Alet. San. Ve Tic. Ltd. Şti',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(95,	'Ttaf Elektronik San.ve Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(96,	'Tuna Ofis Ve Ev Mob. Turz. İnş. San. Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(97,	'Tören Gıda Sanayi Ve Ticaret A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(98,	'Vatan Kablo Metal Endüstri Ve Ticaret A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(99,	'Yılmak Makina San. Ve Tic. A.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(100,	'Yılmaz Redüktör Sanayi Ve Ticaret A.ş',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(101,	'Yılteks Yıkama San . Ve Tic. A. Ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(102,	'Yıltem Konfeksiyon San. Ve Tic. A. Ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(103,	'Ünika Üniversal Kablo San.ve Tic.a.ş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(104,	'Şenmak Makina Sanayi Ve Ticaret Aş.',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(105,	'Zorlu Tekstil',	1,	1566920317,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(110,	'scsdcsc',	1,	1567090614798,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(109,	'xxx',	1,	1567090067039,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(111,	'xxa',	1,	1567090654430,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(121,	'asdfffd',	1,	1567092466508,	NULL,	NULL,	'0',	NULL,	NULL,	'S',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(116,	'wewewe',	1,	1567090977788,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(117,	'we',	1,	1567091058220,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(118,	'qweqrqr',	1,	1567091070247,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(119,	'wewerwer',	1,	1567091121057,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(112,	'qqqqq',	1,	1567090771120,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(113,	'aaaaa',	1,	1567090824264,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(114,	'aaaaaw',	1,	1567090894549,	NULL,	NULL,	'0',	'fgtgtg',	'asdasd',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(115,	'sdsd',	1,	1567090930968,	NULL,	NULL,	'0',	'',	'asdasd',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(120,	'asdadsasdasd',	1,	1567091804241,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(107,	'yep',	1,	1567089995694,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(108,	'trrrrr',	1,	1567090011548,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(122,	'sdd',	1,	1567092806436,	NULL,	NULL,	1,	'ddddsad',	NULL,	'S',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(123,	'ddddddd',	1,	1567094050065,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(124,	'yeee',	1,	1567408116879,	NULL,	NULL,	'0',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(126,	'20180902 11542',	1,	1567414479339,	NULL,	NULL,	1,	'asd',	NULL,	'S',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(127,	'Yani firma',	1,	1567422318238,	NULL,	NULL,	1,	'ticari unvain',	'wwwsi',	'S',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(128,	'msd',	1,	1567423047274,	NULL,	NULL,	1,	'msd tıc',	NULL,	'S',	'msd fırm adrs',	3,	3,	'faruremnaaers',	'fabrika asdres',	'tekeld',	'verdif',	'versi mon',	NULL),
(129,	'Yeni Bir Firma',	1,	1567458716285,	1,	1567458737756,	1,	'Ticari unvani',	'sitesi',	'S',	'Firmanin adresidir',	2,	2,	'Fatura adresi',	'Fabrikasinin adresi',	'2536',	'Vergi dairesi',	'12346',	NULL);

CREATE SEQUENCE "Permissions_id_seq"  START 1 ;

CREATE TABLE "public"."Permissions" (
    "id" integer DEFAULT nextval('"Permissions_id_seq"') NOT NULL,
    "RoleId" integer NOT NULL,
    "PermissionKey" text NOT NULL
) WITH (oids = false);

INSERT INTO "Permissions" ("id", "RoleId", "PermissionKey") VALUES
(4,	1,	'Musteri:Delete'),
(5,	1,	'Proje:Create'),
(6,	1,	'Proje:Read'),
(7,	1,	'Proje:Update'),
(8,	1,	'Proje:Delete'),
(9,	2,	'Musteri:Read'),
(10,	2,	'Musteri:Update'),
(11,	2,	'Proje:Read'),
(12,	2,	'Proje:Update'),
(1,	1,	'Musteri:Create'),
(3,	1,	'Musteri:Read'),
(2,	1,	'Musteri:Update'),
(14,	4,	'Admin:Admin'),
(16,	1,	'Personel:Update'),
(15,	1,	'Personel:Update');

CREATE SEQUENCE "Personel_id_seq"  START 1 ;

CREATE TABLE "public"."Personel" (
    "id" integer DEFAULT nextval('"Personel_id_seq"') NOT NULL,
    "Adi" text NOT NULL,
    "Soyadi" text NOT NULL,
    "Okul" text NOT NULL,
    "EgitimDurumu" integer NOT NULL,
    "Gorevi" integer,
    "Durumu" integer NOT NULL,
    "EkipId" integer,
    "DepartmanId" integer,
    "MusteriId" integer NOT NULL,
    "Status" integer NOT NULL,
    "ProfilId" integer NOT NULL,
    "UpdatedAt" bigint,
    "UpdatedBy" bigint,
    "CreatedAt" bigint,
    "CreatedBy" bigint,
    "IseBaslamaTarihi" bigint,
    "IstenAyrilmaTarihi" bigint
) WITH (oids = false);

INSERT INTO "Personel" ("id", "Adi", "Soyadi", "Okul", "EgitimDurumu", "Gorevi", "Durumu", "EkipId", "DepartmanId", "MusteriId", "Status", "ProfilId", "UpdatedAt", "UpdatedBy", "CreatedAt", "CreatedBy", "IseBaslamaTarihi", "IstenAyrilmaTarihi") VALUES
(16,	' cbcbcvbcvcvbcv ',	'test',	'tset',	1,	NULL,	1,	NULL,	NULL,	1,	'0',	1,	1566390746442,	NULL,	NULL,	NULL,	NULL,	NULL),
(1,	'Polusann',	'calisan soyadi',	'tahtakale',	1,	NULL,	1,	NULL,	NULL,	7,	1,	'0',	1566913565415,	NULL,	NULL,	NULL,	NULL,	NULL);

CREATE SEQUENCE "PersonelProfil_id_seq"  START 1 ;

CREATE TABLE "public"."PersonelProfil" (
    "id" integer DEFAULT nextval('"PersonelProfil_id_seq"') NOT NULL,
    "ProfilAdi" text NOT NULL
) WITH (oids = false);


CREATE SEQUENCE "PersonelProjeCalisma_id_seq"  START 1 ;

CREATE TABLE "public"."PersonelProjeCalisma" (
    "id" integer DEFAULT nextval('"PersonelProjeCalisma_id_seq"') NOT NULL,
    "UserId" integer NOT NULL,
    "ProjeId" integer NOT NULL,
    "ProjeAsama" integer NOT NULL,
    "CalismaGunu" integer NOT NULL,
    "CalismaSaati" integer NOT NULL,
    "Aciklama" integer NOT NULL
) WITH (oids = false);


CREATE SEQUENCE "Proje_id_seq"  START 1 ;

CREATE TABLE "public"."Proje" (
    "id" integer DEFAULT nextval('"Proje_id_seq"') NOT NULL,
    "ProjeAdi" text NOT NULL,
    "ProjeKodu" text NOT NULL,
    "MusteriId" integer NOT NULL,
    "ProjeTuru" integer,
    "ProjeBaslangicTarihi" date,
    "ProjeBitisTarihi" date,
    "ProjeYazimBaslangicTarihi" date,
    "ProjeAlani" integer,
    "SunulanProjeButcesi" integer,
    "OnaylananProjeButcesi" integer,
    "CreatedAt" time without time zone,
    "CreatedBy" integer,
    "UpdatedAt" time without time zone,
    "UpdatedBy" integer,
    "Status" smallint,
    "SorumluDepartman" integer
) WITH (oids = false);

INSERT INTO "Proje" ("id", "ProjeAdi", "ProjeKodu", "MusteriId", "ProjeTuru", "ProjeBaslangicTarihi", "ProjeBitisTarihi", "ProjeYazimBaslangicTarihi", "ProjeAlani", "SunulanProjeButcesi", "OnaylananProjeButcesi", "CreatedAt", "CreatedBy", "UpdatedAt", "UpdatedBy", "Status", "SorumluDepartman") VALUES
(34,	'M1-P2',	'2',	1,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(35,	'M2-P1',	'1',	2,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(33,	'M1-P1',	'1',	1,	NULL,	'2019-09-01',	'2020-08-01',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	NULL),
(36,	'M2-P2',	'2',	2,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	NULL),
(37,	'M2-P3 **',	'PRJ11',	2,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'14:09:41.643852',	NULL,	'14:14:26.898',	1,	1,	NULL);

CREATE SEQUENCE "ProjeAlanlari_id_seq"  START 1 ;

CREATE TABLE "public"."ProjeAlanlari" (
    "id" integer DEFAULT nextval('"ProjeAlanlari_id_seq"') NOT NULL,
    "ProjeAlanAdi" text NOT NULL
) WITH (oids = false);

INSERT INTO "ProjeAlanlari" ("id", "ProjeAlanAdi") VALUES
(1,	'Makine'),
(2,	'Kimya');

CREATE SEQUENCE "ProjeIsPaketi_id_seq"  START 1 ;

CREATE TABLE "public"."ProjeIsPaketi" (
    "id" integer DEFAULT nextval('"ProjeIsPaketi_id_seq"') NOT NULL,
    "ProjeId" integer NOT NULL,
    "IsPaketiAdi" text NOT NULL,
    "BaslangicTarihi" date NOT NULL,
    "BitisTarihi" date NOT NULL
) WITH (oids = false);


CREATE SEQUENCE "ProjeSorumlulari_id_seq"  START 1 ;

CREATE TABLE "public"."ProjeSorumlulari" (
    "id" integer DEFAULT nextval('"ProjeSorumlulari_id_seq"') NOT NULL,
    "ProjeId" integer NOT NULL,
    "UserId" integer NOT NULL,
    "SorumlulukTipi" integer NOT NULL
) WITH (oids = false);


CREATE SEQUENCE "ProjeTurleri_id_seq"  START 1 ;

CREATE TABLE "public"."ProjeTurleri" (
    "id" integer DEFAULT nextval('"ProjeTurleri_id_seq"') NOT NULL,
    "ProjeTurAdi" text NOT NULL
) WITH (oids = false);


CREATE SEQUENCE "Resources_id_seq"  START 1 ;

CREATE TABLE "public"."Resources" (
    "id" integer DEFAULT nextval('"Resources_id_seq"') NOT NULL,
    "ResourceName" text NOT NULL
) WITH (oids = false);

INSERT INTO "Resources" ("id", "ResourceName") VALUES
(1,	'Musteriler'),
(2,	'Projeler'),
(3,	'Kullanicilar');

CREATE SEQUENCE "Role_id_seq"  START 1 ;

CREATE TABLE "public"."Roles" (
    "id" integer DEFAULT nextval('"Role_id_seq"') NOT NULL,
    "RoleName" text NOT NULL
) WITH (oids = false);

INSERT INTO "Roles" ("id", "RoleName") VALUES
(1,	'Admin'),
(2,	'User'),
(3,	'Guest'),
(4,	'SystemManager');

CREATE SEQUENCE "Sektor_id_seq"  START 1 ;

CREATE TABLE "public"."Sektor" (
    "id" integer DEFAULT nextval('"Sektor_id_seq"') NOT NULL,
    "SektorAdi" text NOT NULL
) WITH (oids = false);

INSERT INTO "Sektor" ("id", "SektorAdi") VALUES
(1,	'Kimya'),
(2,	'Makina'),
(3,	'Bilişim');

CREATE SEQUENCE "UserRoles_id_seq"  START 1 ;

CREATE TABLE "public"."UserRoles" (
    "id" integer DEFAULT nextval('"UserRoles_id_seq"') NOT NULL,
    "UserId" integer NOT NULL,
    "RoleId" integer NOT NULL
) WITH (oids = false);

INSERT INTO "UserRoles" ("id", "UserId", "RoleId") VALUES
(1,	1,	1),
(2,	2,	2),
(3,	2,	1),
(4,	3,	3),
(5,	1,	4);

CREATE SEQUENCE "User_id_seq"  START 1 ;

CREATE TABLE "public"."Users" (
    "id" integer DEFAULT nextval('"User_id_seq"') NOT NULL,
    "UserName" text NOT NULL,
    "PasswordHash" text NOT NULL,
    "FirstName" text NOT NULL,
    "LastName" text NOT NULL,
    "EMail" text NOT NULL,
    "CreatedDate" time without time zone NOT NULL,
    "UpdatedDate" time without time zone,
    "CreatedBy" integer NOT NULL,
    "UpdatedBy" integer,
    "Status" smallint NOT NULL,
    "DisplayName" text NOT NULL,
    "PaswordSalt" text NOT NULL,
    "UserImage" text,
    "Dashboard" text,
    "IsAdmin" boolean
) WITH (oids = false);

INSERT INTO "Users" ("id", "UserName", "PasswordHash", "FirstName", "LastName", "EMail", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Status", "DisplayName", "PaswordSalt", "UserImage", "Dashboard", "IsAdmin") VALUES
(3,	'guest',	'guest',	'Guest',	'Guest',	'guest@example.com',	'08:48:57.858631',	NULL,	1,	NULL,	1,	'Guest bey',	'Guest',	NULL,	'/',	NULL),
(2,	'satis',	'satis',	'satis',	'satis',	'satis@',	'15:06:34.596698',	NULL,	1,	NULL,	1,	'Satis',	'satis',	NULL,	'/musteri',	NULL),
(1,	'admin',	'admin',	'a',	'a',	'a',	'12:15:04.326576',	NULL,	1,	NULL,	1,	'a',	'a',	NULL,	'/admin',	NULL);

-- 2019-09-02 22:05:38.188372+00