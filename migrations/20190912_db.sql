-- Bolgeler
ALTER SEQUENCE "Bolge_id_seq" RESTART WITH 1;
DELETE FROM "Bolge";
INSERT INTO "Bolge" ("id", "BolgeAdi") VALUES
(1, 'Istanbul'),
(2, 'Kocaeli'),
(3, 'Bursa'),
(4, 'Gebze');
ALTER SEQUENCE "Bolge_id_seq" RESTART WITH 5;

-- Bolumler
ALTER SEQUENCE "Bolumler_id_seq" RESTART WITH 1;
DELETE FROM "Bolumler";
INSERT INTO "Bolumler" ("id", "BolumAdi") VALUES
(1, 'Bolum1'),
(2, 'Bolum2'),
(3, 'Bolum3');
ALTER SEQUENCE "Bolumler_id_seq" RESTART WITH 4;


-- Departmanlar
ALTER SEQUENCE "Departman_id_seq" RESTART WITH 1;
DELETE FROM "Departman";
INSERT INTO "Departman" ("id", "DepartmanAdi", "MusteriId") VALUES
(1, 'Departman 1',	1),
(2, 'Departman 2',	1),
(3, 'Departman A',	2),
(4, 'Departman B',	2);
ALTER SEQUENCE "Departman_id_seq" RESTART WITH 5;

-- Sektorler
ALTER SEQUENCE "Sektor_id_seq" RESTART WITH 1;
DELETE FROM "Sektor";
INSERT INTO "Sektor" ("id", "SektorAdi") VALUES
(1, 'Kimya'),
(2, 'Makina'),
(3, 'Tekstil'),
(4, 'Bilişim');
ALTER SEQUENCE "Sektor_id_seq" RESTART WITH 5;

-- Egitim durumlari
ALTER SEQUENCE "EgitimDurumu_id_seq" RESTART WITH 1;
DELETE FROM "EgitimDurumu";
INSERT INTO "EgitimDurumu" ("id", "EgitimDurumAdi") VALUES
(1, 'LISANS'),
(2, 'YÜKSEK LİSANS(ÖĞRENCİ)'   ),
(3, 'LISE' ),
(4, 'YÜKSEK LISANS'),
(5, 'DOKTORA(ÖĞRENCİ)'),
(6, 'DOKTORA'),
(7, 'ÖN LISANS');
ALTER SEQUENCE "EgitimDurumu_id_seq" RESTART WITH 8;

-- Ekip
ALTER SEQUENCE "Ekip_id_seq" RESTART WITH 1;
DELETE FROM "Ekip";
INSERT INTO "Ekip" ("id", "MusteriId", "EkipAdi") VALUES
(1, 1,	'Ekip 1'),
(2, 1,	'Ekip 2'),
(3, 1,	'Ekip 3'),
(4, 2,	'Ekip A'),
(5, 2,	'Ekip B');
ALTER SEQUENCE "Ekip_id_seq" RESTART WITH 6;

-- Profiller
--   profil1
--   profil2
--   profil3

ALTER SEQUENCE "PersonelProfil_id_seq" RESTART WITH 1;
DELETE FROM "PersonelProfil";
INSERT INTO "PersonelProfil" ("id", "ProfilAdi") VALUES
(1, 'Profil-1'),
(2, 'Profil-2'),
(3, 'Profil-3');
ALTER SEQUENCE "PersonelProfil_id_seq" RESTART WITH 4;

-- Musteri
ALTER SEQUENCE "Musteri_id_seq" RESTART WITH 1;
DELETE FROM "Musteri";
INSERT INTO "Musteri" ("id", "FirmaAdi", "CreatedBy", "CreatedAt", "UpdatedBy", "UpdatedAt", "Status", "TicariUnvan", "WebSitesi", "FirmaTuru", "FirmaAdresi", "SektorId", "BolgeId", "FaturaAdresi", "FabrikaAdresi", "FirmaTelefonu", "VergiDairesi", "VergiNumarasi", "AktifMi") VALUES
(1, 'Test Müşterisi-1',	1,	'0',	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL),
(2, 'Test Müşterisi-2',	1,	'0',	NULL,	NULL,	'1',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL);
ALTER SEQUENCE "Musteri_id_seq" RESTART WITH 3;

-- Personeller
--   A (Profil 1)
--   B (Profil 1)
--   C (Profil 1)
--   D (Profil 2)
--   E (Profil 2)
--   F (Profil 3)
ALTER SEQUENCE "Personel_id_seq" RESTART WITH 1;
DELETE FROM "Personel";
INSERT INTO "Personel" ("id", "Adi", 
                        "Soyadi", 
                        "EgitimDurumu", 
                        "Gorevi", 
                        "Durumu", 
                        "EkipId", 
                        "DepartmanId", 
                        "MusteriId", 
                        "Status", 
                        "ProfilId", 
                        "UpdatedAt", 
                        "UpdatedBy", 
                        "CreatedAt", 
                        "CreatedBy", 
                        "IseBaslamaTarihi", 
                        "IstenAyrilmaTarihi", 
                        "Bolum", 
                        "Okul") VALUES
(1, 'Personel A-1',	'',	1,	1,	1,	NULL,	1,	1,	1,	1,	NULL,	NULL,	1567688913719,	NULL,	1567688913094,	1567688913094,	1,	1),
(2, 'Personel B-1',	'',	1,	1,	1,	NULL,	1,	1,	1,	1,	NULL,	NULL,	1567688913719,	NULL,	1567688913094,	1567688913094,	1,	1),
(3, 'Personel C-1',	'',	1,	1,	1,	NULL,	1,	1,	1,	1,	NULL,	NULL,	1567688913719,	NULL,	1567688913094,	1567688913094,	1,	1),
(4, 'Personel D-1',	'',	1,	1,	1,	NULL,	1,	1,	1,	2,	NULL,	NULL,	1567688913719,	NULL,	1567688913094,	1567688913094,	1,	1),
(5, 'Personel E-1',	'',	1,	1,	1,	NULL,	1,	1,	1,	2,	NULL,	NULL,	1567688913719,	NULL,	1567688913094,	1567688913094,	1,	1),
(6, 'Personel F-1',	'',	1,	1,	1,	NULL,	1,	1,	1,	3,	NULL,	NULL,	1567688913719,	NULL,	1567688913094,	1567688913094,	1,	1),

(7, 'Personel A-2',	'',	1,	1,	1,	NULL,	1,	2,	1,	1,	NULL,	NULL,	1567688913719,	NULL,	1567688913094,	1567688913094,	1,	1),
(8, 'Personel B-2',	'',	1,	1,	1,	NULL,	1,	2,	1,	1,	NULL,	NULL,	1567688913719,	NULL,	1567688913094,	1567688913094,	1,	1),
(9, 'Personel C-2',	'',	1,	1,	1,	NULL,	1,	2,	1,	1,	NULL,	NULL,	1567688913719,	NULL,	1567688913094,	1567688913094,	1,	1),
(10, 'Personel D-2',	'',	1,	1,	1,	NULL,	1,	2,	1,	2,	NULL,	NULL,	1567688913719,	NULL,	1567688913094,	1567688913094,	1,	1),
(11, 'Personel E-2',	'',	1,	1,	1,	NULL,	1,	2,	1,	2,	NULL,	NULL,	1567688913719,	NULL,	1567688913094,	1567688913094,	1,	1),
(12, 'Personel F-2',	'',	1,	1,	1,	NULL,	1,	2,	1,	3,	NULL,	NULL,	1567688913719,	NULL,	1567688913094,	1567688913094,	1,	1);
ALTER SEQUENCE "Personel_id_seq" RESTART WITH 13;


-- Projeler
--        |  1 |  2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 
-- Prj1   |  X |  X | X | Y | Y | Y | Z | Z | Z | Z  |    |    |   |   |   |   |   |   |   |   |   |    |    |    | 
-- Prj2   |    |    |   |   |   |   |   |   |   |    |    |    |   |   |   |   |   |   |   |   |   |    |    |    | 
-- Prj3   |    |    |   |   |   |   |   |   |   |    |    |    |   |   |   |   |   |   |   |   |   |    |    |    | 
-- Prj4   |    |    |   |   |   |   |   |   |   |    |    |    |   |   |   |   |   |   |   |   |   |    |    |    | 
-- Prj5   |    |    |   |   |   |   |   |   |   |    |    |    |   |   |   |   |   |   |   |   |   |    |    |    | 
-- Prj6   |    |    |   |   |   |   |   |   |   |    |    |    |   |   |   |   |   |   |   |   |   |    |    |    | 
-- Prj7   |    |    |   |   |   |   |   |   |   |    |    |    |   |   |   |   |   |   |   |   |   |    |    |    | 
-- Prj8   |    |    |   |   |   |   |   |   |   |    |    |    |   |   |   |   |   |   |   |   |   |    |    |    | 
-- Prj9   |    |    |   |   |   |   |   |   |   |    |    |    |   |   |   |   |   |   |   |   |   |    |    |    | 
-- Prj10  |    |    |   |   |   |   |   |   |   |    |    |    |   |   |   |   |   |   |   |   |   |    |    |    | 
--
-- 

ALTER SEQUENCE "Proje_id_seq" RESTART WITH 1;
DELETE FROM "Proje";

INSERT INTO "Proje" ("id", "ProjeAdi", "ProjeKodu", "MusteriId", "ProjeTuru", "ProjeAlani", "SunulanProjeButcesi", "OnaylananProjeButcesi", "Status", "SorumluDepartman", "ProjeBaslangicTarihi", "ProjeBitisTarihi", "ProjeYazimBaslangicTarihi", "CreatedAt", "CreatedBy", "UpdatedAt", "UpdatedBy") VALUES
(1, 'Proje 1',	'PRJ-1',	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	'2019.01.20', '2019.10.29',	NULL,	1530306010861,	1,	NULL,	NULL),
(2, 'Proje 2',	'PRJ-2',	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	'2019.01.20', '2019.10.29',	NULL,	1530306010861,	1,	NULL,	NULL),
(3, 'Proje 3',	'PRJ-3',	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	'2019.01.20', '2019.10.29',	NULL,	1530306010861,	1,	NULL,	NULL),
(4, 'Proje 4',	'PRJ-4',	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	'2019.01.20', '2019.10.29',	NULL,	1530306010861,	1,	NULL,	NULL),
(5, 'Proje 5',	'PRJ-5',	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	'2019.01.20', '2019.10.29',	NULL,	1530306010861,	1,	NULL,	NULL),
(6, 'Proje 6',	'PRJ-6',	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	'2019.01.20', '2019.10.29',	NULL,	1530306010861,	1,	NULL,	NULL),
(7, 'Proje 7',	'PRJ-7',	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	'2019.01.20', '2019.10.29',	NULL,	1530306010861,	1,	NULL,	NULL),
(8, 'Proje 8',	'PRJ-8',	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	'2019.01.20', '2019.10.29',	NULL,	1530306010861,	1,	NULL,	NULL),
(9, 'Proje 9',	'PRJ-9',	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	'2019.01.20', '2019.10.29',	NULL,	1530306010861,	1,	NULL,	NULL),
(10, 'Proje 10', 'PRJ-10',	1,	NULL,	NULL,	NULL,	NULL,	1,	NULL,	'2019.01.20', '2019.10.29',	NULL,	1530306010861,	1,	NULL,	NULL);
ALTER SEQUENCE "Proje_id_seq" RESTART WITH 11;

-- Is Paketleri
ALTER SEQUENCE "IsPaketleri_id_seq" RESTART WITH 1;
DELETE FROM "IsPaketleri";

INSERT INTO "IsPaketleri" ("id", "Adi", "Aciklama", "ProjeId", "CalisacakProfilListesi", "BaslangicTarihi", "BitisTarihi", "ProfilId") VALUES
(1, 'IP1-1',	'',	1,	'',	'2019-01-01',	'2019-04-01', 1),
(2, 'IP2-1',	'',	1,	'',	'2019-04-01',	'2019-07-01', 2),
(3, 'IP3-1',	'',	1,	'',	'2019-07-01',	'2019-10-01', 3),

(4, 'IP1-2',	'',	2,	'',	'2019-04-01',	'2019-07-01', 1),
(5, 'IP2-2',	'',	2,	'',	'2019-07-01',	'2019-10-01', 2),
(6, 'IP3-2',	'',	2,	'',	'2019-10-01',	'2020-01-01', 3),

(7, 'IP1-3',	'',	3,	'',	'2019-07-01',	'2019-10-01', 1),
(8, 'IP2-3',	'',	3,	'',	'2019-10-01',	'2020-01-01', 2),
(9, 'IP3-3',	'',	3,	'',	'2020-01-01',	'2020-04-01', 3),

(10, 'IP1-4',	'',	4,	'',	'2019-10-01',	'2020-01-01', 1),
(11, 'IP2-4',	'',	4,	'',	'2020-01-01',	'2020-04-01', 2),
(12, 'IP3-4',	'',	4,	'',	'2020-04-01',	'2020-07-01', 3),

(13, 'IP1-5',	'',	5,	'',	'2020-01-01',	'2020-04-01', 1),
(14, 'IP2-5',	'',	5,	'',	'2020-04-01',	'2020-07-01', 2),
(15, 'IP3-5',	'',	5,	'',	'2020-07-01',	'2020-10-01', 3),

(16, 'IP1-6',	'',	6,	'',	'2020-04-01',	'2020-07-01', 1),
(17, 'IP2-6',	'',	6,	'',	'2020-07-01',	'2020-10-01', 2),
(18, 'IP3-6',	'',	6,	'',	'2020-10-01',	'2021-01-01', 3),

(19, 'IP1-7',	'',	7,	'',	'2019-01-01',	'2019-04-01', 1),
(20, 'IP2-7',	'',	7,	'',	'2019-04-01',	'2019-07-01', 2),
(21, 'IP3-7',	'',	7,	'',	'2019-07-01',	'2019-10-01', 3),

(22, 'IP1-8',	'',	8,	'',	'2019-01-01',	'2019-04-01', 1),
(23, 'IP2-8',	'',	8,	'',	'2019-04-01',	'2019-07-01', 2),
(24, 'IP3-8',	'',	8,	'',	'2019-07-01',	'2019-10-01', 3),

(25, 'IP1-9',	'',	9,	'',	'2019-01-01',	'2019-04-01', 1),
(26, 'IP2-9',	'',	9,	'',	'2019-04-01',	'2019-07-01', 2),
(27, 'IP3-9',	'',	9,	'',	'2019-07-01',	'2019-10-01', 3),

(28, 'IP1-10',	'',	10,	'',	'2019-01-01',	'2019-04-01', 1),
(29, 'IP2-10',	'',	10,	'',	'2019-04-01',	'2019-07-01', 2),
(30, 'IP3-10',	'',	10,	'',	'2019-07-01',	'2019-10-01', 3),

(31, 'IP1-11',	'',	11,	'',	'2019-04-01',	'2019-07-01', 1),
(32, 'IP2-11',	'',	11,	'',	'2019-07-01',	'2019-10-01', 2),
(33, 'IP3-11',	'',	11,	'',	'2019-10-01',	'2020-01-01', 3),

(34, 'IP1-12',	'',	12,	'',	'2019-01-01',	'2019-04-01', 1),
(35, 'IP2-12',	'',	12,	'',	'2019-04-01',	'2019-07-01', 2),

(36, 'IP1-13',   '', 13,   '', '2019.03.01',   '2019.04.01', 1),
(37, 'IP2-13',   '', 13,   '', '2019.04.01',   '2019.05.01', 2),
(38, 'IP3-13',   '', 13,   '', '2019.05.01',   '2019.06.01', 3),

(39, 'IP1-14',   '', 14,   '', '2019.04.01',   '2019.05.01', 1),
(40, 'IP2-14',   '', 14,   '', '2019.05.01',   '2019.06.01', 2),
(41, 'IP3-14',   '', 14,   '', '2019.06.01',   '2019.08.01', 3),

(42, 'IP1-15',   '', 15,   '', '2019.04.01',   '2019.05.01', 1),
(43, 'IP2-15',   '', 15,   '', '2019.05.01',   '2019.06.01', 2);
ALTER SEQUENCE "IsPaketleri_id_seq" RESTART WITH 44;


-- Permissions
ALTER SEQUENCE "Permissions_id_seq" RESTART WITH 1;
DELETE FROM "Permissions";
INSERT INTO "Permissions" ("id", "Path", "Aciklama", "PermissionKey", "Method") VALUES
( 1, '/users/authenticate',	'user bilgilerini Kontrol eder',	'postUsersAuthenticate',	'post'),
( 2, '/users',	'user bilgilerini getirir',	'getUsers',	'get'),
( 3, '/users/:id',	'user id si verilen user � getirir',	'getUsersId',	'get'),
( 4, '/users/:id',	'user bilgilerini g�nceller',	'putUsersId',	'put'),
( 5, '/users',	'yeni bir user ekler',	'postUsers',	'post'),
( 6, '/users/:id',	' user bilgisini siler',	'deleteUsersId',	'delete'),
( 7, '/users/:id/role',	'user id si verilen ki�inin rollerini g�nceller',	'putUsersIdRole',	'put'),
( 8, '/musteri',	' Musteri listesini getirir',	'getMusteri',	'get'),
( 9, '/musteri/:id',	' Musteri id si verilen musteri bilgilerini getirir',	'getMusteriId',	'get'),
( 10, '/musteri/:id',	' Musteri id si verilen musterinin bilgilerini g�nceller',	'putMusteriId',	'put'),
( 11, '/musteri',	' yeni bir m��teri olu�turur',	'postMusteri',	'post'),
( 12, '/musteri/:id',	' musteri id si verilen Musteriyi siler',	'deleteMusteriId',	'delete'),
( 13, '/musteri/:id/proje',	' musteriye ait projeleri getirir',	'getMusteriIdProje',	'get'),
( 14, '/musteri/:id/personel',	' musteriye ait personelleri getirir',	'getMusteriIdPersonel',	'get'),
( 15, '/musteri/:id/personel/:pid',	'musteriye ait personel bilgisini g�nceller.',	'postMusteriIdPersonelPid',	'post'),
( 16, '/musteri/:id/ekip',	' musteriye ait ekip listesini getirir',	'getMusteriIdEkip',	'get'),
( 17, '/musteri/:id/departman',	' musteriye ait departman listesini getirir',	'getMusteriIdDepartman',	'get'),
( 18, '/musteri/:id/proje',	' musteriye yeni bir proje ekler',	'postMusteriIdProje',	'post'),
( 19, '/musteri/:id/departman',	' musteriye yeni bir departman ekler',	'postMusteriIdDepartman',	'post'),
( 20, '/musteri/:id/ekip',	' musteriye yeni bir ekip ekler',	'postMusteriIdEkip',	'post'),
( 21, '/musteri/:id/personel',	' musteriye yeni bir personel ekler',	'postMusteriIdPersonel',	'post'),
( 22, '/musteri/:id/adamay',	' Musterinin adam aylarini getirir',	'getMusteriIdAdamay',	'get'),
( 23, '/musteri/:id/adamay',	' Musteriye adam ay hesaplar',	'postMusteriIdAdamay',	'post'),
( 24, '/proje',	'projeleri getiri',	'getProje',	'get'),
( 25, '/proje/:id',	'proje id si verilen projenin bilgilerini getirir',	'getProjeId',	'get'),
( 26, '/proje/:id',	' proje id si verilen projenin bilgilerini g�nceller',	'putProjeId',	'put'),
( 27, '/proje/:id',	'proje id si verilen projeyi siler',	'deleteProjeId',	'delete'),
( 28, '/proje/:pid/ispaketi',	' projenin i� paketlerini getirir',	'getProjePidIspaketi',	'get'),
( 29, '/personel',	'Personel listesi doner',	'getPersonel',	'get'),
( 30, '/personel/:id',	'Belli bir personeli doner',	'getPersonelId',	'get'),
( 31, '/personel/:id',	'Belli bir personeli siler',	'deletePersonelId',	'delete'),
( 32, '/personel/:pid/musteri/:mid',	'Personeli musteride olusturur',	'postPersonelPidMusteriMid',	'post'),
( 33, '/personel',	'Yeni Bir Personel olu�turur',	'postPersonel',	'post'),
( 34, '/personel/:pid/departman/:did',	'Personelin departmanini set eder',	'putPersonelPidDepartmanDid',	'put'),
( 35, '/personel/:pid/ekip/:eid',	'Personeli ekibe atar',	'putPersonelPidEkipEid',	'put'),
( 36, '/personel/:pid/musteri/:mid',	'Personeli musteriye atar',	'putPersonelPidMusteriMid',	'put'),
( 37, '/personel/:pid/profil/:prid',	'Personelin profilini set eder',	'putPersonelPidProfilPrid',	'put'),
( 38, '/personel/:id',	'Belli bir personeli g�nceller',	'putPersonelId',	'put'),
( 39, '/personel/list',	'',	'getPersonelList',	'get'),
( 40, '/personel/:id',	'',	'getPersonelId',	'get'),
( 41, '/personel/:id',	'',	'deletePersonelId',	'delete'),
( 42, '/personel/new',	'',	'postPersonelNew',	'post'),
( 43, '/personel/:id',	'',	'postPersonelId',	'post'),
( 44, '/egitimDurumu',	'egitimdurumu listesi doner',	'getEgitimDurumu',	'get'),
( 45, '/egitimDurumu',	'egitimdurumu olu�turur',	'postEgitimDurumu',	'post'),
( 46, '/egitimDurumu/:id',	'idsi verilen egitimdurumu getirir',	'getEgitimDurumuId',	'get'),
( 47, '/egitimDurumu/:id',	'egitimdurumu gunceller',	'putEgitimDurumuId',	'put'),
( 48, '/egitimDurumu/:id',	'egitimdurumu siler (Status = 0)',	'deleteEgitimDurumuId',	'delete'),
( 49, '/gorev',	' Gorev listesi doner',	'getGorev',	'get'),
( 50, '/gorev',	' Gorev Olu�turur',	'postGorev',	'post'),
( 51, '/gorev/:id',	' idsi verilen Gorev getirir',	'getGorevId',	'get'),
( 52, '/gorev/:id',	' Gorev gunceller',	'putGorevId',	'put'),
( 53, '/gorev/:id',	' Gorev siler (Status = 0)',	'deleteGorevId',	'delete'),
( 54, '/personelProfil',	' Profil listesi doner',	'getPersonelProfil',	'get'),
( 55, '/personelProfil',	' Profil olu�turur',	'postPersonelProfil',	'post'),
( 56, '/personelProfil/:id',	' idsi verilen Profil getirir',	'getPersonelProfilId',	'get'),
( 57, '/personelProfil/:id',	' Profil gunceller',	'putPersonelProfilId',	'put'),
( 58, '/personelProfil/:id',	' Profil siler (Status = 0)',	'deletePersonelProfilId',	'delete'),
( 59, '/projeTurleri',	' ProjeTurleri listesi doner',	'getProjeTurleri',	'get'),
( 60, '/projeTurleri',	' ProjeTurleri olu�turur',	'postProjeTurleri',	'post'),
( 61, '/projeTurleri/:id',	' idsi verilen ProjeTurleri getirir',	'getProjeTurleriId',	'get'),
( 62, '/projeTurleri/:id',	' ProjeTurleri gunceller',	'putProjeTurleriId',	'put'),
( 63, '/projeTurleri/:id',	' ProjeTurleri siler (Status = 0)',	'deleteProjeTurleriId',	'delete'),
( 64, '/projeAlanlari',	' ProjeAlanlari listesi doner',	'getProjeAlanlari',	'get'),
( 65, '/projeAlanlari',	' ProjeAlanlari olu�turur',	'postProjeAlanlari',	'post'),
( 66, '/projeAlanlari/:id',	' idsi verilen ProjeAlanlari getirir',	'getProjeAlanlariId',	'get'),
( 67, '/projeAlanlari/:id',	' ProjeAlanlari gunceller',	'putProjeAlanlariId',	'put'),
( 68, '/projeAlanlari/:id',	' ProjeAlanlari siler (Status = 0)',	'deleteProjeAlanlariId',	'delete'),
( 69, '/departman',	'Departman listesi doner',	'getDepartman',	'get'),
( 70, '/departman',	' Departman olu�turur',	'postDepartman',	'post'),
( 71, '/departman/:id',	' Belli bir Departmanyi doner',	'getDepartmanId',	'get'),
( 72, '/departman/:id',	' Departman gunceller',	'putDepartmanId',	'put'),
( 73, '/departman/:id',	' Departmanyi siler (Status = 0)',	'deleteDepartmanId',	'delete'),
( 74, '/ekip',	' Ekip listesi doner',	'getEkip',	'get'),
( 75, '/ekip',	' Ekip olu�turur',	'postEkip',	'post'),
( 76, '/ekip/:id',	' idsi verilen Ekip getirir',	'getEkipId',	'get'),
( 77, '/ekip/:id',	' Ekip gunceller',	'putEkipId',	'put'),
( 78, '/ekip/:id',	' Ekip siler (Status = 0)',	'deleteEkipId',	'delete'),
( 79, '/bolge',	' Bolge listesi doner',	'getBolge',	'get'),
( 80, '/bolge',	' Bolge olu�turur',	'postBolge',	'post'),
( 81, '/bolge/:id',	' Belli bir Bolge doner',	'getBolgeId',	'get'),
( 82, '/bolge/:id',	' Bolge gunceller',	'putBolgeId',	'put'),
( 83, '/bolge/:id',	' Bolge siler (Status = 0)',	'deleteBolgeId',	'delete'),
( 84, '/sektor',	' Sektor listesi doner',	'getSektor',	'get'),
( 85, '/sektor',	' Sektor olu�turur',	'postSektor',	'post'),
( 86, '/sektor/:id',	' Belli bir Sektor doner',	'getSektorId',	'get'),
( 87, '/sektor/:id',	' Sektor gunceller',	'putSektorId',	'put'),
( 88, '/sektor/:id',	' Sektor siler (Status = 0)',	'deleteSektorId',	'delete'),
( 89, '/ispaketleri',	' IsPaketleri listesi doner',	'getIspaketleri',	'get'),
( 90, '/ispaketleri',	' IsPaketleri olu�turur',	'postIspaketleri',	'post'),
( 91, '/ispaketleri/:id',	' idsi verilen IsPaketleri getirir',	'getIspaketleriId',	'get'),
( 92, '/ispaketleri/:id',	' IsPaketleri gunceller',	'putIspaketleriId',	'put'),
( 93, '/ispaketleri',	' IsPaketleri listesi doner',	'getIspaketleri',	'get'),
( 94, '/ispaketleri/:id',	' IsPaketleri siler (Status = 0)',	'deleteIspaketleriId',	'delete'),
( 95, '/ispaketleri',	' IsPaketleri olu�turur',	'postIspaketleri',	'post'),
( 96, '/ispaketleri/:id',	' idsi verilen IsPaketleri getirir',	'getIspaketleriId',	'get'),
( 97, '/ispaketleri/:id',	' IsPaketleri gunceller',	'putIspaketleriId',	'put'),
( 98, '/ispaketleri/:id',	' IsPaketleri siler (Status = 0)',	'deleteIspaketleriId',	'delete'),
( 99, '/ispaketleri',	' IsPaketleri listesi doner',	'getIspaketleri',	'get'),
( 100, '/ispaketleri',	' IsPaketleri olu�turur',	'postIspaketleri',	'post'),
( 101, '/ispaketleri/:id',	' idsi verilen IsPaketleri getirir',	'getIspaketleriId',	'get'),
( 102, '/ispaketleri/:id',	' IsPaketleri gunceller',	'putIspaketleriId',	'put'),
( 103, '/ispaketleri/:id',	' IsPaketleri siler (Status = 0)',	'deleteIspaketleriId',	'delete'),
( 104, '/ispaketleri',	' IsPaketleri listesi doner',	'getIspaketleri',	'get'),
( 105, '/ispaketleri',	' IsPaketleri olu�turur',	'postIspaketleri',	'post'),
( 106, '/ispaketleri/:id',	' idsi verilen IsPaketleri getirir',	'getIspaketleriId',	'get'),
( 107, '/ispaketleri/:id',	' IsPaketleri gunceller',	'putIspaketleriId',	'put'),
( 108, '/ispaketleri/:id',	' IsPaketleri siler (Status = 0)',	'deleteIspaketleriId',	'delete'),
( 109, '/ispaketleri',	' IsPaketleri listesi doner',	'getIspaketleri',	'get'),
( 110, '/ispaketleri',	' IsPaketleri olu�turur',	'postIspaketleri',	'post'),
( 111, '/ispaketleri/:id',	' idsi verilen IsPaketleri getirir',	'getIspaketleriId',	'get'),
( 112, '/ispaketleri/:id',	' IsPaketleri gunceller',	'putIspaketleriId',	'put'),
( 113, '/ispaketleri/:id',	' IsPaketleri siler (Status = 0)',	'deleteIspaketleriId',	'delete'),
( 114, '/ispaketleri',	' IsPaketleri listesi doner',	'getIspaketleri',	'get'),
( 115, '/ispaketleri',	' IsPaketleri olu�turur',	'postIspaketleri',	'post'),
( 116, '/ispaketleri/:id',	' idsi verilen IsPaketleri getirir',	'getIspaketleriId',	'get'),
( 117, '/ispaketleri/:id',	' IsPaketleri gunceller',	'putIspaketleriId',	'put'),
( 118, '/ispaketleri/:id',	' IsPaketleri siler (Status = 0)',	'deleteIspaketleriId',	'delete'),
( 119, '/ispaketleri',	' IsPaketleri listesi doner',	'getIspaketleri',	'get'),
( 120, '/ispaketleri',	' IsPaketleri olu�turur',	'postIspaketleri',	'post'),
( 121, '/ispaketleri/:id',	' idsi verilen IsPaketleri getirir',	'getIspaketleriId',	'get'),
( 122, '/ispaketleri/:id',	' IsPaketleri gunceller',	'putIspaketleriId',	'put'),
( 123, '/ispaketleri/:id',	' IsPaketleri siler (Status = 0)',	'deleteIspaketleriId',	'delete'),
( 124, '/ispaketleri',	' IsPaketleri listesi doner',	'getIspaketleri',	'get'),
( 125, '/ispaketleri',	' IsPaketleri olu�turur',	'postIspaketleri',	'post'),
( 126, '/ispaketleri/:id',	' idsi verilen IsPaketleri getirir',	'getIspaketleriId',	'get'),
( 127, '/ispaketleri/:id',	' IsPaketleri gunceller',	'putIspaketleriId',	'put'),
( 128, '/ispaketleri/:id',	' IsPaketleri siler (Status = 0)',	'deleteIspaketleriId',	'delete'),
( 129, '/ispaketleri',	' IsPaketleri listesi doner',	'getIspaketleri',	'get'),
( 130, '/ispaketleri',	' IsPaketleri olu�turur',	'postIspaketleri',	'post'),
( 131, '/ispaketleri/:id',	' idsi verilen IsPaketleri getirir',	'getIspaketleriId',	'get'),
( 132, '/ispaketleri/:id',	' IsPaketleri gunceller',	'putIspaketleriId',	'put'),
( 133, '/ispaketleri/:id',	' IsPaketleri siler (Status = 0)',	'deleteIspaketleriId',	'delete'),
( 134, '/ispaketleri',	' IsPaketleri listesi doner',	'getIspaketleri',	'get'),
( 135, '/ispaketleri',	' IsPaketleri olu�turur',	'postIspaketleri',	'post'),
( 136, '/ispaketleri/:id',	' idsi verilen IsPaketleri getirir',	'getIspaketleriId',	'get'),
( 137, '/ispaketleri/:id',	' IsPaketleri siler (Status = 0)',	'deleteIspaketleriId',	'delete'),
( 138, '/ispaketleri/:id',	' IsPaketleri gunceller',	'putIspaketleriId',	'put'),
( 139, '/ispaketleri',	' IsPaketleri listesi doner',	'getIspaketleri',	'get'),
( 140, '/ispaketleri',	' IsPaketleri olu�turur',	'postIspaketleri',	'post'),
( 141, '/ispaketleri/:id',	' idsi verilen IsPaketleri getirir',	'getIspaketleriId',	'get'),
( 142, '/ispaketleri/:id',	' IsPaketleri gunceller',	'putIspaketleriId',	'put'),
( 143, '/ispaketleri/:id',	' IsPaketleri siler (Status = 0)',	'deleteIspaketleriId',	'delete');
ALTER SEQUENCE "Permissions_id_seq" RESTART WITH 144;


-- PermissionsMethod
DELETE FROM "PermissionsMethod";
INSERT INTO "PermissionsMethod" ("id", "Name") VALUES
(1, 'GET'),
(2, 'POST'),
(3, 'PUT'),
(4, 'DELETE');

-- Roles
ALTER SEQUENCE "Role_id_seq" RESTART WITH 1;
DELETE FROM "Roles";
INSERT INTO "Roles" ("id", "RoleName") VALUES
(1, 'Admin'),
(2, 'User'),
(3, 'Guest'),
(4, 'SystemManager');
ALTER SEQUENCE "Role_id_seq" RESTART WITH 5;

-- users
ALTER SEQUENCE "User_id_seq" RESTART WITH 1;
DELETE FROM "Users";
INSERT INTO "Users" ("id", "UserName", "PasswordHash", "FirstName", "LastName", "EMail", "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy", "Status", "DisplayName", "PaswordSalt", "UserImage", "Dashboard", "IsAdmin") VALUES
(1, 'admin',	'admin',	'a',	'a',	'a',	'12:15:04.326576',	NULL,	1,	NULL,	1,	'a',	'a',	NULL,	'/admin',	NULL),
(2, 'satis',	'satis',	'satis',	'satis',	'satis@',	'15:06:34.596698',	NULL,	1,	NULL,	1,	'Satis',	'satis',	NULL,	'/musteri',	NULL),
(3, 'guest',	'guest',	'Guest',	'Guest',	'guest@example.com',	'08:48:57.858631',	NULL,	1,	NULL,	1,	'Guest bey',	'Guest',	NULL,	'/',	NULL);
ALTER SEQUENCE "User_id_seq" RESTART WITH 4;

-- User roles
ALTER SEQUENCE "UserRoles_id_seq" RESTART WITH 1;
DELETE FROM "UserRoles";
INSERT INTO "UserRoles" ("id", "UserId", "RoleId") VALUES
(1, 1,	1),
(2, 1,	4),
(3, 2,	2),
(4, 2,	1),
(5, 3,	3);
ALTER SEQUENCE "UserRoles_id_seq" RESTART WITH 6;


-- Role Permissions
ALTER SEQUENCE "RolesPermissions_Id_seq" RESTART WITH 1;
DELETE FROM "RolesPermissions";
INSERT INTO "RolesPermissions" ("id", "RoleId", "PermissionId") VALUES
(  1, 1,   1),
(  2, 1,   2),
(  3, 1,   3),
(  4, 1,   4),
(  5, 1,   5),
(  6, 1,   6),
(  7, 1,   7),
(  8, 1,   8),
(  9, 1,   9),
( 10, 1,  10),
( 11, 1,  11),
( 12, 1,  12),
( 13, 1,  13),
( 14, 1,  14),
( 15, 1,  15),
( 16, 1,  16),
( 17, 1,  17),
( 18, 1,  18),
( 19, 1,  19),
( 20, 1,  20),
( 21, 1,  21),
( 22, 1,  22),
( 23, 1,  23),
( 24, 1,  24),
( 25, 1,  25),
( 26, 1,  26),
( 27, 1,  27),
( 28, 1,  28),
( 29, 1,  29),
( 30, 1,  30),
( 31, 1,  31),
( 32, 1,  32),
( 33, 1,  33),
( 34, 1,  34),
( 35, 1,  35),
( 36, 1,  36),
( 37, 1,  37),
( 38, 1,  38),
( 39, 1,  39),
( 40, 1,  40),
( 41, 1,  41),
( 42, 1,  42),
( 43, 1,  43),
( 44, 1,  44),
( 45, 1,  45),
( 46, 1,  46),
( 47, 1,  47),
( 48, 1,  48),
( 49, 1,  49),
( 50, 1,  50),
( 51, 1,  51),
( 52, 1,  52),
( 53, 1,  53),
( 54, 1,  54),
( 55, 1,  55),
( 56, 1,  56),
( 57, 1,  57),
( 58, 1,  58),
( 59, 1,  59),
( 60, 1,  60),
( 61, 1,  61),
( 62, 1,  62),
( 63, 1,  63),
( 64, 1,  64),
( 65, 1,  65),
( 66, 1,  66),
( 67, 1,  67),
( 68, 1,  68),
( 69, 1,  69),
( 70, 1,  70),
( 71, 1,  71),
( 72, 1,  72),
( 73, 1,  73),
( 74, 1,  74),
( 75, 1,  75),
( 76, 1,  76),
( 77, 1,  77),
( 78, 1,  78),
( 79, 1,  79),
( 80, 1,  80),
( 81, 1,  81),
( 82, 1,  82),
( 83, 1,  83),
( 84, 1,  84),
( 85, 1,  85),
( 86, 1,  86),
( 87, 1,  87),
( 88, 1,  88),
( 89, 1,  89),
( 90, 1,  90),
( 91, 1,  91),
( 92, 1,  92),
( 93, 1,  93),
( 94, 1,  94),
( 95, 1,  95),
( 96, 1,  96),
( 97, 1,  97),
( 98, 1,  98),
( 99, 1,  99),
(100, 1, 100),
(101, 1, 101),
(102, 1, 102),
(103, 1, 103),
(104, 1, 104),
(105, 1, 105),
(106, 1, 106),
(107, 1, 107),
(108, 1, 108),
(109, 1, 109),
(110, 1, 110),
(111, 1, 111),
(112, 1, 112),
(113, 1, 113),
(114, 1, 114),
(115, 1, 115),
(116, 1, 116),
(117, 1, 117),
(118, 1, 118),
(119, 1, 119),
(120, 1, 120),
(121, 1, 121),
(122, 1, 122),
(123, 1, 123),
(124, 1, 124),
(125, 1, 125),
(126, 1, 126),
(127, 1, 127),
(128, 1, 128),
(129, 1, 129),
(130, 1, 130),
(131, 1, 131),
(132, 1, 132),
(133, 1, 133),
(134, 1, 134),
(135, 1, 135),
(136, 1, 136),
(137, 1, 137),
(138, 1, 138),
(139, 1, 139),
(140, 1, 140),
(141, 1, 141),
(142, 1, 142),
(143, 1, 143);
ALTER SEQUENCE "RolesPermissions_Id_seq" RESTART WITH 144;

