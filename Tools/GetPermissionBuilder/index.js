 
const path = require('path');
const fs = require('fs');
const command = require('./command'); 


 
var permissions = []

String.prototype.hashCode = function() {
    var hash = 0, i, chr;
    if (this.length === 0) return hash;
    for (i = 0; i < this.length; i++) {
      chr   = this.charCodeAt(i);
      hash  = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  };
  


class Permissions {
    constructor() {
        this.Code =   "";
        this.Path =   "";
        this.Aciklama =  ""; 
        this.Method =  ""; 
        this.PermissionKey =  ""; 
    }
  
    getSql() {
        var Query = `INSERT INTO "Permissions" 
                ("Code",
                "Path", 
                "Aciklama",
                "Method",
                "PermissionKey")
        VALUES (
            ${this.Code ? '\'' + this.Code + '\'' : '\'\''}, 
            ${this.Path ? '\'' + this.Path + '\'' : '\'\''}, 
            ${this.Aciklama ? '\'' + this.Aciklama + '\'' : '\'\''},
            ${this.Method ? '\'' + this.Method + '\'' : '\'\''},
            ${this.PermissionKey ? '\'' + this.PermissionKey + '\'' : '\'\''}
                );\n`;
        return Query;
    }
}




function pathControllerCozumle(filePath) {
    var lines = require('fs').readFileSync(filePath, 'utf-8')
    .split('\n')
    .filter(Boolean);

    for (let index = 0; index < lines.length; index++) {
        //app.js dosyasının satır satır okkunmasını saglar
       
        var str =  lines[index]; //her satır sırası ile regeg kodu ile 
        var usePath = /app.use[(]['][/][a-z]*[']/i; // app dosyasında kullanılan ana urlnin pathinı bulmak için kullanılan regex ör: "app.use('/users'"
        var controllerPath = /require[(]['][./a-z]*[']/i; //app dosyasının o satırındaki urleye baglı alt controller dosyası nın ismini almak için kullanılan regex ör " require('./routesAndController/users.Controller'"
        if (str.match(usePath)) { // pathı eşlesen varsa 
            var useResult = str.match(usePath).toString(); //eşlesen kelimeyi sec ve stringe cevir "app.use('/users'"
            var usePathResult = useResult.split("'")[1]; // sonra bu dosyanın  pathını bulmak için "'" karakteri ile böl ve pathı al "/users"
        }
        if (str.match(controllerPath)) { //yukardakinin aynısı fakat "routesAndController/users.Controller"  ismini bulmak için yapılmıştır
            var controllerResult = str.match(controllerPath).toString();
            var controllerPathResult = controllerResult.split("'")[1];
            controllerPathResult = controllerPathResult.split(/([.][/]|[.][.][/])/i)[2]
        }
    
        if (usePathResult && controllerPathResult) { // eger ikiside eşleşmiş ise  alt kontroller dosyasındaki dosyaları okuyacak

            var controllerFilePath = path.join(__dirname+"/../..", controllerPathResult +'.js');
            var controllerLines = require('fs').readFileSync(controllerFilePath, 'utf-8')
            .split('\n')
            .filter(Boolean);///controlleri sıra sıra okuyoruz
    
            for (let i = 0; i < controllerLines.length; i++) { ///controlleri sıra sıra okuyoruz
                const Cline = controllerLines[i];
                var CPath = /router[.][a-z]*[\s]*[(]['][/:a-z]*[']/i; // controllerdaki pathı bulmak için regex kodu
                //var aciklamaregex = /[)][\s]*[)][\s]*[;][\s]*[/][/][/]*/i; 
                var aciklamaregex =/[)][\s]*[;]*[\s]*[/][/][/]*/i// controllerdaki  acıklamayı bulmak için regex kodu
                if (Cline.match(CPath)) {
                    var ClineResult = Cline.match(CPath).toString();
                    var methot = ClineResult.split("'")[0];
                    methot=methot.replace('(', " ");
                    methot=methot.split(".")[1].trim();
                    var ClinePathResult = ClineResult.split("'")[1];
                    console.log(methot+"\t ==>>\t "+usePathResult+ClinePathResult )
                    if (Cline.match(aciklamaregex)) {
                      var tempAciklama = Cline.split(aciklamaregex)[1];  
                      tempAciklama=tempAciklama.replace(/[']/g, ' ');
                      tempAciklama=tempAciklama.replace(/["]/g,' ');
                    }else{
                        var tempAciklama =""; 
                    }
                    

                    var temp = new Permissions();//permissino oluşturuyoruz
                    temp.Path =   usePathResult+ClinePathResult;
                    temp.Aciklama =  tempAciklama; 
                    temp.Method =  methot; 
                    temp.PermissionKey =  command.sadelestir(methot+usePathResult+ClinePathResult); 
                    temp.Code =  parseInt(temp.PermissionKey.hashCode()); 

                    var elemanVar=0;
                    for (let j = 0; j < permissions.length; j++) {
                        if(temp.Code==permissions[j].Code ){
                            elemanVar=1; 
                            break;
                        }
                    }
                    if (!elemanVar) {
                      permissions.push(temp); //permissina ekliyoruz  
                    }
                    
                }   
            }
        }

       
        




    }

 
        
    
    
  
    
}

var filePath = path.join(__dirname+"/../..", 'app.js');
pathControllerCozumle(filePath);
console.log(permissions);


fs.writeFile(`./[ExportSqlQuery]/Permissions.txt`, "", function (err) {
    if (err) throw err;
    console.log('Permissions File is created successfully.');

    for (let index = 0; index < permissions.length; index++) {
        var query = permissions[index].getSql(); 
        fs.appendFile(`./[ExportSqlQuery]/Permissions.txt`, query, function (err) {
            if (err) throw err;
        });
    }
});


fs.writeFile(`./[ExportSqlQuery]/Permissions-UI.txt`, "", function (err) {
    if (err) throw err;
    console.log('Permissions-UI File is created successfully.');
    var query ="var PermissionData={\n";
    for (let index = 0; index < permissions.length; index++) {
        var data={
            id:index+1,
            Aciklama:permissions[index].Aciklama,
            Method:permissions[index].Method
        }
         query +="\t"+ permissions[index].PermissionKey+":" + JSON.stringify(data) +",\n";   
    }
    query +="};";
    fs.appendFile(`./[ExportSqlQuery]/Permissions-UI.txt`, query, function (err) {
        if (err) throw err;
    });
});

